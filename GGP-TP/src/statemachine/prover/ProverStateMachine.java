package statemachine.prover;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import statemachine.MachineState;
import statemachine.Move;
import statemachine.Prover;
import statemachine.Role;
import statemachine.StateMachine;

import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;
import core.gdl.Gdl;
import core.gdl.GdlConstant;
import core.gdl.GdlRelation;
import core.gdl.GdlSentence;

public class ProverStateMachine extends StateMachine {
    private MachineState initialState;
    private Prover prover;
    private List<Role> roles;
   
    
    /** These fields are for time analysis*/
    protected int nextstatecalls;
    protected long timepernextstate;
    protected int goalstatecalls = 0;
    protected long timerpergoalstate = 0;
    protected int legalcalls = 0;
    protected long timerperlegal = 0;
    protected int terminalcalls = 0;
    protected long timerperterminal = 0;
	private long maxterminalcomputation = 0;
	private long maxgoalcomputation = 0;
	private long maxnextstatecomputation = 0;
	private long maxlegalcomputation = 0;
    
    
    
    /**
     * Initialize must be called before using the StateMachine
     */
    public ProverStateMachine() {

    }

    public void initialize(List<Gdl> description) {
        prover = new AimaProver(new HashSet<Gdl>(description));
        roles = Role.computeRoles(description);
        initialState = computeInitialState();
    }

    private MachineState computeInitialState() {
        Set<GdlSentence> results = prover.askAll(
                ProverQueryBuilder.getInitQuery(), new HashSet<GdlSentence>());
        return new ProverResultParser().toState(results);
    }

    @Override
    public int getGoal(MachineState state, Role role)
            throws GoalDefinitionException {
        goalstatecalls++;
        long start = System.currentTimeMillis();
        
        Set<GdlSentence> results = prover.askAll(
                ProverQueryBuilder.getGoalQuery(role),
                ProverQueryBuilder.getContext(state));

        if (results.size() != 1) {
            // TODO GamerLogger.logError("StateMachine",
            // "Got goal results of size: " + results.size() +
            // " when expecting size one.");
            throw new GoalDefinitionException(state, role);
        }

        try {
            GdlRelation relation = (GdlRelation) results.iterator().next();
            GdlConstant constant = (GdlConstant) relation.get(1);
            
            long stop = System.currentTimeMillis();
            timerpergoalstate += stop-start;
            if(stop-start > getMaxgoalcomputation())
    			maxgoalcomputation = (stop-start);
            return Integer.parseInt(constant.toString());
        } catch (Exception e) {
            throw new GoalDefinitionException(state, role);
        }
    }

    @Override
    public MachineState getInitialState() {
        return initialState;
    }

    @Override
    public List<Move> getLegalMoves(MachineState state, Role role)
            throws MoveDefinitionException {
        legalcalls++;
        long start = System.currentTimeMillis();
        
        Set<GdlSentence> results = prover.askAll(
                ProverQueryBuilder.getLegalQuery(role),
                ProverQueryBuilder.getContext(state));

        if (results.size() == 0) {
            throw new MoveDefinitionException(state, role);
        }
        long stop = System.currentTimeMillis();
        timerperlegal += stop-start;
        if(stop-start > getMaxlegalcomputation())
			maxlegalcomputation = (stop-start);
        return new ProverResultParser().toMoves(results);
    }

    @Override
    public MachineState getNextState(MachineState state, List<Move> moves)
            throws TransitionDefinitionException {
    	long start = System.currentTimeMillis();
        Set<GdlSentence> results = prover.askAll(
                ProverQueryBuilder.getNextQuery(),
                ProverQueryBuilder.getContext(state, getRoles(), moves));

        for (GdlSentence sentence : results) {
            if (!sentence.isGround()) {
                throw new TransitionDefinitionException(state, moves);
            }
        }
        long stop = System.currentTimeMillis();
        timepernextstate += stop-start;
        if(stop-start > getMaxnextstatecomputation())
			maxnextstatecomputation = (stop-start);
        return new ProverResultParser().toState(results);
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }

    @Override
    public boolean isTerminal(MachineState state) {
        terminalcalls++;
        long start = System.currentTimeMillis();
        
        boolean result = prover.prove(ProverQueryBuilder.getTerminalQuery(),
                ProverQueryBuilder.getContext(state));
        
        long stop = System.currentTimeMillis();
        timerperterminal += stop-start;
        if(stop-start > getMaxterminalcomputation())
			maxterminalcomputation = (stop-start);
        return result;
    }


    @Override
    public long getStateComputationTime() {
        return timepernextstate;
    }
    @Override
    public int getNextStateCalls() {
        return nextstatecalls;
    }
    @Override
    public int getGoalCalls() {
        return goalstatecalls;
    }
    @Override
    public int getLegalCalls() {
        return legalcalls;
    }
    @Override
    public int getTerminalCalls() {
       return terminalcalls;
    }
    @Override
    public long getLegalComputationTime() {
       return timerperlegal;
    }
    @Override
    public long getGoalComputationTime() {
       return timerpergoalstate;
    }
    @Override
    public long getTerminalComputationTime() {
        return timerperterminal;
    }

	@Override
	public long getMaxgoalcomputation() {
		return maxgoalcomputation;
	}
	@Override
	public long getMaxterminalcomputation() {
		return maxterminalcomputation;
	}

	@Override
	public long getMaxnextstatecomputation() {
		return maxnextstatecomputation;
	}

	@Override
	public long getMaxlegalcomputation() {
		return maxlegalcomputation;
	}
}
