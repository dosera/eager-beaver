package core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

import statemachine.*;

import core.gdl.*;

/**
 * Match encapsulates all of the information relating to a single match. A match
 * is a single play through a game, with a complete history that lists what move
 * each player made at each step through the match. This also includes other
 * relevant metadata about the match, including some unique identifiers,
 * configuration information, and so on.
 * 
 * NOTE: Match objects created by a player, representing state read from a
 * server, are not completely filled out. For example, they only get an
 * ephemeral Game object, which has a rulesheet but no key or metadata. Gamers
 * which do not derive from StateMachineGamer also do not keep any information
 * on what states have been observed, because (somehow) they are representing
 * games without using state machines. In general, these player-created Match
 * objects shouldn't be sent out into the ecosystem.
 * 
 * @author Sam Schreiber
 */
public final class Match {
    private final String matchId;
    private final int playClock;
    private final int startClock;
    private final Date startTime;
    private final Game theGame;
    private final List<String> theRoleNames;
    private final List<List<GdlSentence>> moveHistory;
    private List<Move> lastMoves;
    private final List<Set<GdlSentence>> stateHistory;
    private final List<List<String>> errorHistory;
    private final List<Date> stateTimeHistory;
    private boolean isCompleted;
    private final List<Integer> goalValues;
    
    public Match(String matchId, int startClock, int playClock, Game theGame) {
        this.matchId = matchId;
        this.startClock = startClock;
        this.playClock = playClock;
        this.theGame = theGame;

        this.startTime = new Date();
        this.isCompleted = false;

        this.theRoleNames = new ArrayList<String>();
        for (Role r : Role.computeRoles(theGame.getRules())) {
            this.theRoleNames.add(r.getName().getName().toString());
        }

        this.moveHistory = new ArrayList<List<GdlSentence>>();
        this.stateHistory = new ArrayList<Set<GdlSentence>>();
        this.stateTimeHistory = new ArrayList<Date>();
        this.errorHistory = new ArrayList<List<String>>();

        this.goalValues = new ArrayList<Integer>();
    }

    /* Mutators */
    public void appendMoves(List<GdlSentence> moves) {
        moveHistory.add(moves);
        lastMoves = new ArrayList<Move>();
        for(GdlSentence gsent : moves)
            lastMoves.add(new Move(gsent));
    }

    public void appendMoves2(List<Move> moves) {
        // NOTE: This is appendMoves2 because it Java can't handle two
        // appendMove methods that both take List objects with different
        // templatized parameters.
        if (moves.get(0) instanceof Move) {
            List<GdlSentence> theMoves = new ArrayList<GdlSentence>();
            for (Move m : moves) {
                theMoves.add(m.getContents());
            }
            appendMoves(theMoves);
        }
    }

    public void appendState(Set<GdlSentence> state) {
        stateHistory.add(state);
        stateTimeHistory.add(new Date());
    }

    public void appendErrors(List<String> errors) {
        errorHistory.add(errors);
    }

    public void appendNoErrors() {
        List<String> theNoErrors = new ArrayList<String>();
        for (int i = 0; i < this.theRoleNames.size(); i++) {
            theNoErrors.add("");
        }
        errorHistory.add(theNoErrors);
    }

    public void markCompleted(List<Integer> theGoalValues) {
        this.isCompleted = true;
        if (theGoalValues != null) {
            this.goalValues.addAll(theGoalValues);
        }
    }

    /* Complex accessors */
    public List<GdlSentence> getMostRecentMoves() {
        if (moveHistory.size() == 0)
            return null;
        return moveHistory.get(moveHistory.size() - 1);
    }

    public Set<GdlSentence> getMostRecentState() {
        if (stateHistory.size() == 0)
            return null;
        return stateHistory.get(stateHistory.size() - 1);
    }

    public String getGameName() {
        return getGame().getName();
    }

    public String getGameRepositoryURL() {
        return getGame().getRepositoryURL();
    }


    /* Simple accessors */

    public String getMatchId() {
        return matchId;
    }

    public Game getGame() {
        return theGame;
    }

    public List<List<GdlSentence>> getMoveHistory() {
        return moveHistory;
    }

    public List<Set<GdlSentence>> getStateHistory() {
        return stateHistory;
    }

    public List<Date> getStateTimeHistory() {
        return stateTimeHistory;
    }

    public List<List<String>> getErrorHistory() {
        return errorHistory;
    }

    public int getPlayClock() {
        return playClock;
    }

    public int getStartClock() {
        return startClock;
    }

    public Date getStartTime() {
        return startTime;
    }

    public List<String> getRoleNames() {
        return theRoleNames;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public List<Integer> getGoalValues() {
        return goalValues;
    }
    
    public List<Move> getLastMoves() {
        return lastMoves;
    }

    /* Static methods */

    public static String getRandomString(int nLength) {
        Random theGenerator = new Random();
        String theString = "";
        for (int i = 0; i < nLength; i++) {
            int nVal = theGenerator.nextInt(62);
            if (nVal < 26)
                theString += (char) ('a' + nVal);
            else if (nVal < 52)
                theString += (char) ('A' + (nVal - 26));
            else if (nVal < 62)
                theString += (char) ('0' + (nVal - 52));
        }
        return theString;
    }
}
