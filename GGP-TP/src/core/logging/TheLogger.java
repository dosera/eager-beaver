/**
 * 
 */
package core.logging;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import core.Settings;

/**
 * @author Phil
 * Global Logger.
 */
public final class TheLogger {
    public final static Logger LOGGER = Logger.getLogger("global") ;
    
    public static void setup() throws SecurityException, IOException
    {
        
        // trys to create the logdir if not existent
        String subdir = "logs";
        File file = new File(subdir);
        boolean exists = file.exists();
        if(!exists)
        {
            boolean dir = new File(subdir).mkdir();
            if (!dir){
                System.err.print("error creating logdir");
                System.exit(0);
            }
        }
        
        LOGGER.setLevel(Level.ALL);
        
        // handles the logging stuff
        // filehandler
        String timeStamp = new SimpleDateFormat("yyMMdd-k.mm.ss").format(new Date());
        String filename = subdir + "/" + timeStamp + "-%g.log";
        if (Settings.logfilename.length() != 0)
        {
        	filename = subdir + "/" + Settings.logfilename + "--" + timeStamp + "-%g.log"; 
        }
        FileHandler fh = new FileHandler(filename, 30000000, 100);  
        fh.setFormatter(new FileFormatter());
         fh.setLevel(Settings.logLevelOfFileHandler);
        
        // consolehandler
        ConsoleHandler consolehandler = new ConsoleHandler();
        if(Settings.verbose)
        	consolehandler.setLevel(Level.FINE);
        else
        	consolehandler.setLevel(Level.CONFIG);
        consolehandler.setFormatter(new ConsoleFormatter());

            
        LOGGER.setUseParentHandlers(false);

        
        LOGGER.addHandler(consolehandler);
        LOGGER.addHandler(fh);
//        try{
//            String logpath = "logging.properties";
//            File fileLog = new File(logpath);
//            LogManager.getLogManager().readConfiguration(new FileInputStream(fileLog));
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }
    }
}
