package core.exceptions;

@SuppressWarnings("serial")
public class GameCycleException extends Exception {
    public GameCycleException(String msg) {
        super(msg);
    }
}