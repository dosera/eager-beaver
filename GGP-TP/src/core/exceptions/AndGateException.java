package core.exceptions;

@SuppressWarnings("serial")
/**
 * This exception is thrown if during propnetcode generation one and gate had more than 127 inputs.
 * Important, because we use byte for the and-gate input count.
 * @author flo
 *
 */
public class AndGateException extends Exception{
    @Override
    public String toString()
    {
            return "During writing propnet code an And Gate had more than 127 inputs";
    }

}
