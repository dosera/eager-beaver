package core.exceptions;

@SuppressWarnings("serial")
public final class MetaGamingException extends Exception
{

	@Override
	public String toString()
	{
		return "An unhandled exception occurred during metaGaming!";
	}

}
