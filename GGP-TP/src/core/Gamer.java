package core;

import java.util.ArrayList;
import java.util.List;

import core.exceptions.MetaGamingException;
import core.exceptions.MoveSelectionException;
import core.gdl.GdlProposition;
import core.gdl.GdlSentence;

/**
 * The Gamer class defines methods for both meta-gaming and move selection in a
 * pre-specified amount of time. The Gamer class is based on the <i>algorithm</i>
 * design pattern.
 */
public abstract class Gamer
{	
	private Match match;
	private GdlProposition roleName;

	public Gamer()
	{		
		// When not playing a match, the variables 'match'
		// and 'roleName' should be NULL. This indicates that
		// the player is available for starting a new match.
		match = null;
		roleName = null;
	}

	/* The following values are recommendations to the implementations
	 * for the minimum length of time to leave between the stated timeout
	 * and when you actually return from metaGame and selectMove. They are
	 * stored here so they can be shared amongst all Gamers. */
    public static final long PREFERRED_METAGAME_BUFFER = 3900;
    public static final long PREFERRED_PLAY_BUFFER = 1900;    
	
	// ==== The Gaming Algorithms ====
        // TODO: fix exceptions
	public abstract void metaGame(long timeout) throws MetaGamingException;
	
	public abstract GdlSentence selectMove(long timeout) throws MoveSelectionException;
	
	/* Note that the match's goal values will not necessarily be known when
	 * stop() is called, as we only know the final set of moves and haven't
	 * interpreted them yet. To get the final goal values, process the final
	 * moves of the game.
	 */
	public abstract void stop();
	
	// ==== Gamer Profile and Configuration ====
	public abstract String getName();
	
	// TODO: readd this?
	/*
	public ConfigPanel getConfigPanel() {
		return new EmptyConfigPanel();
	}
	
	public DetailPanel getDetailPanel() {
		return new EmptyDetailPanel();
	}
	*/

	// ==== Accessors ====	
	public final Match getMatch() {
		return match;
	}
	
	public final void setMatch(Match match) {
		this.match = match;
	}

	public final GdlProposition getRoleName() {
		return roleName;
	}
	
	public final void setRoleName(GdlProposition roleName) {
		this.roleName = roleName;
	}
}