package core.request.symbol;

/**
 * @author Sam Schreiber 
 * the abstract represenation of an element in a syntax tree
 */
public abstract class Symbol
{

	@Override
	public abstract String toString();

}
