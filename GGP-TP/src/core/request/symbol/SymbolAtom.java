package core.request.symbol;

/**
 * @author Sam Schreiber
 * an atom is an atomic symbol (leaf in the syntax tree)
 */
public final class SymbolAtom extends Symbol
{
	private final String value;

	SymbolAtom(String value)
	{
		this.value = value.intern();
	}

	public String getValue()
	{
		return value;
	}

	@Override
	public String toString()
	{
		return value;
	}

}
