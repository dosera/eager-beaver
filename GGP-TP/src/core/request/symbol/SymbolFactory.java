/**
 * 
 */
package core.request.symbol;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

import core.exceptions.SymbolFormatException;

/**
 * @author Sam Schreiber
 *
 */
public final class SymbolFactory {
    
    public static Symbol create(String string) throws SymbolFormatException
    {
        try
        {
            // first, we fix the whitespaces in the given string
            // then we extract the tokens (split by the spaces)
            // and return the converted symbol (atomic or a list itself)
            String fixedString = fixString(string);
            List<String> tokens = new ArrayList<String>();
            for (String token : fixedString.split(" "))
                tokens.add(token);
            return convert(new LinkedList<String>(tokens));
        }
        catch (Exception e)
        {
            throw new SymbolFormatException(string);
        }
    }
    
    /**
     * @param tokens
     * @return the symbol
     * converts the tokens into symbols and adds them to the collection (SymbolPool)
     */
    private static Symbol convert(LinkedList<String> tokens)
    {
        // we check whether we have an atomic expression or a nested function/relation
        if (tokens.getFirst().equals("("))
        {
            // this is handles recursively
            return convertList(tokens);
        }
        else
        {
            return SymbolPool.getAtom(tokens.removeFirst());
        }
    }
    
    /**
     * @param tokens
     * @return the list of symbols
     * recursively handles the list of symbols
     */
    private static SymbolList convertList(LinkedList<String> tokens)
    {
        List<Symbol> contents = new ArrayList<Symbol>();
        
        tokens.removeFirst();
        while (!tokens.getFirst().equals(")"))
        {
            contents.add(convert(tokens));
        }
        tokens.removeFirst();
        
        return SymbolPool.getList(contents);
    }
    
    /**
     * fixes the whitespaces in the input string
     * @param string the input string
     * @return the fixed string
     */
    private static String fixString(String string)
    {

        string = string.replaceAll("\\(", " ( ");
        string = string.replaceAll("\\)", " ) ");
        string = string.replaceAll("\\s+", " ");
        string = string.trim();
        
        return string;
    }
}
