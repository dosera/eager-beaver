/**
 * 
 */
package core.request;

import java.util.List;

import core.Gamer;
import core.gdl.*;

/**
 * @author Sam Schreiber
 *
 */
public final class AbortRequest extends Request {
    
    private final Gamer gamer;
    private final String matchId;
    
    public AbortRequest(Gamer gamer, String matchId)
    {
        this.gamer = gamer;
        this.matchId = matchId;
    }

    @Override
    public String process(long receptionTime) {
        
        // check whether the match is valid
        if (gamer.getMatch() == null || !gamer.getMatch().getMatchId().equals(matchId))
            return "busy";
        
        gamer.setMatch(null);
        gamer.setRoleName(null);
        
        return "aborted";
    }

    
    @Override
    public String getMatchId() {
        return matchId;
    }

    @Override
    public String toString() {
        return "abort";
    }

}
