/**
 * 
 */
package core.request;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import core.Game;
import core.Gamer;
import core.gdl.Gdl;
import core.gdl.GdlFactory;
import core.gdl.GdlProposition;
import core.gdl.GdlSentence;
import core.request.symbol.Symbol;
import core.request.symbol.SymbolFactory;
import core.request.symbol.SymbolList;
import core.request.symbol.SymbolAtom;
import core.logging.TheLogger;;

/**
 * @author Sam Schreiber
 * 
 */
public class RequestFactory {
	
	
    // this function creates a new request according to
    // the message received by the server
    public Request create(Gamer gamer, String source) throws Exception
    {
        try
        {
            // we transform the given string into a syntaxtree-like representation
            SymbolList list = (SymbolList) SymbolFactory.create(source);
            SymbolAtom head = (SymbolAtom) list.get(0);
            
            String type = head.getValue().toLowerCase();
            // react accordingly to the message sent by the server
            
            // TODO: Change this back! 
            // USED FOR JAVA 1.6
            int a = 0;
            if(type.equalsIgnoreCase("start")) a = 1;
            if(type.equalsIgnoreCase("play")) a = 2;
            if(type.equalsIgnoreCase("stop")) a = 3;
            if(type.equalsIgnoreCase("ping")) a = 4;
            if(type.equalsIgnoreCase("abort")) a = 5;
            
            switch (a)
            {
                case 1:
                    // in this case, a new game is started
                    TheLogger.LOGGER.log(Level.FINEST, "processing startmessage from the server");
                    return createStart(gamer, list);
                case 2:
                    // the game is started
                    TheLogger.LOGGER.log(Level.FINEST, "processing playmessage from the server");
                    return createPlay(gamer, list);
                case 3:
                    // the stop command was issued by the server
                    TheLogger.LOGGER.log(Level.FINEST, "processing stopmessage from the server");
                    return createStop(gamer, list);
                case 4:
                    // the server pinged the gamer
                    TheLogger.LOGGER.log(Level.FINEST, "processing pingmessage from the server");
                    return createPing(gamer, list);
                case 5:
                    // 
                    TheLogger.LOGGER.log(Level.FINEST, "processing abortmessage");
                    return createAbort(gamer, list);
                default:
                    throw new IllegalArgumentException("Unrecognized request type!");
            }
        }
        catch (Exception e)
        {
            // TODO: make sure to handle this correctly
            throw new Exception("ERROR");
        }
    }

    private Request createAbort(Gamer gamer, SymbolList list) throws Exception {
        
        if (list.size() != 2)
            throw new IllegalArgumentException("Expected exactly 1 argument!");
        
        SymbolAtom arg1 = (SymbolAtom) list.get(1);       
        String matchId = arg1.getValue();
        TheLogger.LOGGER.config("game aborted by server");
        return new AbortRequest(gamer, matchId);
    }

    /**
     * this method handles the "ping" request 
     * @param gamer
     * @param list
     * @return
     */
    private Request createPing(Gamer gamer, SymbolList list) {
        if (list.size() != 1)
            throw new IllegalArgumentException("Expected exactly 0 arguments!");
        
        return new PingRequest(gamer);
    }

    /**
     * this method handles the request to the "stop" command sent by the server 
     * (stops the game)
     * @param gamer
     * @param list
     * @return
     * @throws Exception
     */
    private StopRequest createStop(Gamer gamer, SymbolList list) throws Exception {
        
        if (list.size() != 3)
            throw new IllegalArgumentException("Expected exactly 2 arguments!");
        
        SymbolAtom arg1 = (SymbolAtom) list.get(1);
        Symbol arg2 = list.get(2);
        
        String matchId = arg1.getValue();
        List<GdlSentence> moves = parseMoves(arg2);
        TheLogger.LOGGER.config("game stopped by server: " + list.get(0).toString());
        return new StopRequest(gamer, matchId, moves);
    }

    /**
     * this method handles the request to the "play" command sent by the server 
     * (starts the actual initialized game)
     * @param   gamer
     * @param   list
     * @return  
     * @throws Exception 
     */
    // TODO: better exception
    private PlayRequest createPlay(Gamer gamer, SymbolList list) throws Exception {
        
        if (list.size() != 3)
            throw new IllegalArgumentException("Expected exactly 2 arguments!");
        
        SymbolAtom arg1 = (SymbolAtom) list.get(1);
        Symbol arg2 = list.get(2);
        
        String matchId = arg1.getValue();
        List<GdlSentence> moves = parseMoves(arg2);
        
        return new PlayRequest(gamer, matchId, moves);
    }

    /**
     * @param list
     * @return StartRequest
     * this method handles the request to start a new game
     * @throws Exception 
     */
    // TODO: exception
    private StartRequest createStart(Gamer gamer, SymbolList list) throws Exception {
        
        // TODO: exceptions
        if (list.size() < 6)
            throw new IllegalArgumentException("Expected at least 5 arguments!");
        
        
        SymbolAtom arg1 = (SymbolAtom) list.get(1);
        SymbolAtom arg2 = (SymbolAtom) list.get(2);
        SymbolList arg3 = (SymbolList) list.get(3);
        SymbolAtom arg4 = (SymbolAtom) list.get(4);
        SymbolAtom arg5 = (SymbolAtom) list.get(5);
        
        String matchId = arg1.getValue();
        GdlProposition roleName = (GdlProposition) GdlFactory.create(arg2);
        List<Gdl> theRules = parseDescription(arg3);
        int startClock = Integer.valueOf(arg4.getValue());
        int playClock = Integer.valueOf(arg5.getValue());
        
        // TODO: There may be more than five arguments. These may be worth
        // parsing, once we find a meaningful way to handle them. They aren't
        // yet standardized, but, for example, one might be the URL of an XSL
        // stylesheet for visualizing a state of the game, or the URL for the
        // game on a repository server.

        Game theReceivedGame = Game.createEphemeralGame(theRules);
        TheLogger.LOGGER.config("initializing match, ID: "+ matchId + ", role: " + roleName + ", startclock: " + startClock + ", playclock: " + playClock);
        return new StartRequest(gamer, matchId, roleName, theReceivedGame, startClock, playClock);
    }

    
    // Parsing functions
    
    // TODO: fix exception
    
    /**
     * parses the previous move which is sent by the server in the "play" command
     * @param symbol
     * @return
     * @throws Exception
     */
    private List<GdlSentence> parseMoves(Symbol symbol) throws Exception
    {
        if (symbol instanceof SymbolAtom)
            return null;
        else
        {
            List<GdlSentence> moves = new ArrayList<GdlSentence>();
            SymbolList list = (SymbolList) symbol;
            
            for (int i = 0; i < list.size(); i++)
                moves.add((GdlSentence) GdlFactory.create(list.get(i)));
            
            return moves;
        }
    }
    
    // TODO: exception, kommentar
    private List<Gdl> parseDescription(SymbolList list) throws Exception {
        List<Gdl> description = new ArrayList<Gdl>();
        for (int i = 0; i < list.size(); i++)
        {
            description.add(GdlFactory.create(list.get(i)));
        }
        
        return description;
    }

}
