package core.request;


/**
 * @author Sam Schreiber
 * this class represents an abstract representation of a request according
 * to the different messages received by the server. 
 */
public abstract class Request {

	public abstract String process(long receptionTime);

	public abstract String getMatchId();
	
	@Override
	public abstract String toString();

}
