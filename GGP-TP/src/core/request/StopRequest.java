/**
 * 
 */
package core.request;

import java.util.List;

import core.Gamer;
import core.gdl.*;

/**
 * @author Sam Schreiber
 *
 */
public final class StopRequest extends Request {
    
    private final Gamer gamer;
    private final String matchId;
    private final List<GdlSentence> moves;
    
    public StopRequest(Gamer gamer, String matchId, List<GdlSentence> moves)
    {
        this.gamer = gamer;
        this.matchId = matchId;
        this.moves = moves;
    }

    @Override
    public String process(long receptionTime) {
        
        // check whether the match is valid
        if (gamer.getMatch() == null || !gamer.getMatch().getMatchId().equals(matchId))
            return "busy";
        
        if (moves != null)
            gamer.getMatch().appendMoves(moves);
        
        gamer.getMatch().markCompleted(null);
        gamer.stop();
        
        gamer.setMatch(null);
        gamer.setRoleName(null);
        
        return "done";
    }

    
    @Override
    public String getMatchId() {
        return matchId;
    }

    @Override
    public String toString() {
        return "stop";
    }

}
