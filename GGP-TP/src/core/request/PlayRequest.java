/**
 * 
 */
package core.request;

import java.util.List;
import java.util.logging.Level;

import core.Gamer;
import core.Settings;
import core.gdl.*;
import core.logging.TheLogger;

/**
 * @author Sam Schreiber
 *
 */
public final class PlayRequest extends Request {
    
    private final Gamer gamer;
    private final String matchId;
    private final List<GdlSentence> moves;
    
    public PlayRequest(Gamer gamer, String matchId, List<GdlSentence> moves)
    {
        this.gamer = gamer;
        this.matchId = matchId;
        this.moves = moves;
    }

    @Override
    public String process(long receptionTime) {
        // check whether the match is valid
        if (gamer.getMatch() == null || !gamer.getMatch().getMatchId().equals(matchId)) {
            return "busy";
        }
        if (moves != null)
        {
            gamer.getMatch().appendMoves(moves);
        }
        
        try
        {
        	// used to check if the time limit is being exceeded by the UCT algorithm
        	long finishUntilDate = gamer.getMatch().getPlayClock() * 1000 + receptionTime - Settings.computationthreshold;
        	String move = gamer.selectMove(finishUntilDate).toString();
            long endtime = System.currentTimeMillis();
            if (endtime > finishUntilDate + Settings.computationthreshold)
            	TheLogger.LOGGER.log(Level.SEVERE, "timelimit exceeded by " + (endtime - finishUntilDate) + " ms");
            return move;
        } catch (Exception e) {
            e.printStackTrace();
            return "nil";
        }
    }

    
    @Override
    public String getMatchId() {
        return matchId;
    }

    @Override
    public String toString() {
        return "play";
    }

}
