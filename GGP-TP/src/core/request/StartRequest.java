/**
 * 
 */
package core.request;

import core.Game;
import core.Gamer;
import core.Match;
import core.Settings;
import core.gdl.GdlProposition;
import core.logging.TheLogger;

/**
 * @author Sam Schreiber
 *
 */
public final class StartRequest extends Request {

    private final Game game;
    private final Gamer gamer;
    private final String matchId;
    private final int playClock;
    private final GdlProposition roleName;
    private final int startClock;
    
    public StartRequest(Gamer gamer, String matchId, GdlProposition roleName, Game theGame, int startClock, int playClock)
    {
        this.gamer = gamer;
        this.matchId = matchId;
        this.roleName = roleName;
        this.game = theGame;
        this.startClock = startClock;
        this.playClock = playClock;
    }
    
    @Override
    public String process(long receptionTime) {
        // ensure that we aren't already playing a match, if we are ignore the message saying that we are busy
        if (gamer.getMatch() != null)
            return "busy";
        
        // create a new match and handle all the associated logistics
        Match match = new Match(matchId, startClock, playClock, game);
        gamer.setMatch(match);
        gamer.setRoleName(roleName);
        
        // we subtract "time" to ensure that the server communication 
        // has enough time to succeed
        // 10%
        try {
            gamer.metaGame(gamer.getMatch().getStartClock()* 1000 + receptionTime - gamer.getMatch().getStartClock() * 100);        
        }
        catch (Exception e) {
            // upon encountering an uncaught exception during metagaming,
            // assume that indicates that we aren't actually able to play
            // tell this to the server
            gamer.setMatch(null);
            gamer.setRoleName(null);
            e.printStackTrace();
            return "busy";
        }
        return "ready";
    }

    @Override
    public String getMatchId() {
        return matchId;
    }

    @Override
    public String toString() {
        return "start";
    }

}
