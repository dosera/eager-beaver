/**
 * 
 */
package core;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.logging.Level;

import core.http.HttpReader;
import core.http.HttpWriter;
import core.logging.TheLogger;
import core.request.AbortRequest;
import core.request.Request;
import core.request.RequestFactory;
import core.request.StopRequest;

/**
 * @author Phil
 *
 */
public class GamePlayer extends Thread {

    private int port;
    private ServerSocket listener;
    private final Gamer gamer;
    int maxTries = 1000;
    int maxListening = 5;

    public GamePlayer(int port, Gamer gamer)  {
        
        listener = null;
        
        // open a new socket on the given port
        while (listener == null && maxTries > 0) {
            try {
                listener = new ServerSocket(port);
            } catch (IOException ex) {
                // in this case, the port isn't available
                // we try the next one
                listener = null;
                port++;
                maxTries--;
                TheLogger.LOGGER.warning("failed to start gamer on port: " + (port-1) + " trying port " + port);
            }       
        }
        
        this.port = port; 
        this.gamer = gamer;
    }
    
    public void run()
    {  
        TheLogger.LOGGER.info("listening to port " + port);
        while (!isInterrupted() && (Settings.listenForever || maxListening > 0))
        {
            try
            {

                listener.setSoTimeout(10000);
                Socket connection = listener.accept();
                
                if(!listener.isBound())
                	break;
                
                TheLogger.LOGGER.finest("server connection established to " + listener.getInetAddress());
                // process the incoming message
                String in = HttpReader.readAsServer(connection);
                
                if (in.length() == 0) {
                    throw new IOException("Empty message received.");
                }
                
                 TheLogger.LOGGER.config("received: " + in);
                 Request request = new RequestFactory().create(gamer, in);
                 long startTime = System.currentTimeMillis();
                 
                 String out = request.process(System.currentTimeMillis());
                 
                 // send the response back to the server [and also to the logger]
                 HttpWriter.writeAsServer(connection, out);
                 TheLogger.LOGGER.config("response after " + (System.currentTimeMillis() - startTime) + " ms: " + out);
                 connection.close();
                 if(!Settings.listenForever && (request instanceof StopRequest || request instanceof AbortRequest))
                 {
                	 TheLogger.LOGGER.config("received {stop, abort} request, shutting down the player ...");
                	 break;
                 }
            }
            catch (SocketTimeoutException e)
            {
            	TheLogger.LOGGER.log(Level.FINEST, "connection timeout, retrying (attempts left: " + maxListening + ")");
                maxListening--;
            }
            catch (Exception e)
            {
                TheLogger.LOGGER.log(Level.SEVERE, "An error occured while contacting the server", e);
            }
        }
        
    }
    

}
