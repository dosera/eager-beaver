/**
 * 
 */
package core;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import players.uct.UCTSubPlayer;
import players.uct.evaluation.AEvaluation;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.methods.CombinationHeuristic;
import players.uct.evaluation.methods.FocusHeuristic;
import players.uct.evaluation.methods.MobilityHeuristic;
import players.uct.evaluation.methods.RandomHeuristic;
import players.uct.expansion.AExpansion;
import players.uct.expansion.ExpandAllNodes;
import players.uct.expansion.ExpandNodebyNode;
import players.uct.selection.ASelection;
import players.uct.selection.UCB1Computation;
import players.uct.selection.UCBTuned1Computation;
import players.uct.selection.UCBTuned2Computation;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.methods.*;
import propnet.CachedPropNetStateMachine;
import propnet.PropNetStateMachine;
import propnet.SuperPropNetStateMachine;
import statemachine.StateMachine;
import statemachine.prover.CachedProverStateMachine;
import statemachine.prover.ProverStateMachine;
import core.logging.TheLogger;

/**
 * @author tasse
 * 
 */
public final class Settings {
	
	// normalization of the scores (100 -> 1 etc.)
	public static final float SCORE_NORMALIZER = 0.01f;
	// name of the player
	static String playerName = "UCTPlayer";
	// port
	static int port = 4001;
	// visualization
	public static int visUCTDepth = 3;
	public static String visUCTFilename = "uct_vis";
	public static String visPNETFilename = "pnet";
	public static boolean drawUCT, drawPNET = false;
	// verbose logging
	public static boolean verbose = false;
	// prover
	public static String prover = "cpnet";
	// gamer
	public static String gamer = "uct";
	// scheduler
	public static int numThreads = -1;
	// listen forever
	public static boolean listenForever = false;
	// assertions
	public static boolean assertions = false;
	// strategy
	// [type,expansion,selection,updatebias,heuristic:depth]
	public static String strategy = "rave:nbn,ucb1,true,none";
	public static String newStrategy;
	public static AStrategyHandler defaultStrategy;
	public static AExpansion defaultExpansion;
	public static ASelection defaultSelection;
	public static boolean updateBias = false;
	public static AEvaluation defaultHeuristic;
	public static int evaluationDepth = 0;
	public static boolean maximize = true;
	public static int overvisited = 10;
	public static String logfilename = "";
	public static Level logLevelOfFileHandler = Level.ALL;
	public static boolean betterBeta = false;
	// threshold for server communication
	public static long computationthreshold = 1000;
	// constant for RAVE - k
	public static float k = 1000;
	// constant for MAST - tau
	public static float tau = .1f;
	public static boolean treeOnlyMast = false;
	// treeswapping enabled?
	public static boolean treeswapping = true;
	// avg?
	public static boolean useAveragesForUCT = false;
	// MATCH ID (used for FAST..)
	public static String matchID = "";

	
	public static void setup(String[] args) throws ParseException {
		
	    // create the parser
	    CommandLineParser parser = new GnuParser();
	    Options options = new Options();
	    
	    // all my options live in texas
	    // help
	    options.addOption("h", "help", false, "displays the help");
	    
	    OptionBuilder.withArgName("p");
		OptionBuilder.withLongOpt("port");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("the port the player is listening to");
		// port
	    Option portOption = OptionBuilder.create("p");
	    options.addOption(portOption);
	    
	    // visualization
	    
	    OptionBuilder.withArgName("depth");
		OptionBuilder.hasArg();
		OptionBuilder.
	    		withDescription("enables exportation to graphviz format of the UCT gaming tree, args: depth");
		Option visUCTOption = OptionBuilder.create("vuct");
	    options.addOption(visUCTOption);
	    options.addOption("vpnet", false, "enables exportation to graphviz format of the propnet");
	    
	    // logginglevel
	    options.addOption("v", "verbose", false, "verbose, sets the logging level to FINE for the console output");
	    
	    OptionBuilder.withArgName("prover");
		OptionBuilder.hasArg();
		OptionBuilder.
	    		withDescription("chooses the prover, args: prover {p, cp, pnet, cpnet (default), sppnet}");
		// which prover/propnet
	    Option proverOption = OptionBuilder.create("prover");
	    options.addOption(proverOption);
	    
	    OptionBuilder.withArgName("g");
		OptionBuilder.withLongOpt("gamer");
		OptionBuilder.hasArg();
		OptionBuilder.
	    		withDescription("chooses the gamer, args: gamer {uct (default), mc, random}");
		// which gamer [UCT, SimpleMC, Random...]
	    Option gamerOption = OptionBuilder.create("g");
	    options.addOption(gamerOption);
	    
	    OptionBuilder.withArgName("numthreads");
		OptionBuilder.withLongOpt("scheduler");
		OptionBuilder.hasArg();
		OptionBuilder.
	    		withDescription("if set, threading is activated, args: number of threads");
		// which scheduler
	    Option schedulerOption = OptionBuilder.create("c");
	    options.addOption(schedulerOption);
	    
	    // listen forever?
	    options.addOption("f", "forever", false, "shall the player listen forever and NOT stop after a game is finished?");
	    
	    // log name
	    Option logfileOption = OptionBuilder.withArgName("filename").
	    		withLongOpt("logfile").hasArg().
	    		withDescription( "specifies a file to log to").create("l");
	    options.addOption(logfileOption);
	    
	    // log level
	    Option logLevelOption = OptionBuilder.withArgName("loggingLevel").
	    		withLongOpt("logLevel").hasArg().
	    		withDescription( "specifies the file logging level {all (default), severe, warning, info, config, fine, finer, finest, off}").create("lvl");
	    options.addOption(logLevelOption);
	    
	    // assertions
	    options.addOption("ea", "assertions", false, "runs the programm with assertions");
	    
	    OptionBuilder.withArgName("arg");
		OptionBuilder.
	    		withLongOpt("strategy");
		OptionBuilder.hasArg();
		OptionBuilder.
	    		withDescription("players strategy, where arg: [type,expansion,selection,updatebias,heuristic:depth], (default: " +
	    				strategy +")");
		// strategy
	    Option strategyOption = OptionBuilder.create("s");
	    options.addOption(strategyOption);
	    
	    // minimize the opponent?
	    options.addOption("m", "minimize", false, 
	    		"we minimize our own score, if its the opponents turn (default: we maximize the opponents score if its his turn) ");
	    OptionBuilder.withArgName("#");
		OptionBuilder.
	    		withLongOpt("overvisited");
		OptionBuilder.hasArg();
		OptionBuilder.
	    		withDescription("how often has a node to be selected # more times as its siblings until it will be locked (default: "
	    				+ overvisited + ")");
		
		// set the overvisited parameter
	    Option overvisitedOption = OptionBuilder.create("o");
	    options.addOption(overvisitedOption);
	    
	    OptionBuilder.withArgName("z");
		OptionBuilder.withLongOpt("threshold");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("the threshold that ensures successful server communication");
	    // set the computation threshold
	    Option computationthreshold = OptionBuilder.create("z");
	    options.addOption(computationthreshold);
	    
	    // betterbeta if rave
	    options.addOption("b", "better Beta", false, "enables the better beta formula (~ -20% visits!)");
	    
	    OptionBuilder.withArgName("k");
		OptionBuilder.withLongOpt("raveconstant");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("sets the rave constant (default: 1000), has no impact if strategy is not RAVE!");
	    // set the k value for RAVE
	    Option raveconstant = OptionBuilder.create("k");
	    options.addOption(raveconstant);
	    
	    // disable tree swapping
	    options.addOption("t", "tree-swapping", false, 
	    		"disables tree swapping if set");
	    
	    // tree only MAST?
	    options.addOption("to", "tree-only-mast", false, 
	    		"enables tree only mast iff set");
	    
	    // Averages for UCT?
	    options.addOption("avg", "useAveragesForUCT", false,
	    		"using averages (wins/visits) for UCT instead of just visits");
	   
	    
	    // parse the command line arguments
	    try
	    {
	    	setValues(parser.parse(options, args), options);
	    }
	    catch (Exception e)
	    {
	       HelpFormatter formatter = new HelpFormatter();
	 	   formatter.printHelp(playerName, options );
	 	   Thread.currentThread().stop();
	    }
	    
	}

	private static void setValues(CommandLine line, Options options) {
	    if(line.hasOption("h")) {
	 	// automatically generate the help statement
	 	   HelpFormatter formatter = new HelpFormatter();
	 	   formatter.printHelp( playerName, options );
	 	   Thread.currentThread().stop();
	    }
	    if(line.getOptionValue("p") != null )
		 	   port = Integer.parseInt(line.getOptionValue("p"));
	    if(line.getOptionValue("vuct") != null )
	    {
	    	visUCTDepth = Integer.parseInt(line.getOptionValue("vuct"));
	    	drawUCT = true;
	    }
	    if(line.hasOption("vpnet"))
	    	drawPNET = true;
	    if(line.hasOption("v"))
	    	verbose = true;
	    if(line.hasOption("b"))
	    	betterBeta = true;
	    if(line.hasOption("f"))
	    	listenForever = true;
	    if(line.hasOption("ea"))
	    	assertions = true;
	    if(line.hasOption("t"))
	    	treeswapping = false;
	    if(line.hasOption("to"))
	    	treeOnlyMast = true;
	    if(line.hasOption("avg"))
	    	useAveragesForUCT  = true;
	    // which prover
	    if(line.getOptionValue("prover") != null )
	    {
	    	String[] possibleProvers = new String[]{"p","cp","pnet","cpnet","sppnet"};
	    	boolean found = false;
	    	for(String s : possibleProvers)
	    		if(s.equalsIgnoreCase(line.getOptionValue("prover")))
	    			found = true;
	    	if(found)
	    		prover = line.getOptionValue("prover");
	    	
	    }
	    // set logfile name 
	    if (line.getOptionValue("l") != null)
	    {
	    	logfilename = line.getOptionValue("l");
	    }
	    // set loglevel
	    if (line.getOptionValue("lvl") != null)
	    {
	    	logLevelOfFileHandler = Level.parse(line.getOptionValue("lvl").toUpperCase());
	    }
	    // which gamer
	    if(line.getOptionValue("gamer") != null )
	    {
	    	String[] possibleGamers = new String[]{"uct","mc", "random"};
	    	boolean found = false;
	    	for(String s : possibleGamers)
	    		if(s.equalsIgnoreCase(line.getOptionValue("gamer")))
	    			found = true;
	    	if(found)
	    		gamer = line.getOptionValue("gamer");
	    }
	    // which scheduler
	    if(line.getOptionValue("c") != null )
	    	numThreads = Integer.parseInt(line.getOptionValue("c"));
	    
	    // strategy
	    if(line.getOptionValue("s") != null)
	    {
	    	newStrategy = line.getOptionValue("s");
	    }
	    // max/minimize
	    if(line.getOptionValue("m") != null)
	    		maximize = false;
	    // overvisited
	    if(line.getOptionValue("o") != null)
	    	overvisited = Integer.parseInt(line.getOptionValue("o"));
	    // computationthreshold
	    if(line.getOptionValue("z") != null)
	    	computationthreshold = Long.parseLong(line.getOptionValue("z"));
	    // k value for rave
	    if(line.getOptionValue("k") != null)
	    	k = Float.parseFloat(line.getOptionValue("k"));
	    
	    // RAVE: if betterbeta is set, k is the raveBias
		if(betterBeta) k = 1;
	}

	public static StateMachine getStateMachine() {
		if(Settings.prover.equalsIgnoreCase("p"))
			return new ProverStateMachine();
		if(Settings.prover.equalsIgnoreCase("cp"))
			return new CachedProverStateMachine();
		if(Settings.prover.equalsIgnoreCase("pnet"))
			return new PropNetStateMachine();
		if(Settings.prover.equalsIgnoreCase("cpnet"))
			return new CachedPropNetStateMachine();
		if(Settings.prover.equalsIgnoreCase("sppnet"))
			return new SuperPropNetStateMachine();
		return new CachedPropNetStateMachine();
	}

	public static AStrategyHandler setUpStrategy(UCTSubPlayer sp) {
		try
		{
			return parseStrategy(newStrategy, sp);
		}
		catch (Exception e)
		{
			TheLogger.LOGGER.log(Level.SEVERE, "error while setting up strategy: ", e);
			AStrategyHandler defaultStrategy = new RaveStrategy(sp, new ExpandNodebyNode(), new UCB1Computation(), new RandomHeuristic());
			TheLogger.LOGGER.log(Level.CONFIG, "error: strategy was not given in correct syntax, using default strategy: " + defaultStrategy.toString());
			return defaultStrategy;
		}
	}

	// rave:nbn,ucb1,true,none
	private static AStrategyHandler parseStrategy(String toParse, UCTSubPlayer sp) throws Exception {
		String type = toParse.split(":")[0];
		String[] args = toParse.split(":")[1].split(",");
		
		AExpansion expansion = null;
		if(args[0].equalsIgnoreCase("nbn"))
			expansion = new ExpandNodebyNode();
		else if(args[0].equalsIgnoreCase("ean"))
			expansion = new ExpandAllNodes();
		
		ASelection selection = null;
		if(args[1].equalsIgnoreCase("ucb1"))
			selection = new UCB1Computation();
		else if(args[1].equalsIgnoreCase("ucbt1"))
			selection = new UCBTuned1Computation();
		else if(args[1].equalsIgnoreCase("ucbt2"))
			selection = new UCBTuned2Computation();
		
		if(args[2].equalsIgnoreCase("true"))
			updateBias = true;
		
		AHeuristic heuristic = null;
		if(args[3].equalsIgnoreCase("none"))
		{
			heuristic = new RandomHeuristic();
			evaluationDepth = 0;
		}
		else if(args[3].split("->")[0].equalsIgnoreCase("rh"))
		{
			heuristic = new RandomHeuristic();
			evaluationDepth = Integer.parseInt(args[3].split("->")[1]);
		}
		else if(args[3].split("->")[0].equalsIgnoreCase("mh"))
		{
			heuristic = new MobilityHeuristic();
			evaluationDepth = Integer.parseInt(args[3].split("->")[1]);
		}
		else if(args[3].split("->")[0].equalsIgnoreCase("fh"))
		{
			heuristic = new FocusHeuristic();
			evaluationDepth = Integer.parseInt(args[3].split("->")[1]);
		}
		else if(args[3].split("->")[0].equalsIgnoreCase("mfh"))
		{
			List<AHeuristic> list = new ArrayList<AHeuristic>();
			list.add(new MobilityHeuristic());
			list.add(new FocusHeuristic());
			List<Float> weight = new ArrayList<Float>();
			weight.add(.5f);
			weight.add(.5f);
			heuristic = new CombinationHeuristic(list,weight);
			
			evaluationDepth = Integer.parseInt(args[3].split("->")[1]);
		}
		
		if(expansion == null || heuristic == null || selection == null)
			throw new Exception();
		
		// handlers
		AStrategyHandler handler = null;
		if(type.equalsIgnoreCase("def"))
			handler = new DefaultStrategy(sp, selection, expansion, heuristic);
		else if (type.equalsIgnoreCase("mast"))
			handler = new MastStrategy(sp, selection, expansion, heuristic);
		else if (type.equalsIgnoreCase("fast"))
			handler = new FastStrategy(sp, selection, expansion, heuristic);
		else if (type.equalsIgnoreCase("rave"))
			handler = new RaveStrategy(sp, expansion, selection, heuristic);
		else if (type.equalsIgnoreCase("ravemast"))
			handler = new RaveMastStrategy(sp, expansion, selection, heuristic);
		else if (type.equalsIgnoreCase("past"))
			handler = new PastStrategy(sp, selection, expansion, heuristic);
		else if (type.equalsIgnoreCase("ravepast"))
			handler = new RavePastStrategy(sp, expansion, selection, heuristic);
		else if (type.equalsIgnoreCase("ravefast"))
			handler = new RaveFastStrategy(sp, expansion, selection, heuristic);
		else if (type.equalsIgnoreCase("fastmast"))
			handler = new FastMastStrategy(sp, expansion, selection, heuristic);
		else if (type.equalsIgnoreCase("fastpast"))
			handler = new FastPastStrategy(sp, expansion,selection, heuristic);
		else if (type.equalsIgnoreCase("ravepastfast"))
			handler = new RavePastFastStrategy(sp, expansion,selection, heuristic);
		// handler-logging
		TheLogger.LOGGER.log(Level.CONFIG, "using strategy: " + handler.toString());

		return handler;
	}
}
