/**
 *
 */
package core;

import java.io.IOException;

import org.apache.commons.cli.ParseException;

import players.RandomGamer;
import players.SimpleMonteCarloGamer;
import players.uct.UCTGamer;
import statemachine.StateMachineGamer;
import core.logging.TheLogger;

/**
 * @author flo
 * 
 */
public class Main {

	/**
	 * @param args
	 * @throws IOException
	 *             a
	 * @throws ParseException
	 */
	public static void main(String[] args) throws IOException, ParseException {
		Settings.setup(args);
		TheLogger.setup();

		// which gamer is set by the commandline?
		StateMachineGamer gamer = new UCTGamer();
		if (Settings.gamer.equalsIgnoreCase("mc"))
			gamer = new SimpleMonteCarloGamer();
		if (Settings.gamer.equalsIgnoreCase("random"))
			gamer = new RandomGamer();

		// runs the Player (with the port specified in the Settings class)
		GamePlayer gamePlayer = new GamePlayer(Settings.port, gamer);
		gamePlayer.run();
	}
}
