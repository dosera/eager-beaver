package propnet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;
import statemachine.prover.TtlCache;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;

/**
 * A CachedPropNetStateMachine is based on a PropNetStateMachine but
 * caches its visisted States, so that you dont have to compute them again.
 * This is the same approach as the cachedproverstatemachine.
 * @author flo
 *
 */
public class CachedPropNetStateMachine extends PropNetStateMachine {

    private final class Entry
    {
            public Map<Role, Integer> goals;
            public Map<Role, List<Move>> moves;
            public Map<List<Move>, MachineState> nexts;
            public Boolean terminal;


            public Entry()
            {
                    goals = new HashMap<Role, Integer>();
                    moves = new HashMap<Role, List<Move>>();
                    nexts = new HashMap<List<Move>, MachineState>();
                    terminal = null;
            }
    }

    private final TtlCache<MachineState, Entry> ttlCache;

    public CachedPropNetStateMachine()
    {
            ttlCache = new TtlCache<MachineState, Entry>(1);
    }

    private Entry getEntry(MachineState state)
    {
            if (!ttlCache.containsKey(state))
            {
                    ttlCache.put(state, new Entry());
            }

            return ttlCache.get(state);
    }

    @Override
    public int getGoal(MachineState state, Role role) throws GoalDefinitionException
    {
            Entry entry = getEntry(state);
            {
                    if (!entry.goals.containsKey(role))
                    {
                            entry.goals.put(role, super.getGoal(state, role));
                    }
                    keepCacheSize();
                    return entry.goals.get(role);
            }
    }

    @Override
    public List<Move> getLegalMoves(MachineState state, Role role) throws MoveDefinitionException
    {
            Entry entry = getEntry(state);
            {
                    if (!entry.moves.containsKey(role))
                    {
                            entry.moves.put(role, super.getLegalMoves(state, role));
                    }
                    keepCacheSize();
                    return entry.moves.get(role);
            }
    }


    
    @Override
    public MachineState getNextState(MachineState state, List<Move> moves) throws TransitionDefinitionException
    {
            
            Entry entry = getEntry(state);
            {
                    if (!entry.nexts.containsKey(moves))
                    {
                            entry.nexts.put(moves, super.getNextState(state, moves));
                    }
                    keepCacheSize();
                    return entry.nexts.get(moves);
            }
    }

    @Override
    public boolean isTerminal(MachineState state)
    {
            Entry entry = getEntry(state);
            {
                    if (entry.terminal == null)
                    {
                            entry.terminal = super.isTerminal(state);
                    }
                    keepCacheSize();
                    return entry.terminal;
            }
    }
    
    @Override
    public void doPerMoveWork()
    {
            prune();
    }

    public void prune()
    {
            ttlCache.prune();
    }

    
    /**
     * For some reason the generated code needs a very long time to load if the
     * cache gets too large. Since we currently don't have any work per move,
     * we try to keep a cachesize of 10k before we prune.
     */
    private void keepCacheSize(){
    	if (ttlCache.size() > 2500) { // before: 10000 without gc()
    		prune();
    		// TODO: adjust cachesize 
    		Runtime.getRuntime().gc();
    	}
    }
    
}


