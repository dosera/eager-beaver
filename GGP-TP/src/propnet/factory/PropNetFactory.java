package propnet.factory;

import java.util.List;
import java.util.logging.Level;

import core.gdl.Gdl;
import core.gdl.GdlRule;
import core.logging.TheLogger;
import propnet.PropNet;
import propnet.factory.PropNetConverter;
import propnet.factory.PropNetFlattener;

/**
 * The PropNetFactory class defines the creation of PropNets from game
 * descriptions.
 */
@SuppressWarnings("unused")
public final class PropNetFactory
{
        /**
         * Creates a PropNet from a game description using the following process:
         * <ol>
         * <li>Flattens the game description to remove variables.</li>
         * <li>Converts the flattened description into an equivalent PropNet.</li>
         * </ol>
         * @authorcomment flo: since roles cant be computed when they are given as rules, as it is in a flattened
         * description, we give the old description to convert, so we can compute the roles.
         * we can probably make some improvements for that, but it suffices for now.
         * 
         * @param description
         *            A game description.
         * @return An equivalent PropNet.
         */
        public static PropNet create(List<Gdl> description)
        {
        try {
            List<GdlRule> flatDescription = new PropNetFlattener(description).flatten();
            TheLogger.LOGGER.log(Level.FINE, "StateMachine", "Converting...");
            return new PropNetConverter().convert(flatDescription, description);
        } catch(Exception e) {
            TheLogger.LOGGER.log(Level.FINE, "StateMachine", e);
            return null;
        }
        }
}