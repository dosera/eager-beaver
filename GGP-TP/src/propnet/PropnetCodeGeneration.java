package propnet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import core.exceptions.AndGateException;

import propnet.architecture.And;
import propnet.architecture.Component;
import propnet.architecture.Not;
import propnet.architecture.Or;
import propnet.architecture.Proposition;
import propnet.architecture.Transition;

/**
 * this class creates a .java file for a given propnet, which can be compiled to
 * compute operations on the propnet
 * 
 * @author flo
 * @deprecated This was an earlier implementation of the code generation. It updates the whole array of
 * propositions, thus requiring much more time than the optimized code generation. 
 */
public class PropnetCodeGeneration {

    int splitnumber = 100;
    /**
     * The number of all propositions in the propnet
     */
    private int propnumber;
    /**
     * The basepropositionnumbers in the array of all propositions
     */
    private List<Integer> baseprops = new ArrayList<Integer>();

    /**
     * the legalpropositionnumbers in the array of all propositions
     */
    private List<Integer> legalprops = new ArrayList<Integer>();

    /**
     * the goalpropositionnumbers in the array of all propositions
     */
    private List<Integer> goalprops = new ArrayList<Integer>();

    /**
     * the terminalpropositionnumber
     */
    private int terminalprop;

    /**
     * a map that maps each proposition to its position in the boolean array for
     * statecomputation
     */
    private HashMap<Proposition, Integer> propToInt;

    /**
     * a map that maps each position in the boolean array to a proposition
     */
    private HashMap<Integer, Proposition> intToProp;

    /**
     * the ordering in which order the view props have to be computed
     */
    private List<Proposition> ordering;

    public PropnetCodeGeneration(int propnumber, List<Integer> baseprops,
            List<Integer> legalprops, List<Integer> goalprops,
            int terminalprop, HashMap<Proposition, Integer> propToInt,
            HashMap<Integer, Proposition> intToProp, List<Proposition> ordering) {
        this.propnumber = propnumber;
        this.baseprops = baseprops;
        this.legalprops = legalprops;
        this.goalprops = goalprops;
        this.terminalprop = terminalprop;
        this.propToInt = propToInt;
        this.ordering = ordering;
        this.intToProp = intToProp;
    }

    /**
     * creates a .java file, with the given filename, which can be compiled to
     * compute states from a propnet
     * 
     * @param filename
     * @throws IOException
     * @throws AndGateException
     */
    public void createPropnetProgram(String filename)
            throws IOException, AndGateException {
        // create a new file with the propnetname and the .java tag
        File file = new File(filename + ".java");
        file.delete();
        file.createNewFile();
        PrintWriter writer = new PrintWriter(file);
        // handle the imports, needed by that file
        writer.println("import java.util.ArrayList;");
        writer.println("import java.util.List;");
        // start the class body
        writer.println("public class " + filename + " {");
        // a boolean array which represents the proposition values
        writer.println("static boolean[] propositions = new boolean["
                + propnumber + "];");
        // an int array, which says which proposition number is a base prop
        String baseproplist = "{";
        for (int i = 0; i < baseprops.size() - 1; i++) {
            baseproplist += baseprops.get(i) + ",";
        }
        baseproplist += baseprops.get(baseprops.size() - 1) + "}";
        writer.println("static int[] baseprops = new int[]" + baseproplist
                + ";");
        // an int array, which says which proposition number is a legal prop
        String legalproplist = "{";
        for (int i = 0; i < legalprops.size() - 1; i++) {
            legalproplist += legalprops.get(i) + ",";
        }
        legalproplist += legalprops.get(legalprops.size() - 1) + "}";
        writer.println("static int[] legalprops = new int[]" + legalproplist
                + ";");
        // an int array, which says which proposition number is a goal prop
        String goalproplist = "{";
        for (int i = 0; i < goalprops.size() - 1; i++) {
            goalproplist += goalprops.get(i) + ",";
        }
        goalproplist += goalprops.get(goalprops.size() - 1) + "}";
        writer.println("static int[] goalprops = new int[]" + goalproplist
                + ";");
        // the terminalpropositionnumber
        writer.println("static int terminalprop = " + terminalprop + ";");

        // write the code for the update method
        writer.println("public static void update(int[] args) {");
        // set the state propositions to true
        writer.println("for (int i = 0; i < args.length; i++) {propositions[args[i]] = true;}");
        // set the view propositions to true
        writer.println("}");
        for (int i = 0; i <= ordering.size() / splitnumber; i++) {
            writer.println("public static void update"+ i + "() {");
            for (int j = i*splitnumber; (j < (i+1) *splitnumber) && (j < ordering.size()); j++) {
                Proposition viewprop = ordering.get(j);
                String viewpropline = writeViewProp(viewprop.getSingleInput());
                String wholeline = "propositions[" + propToInt.get(viewprop) + "] = " + viewpropline +";";
                writer.println(wholeline);
            }
            writer.println("}");
            
        }
        
        //set the transitionvalue
        
        writer.println("public static void update"+((ordering.size() / splitnumber) + 1)+"() {");
        // set the base propositions to their transitioninput value
        for (int i = 0; i < baseprops.size(); i++) {
            // we need the number of the input of the transition of the
            // baseproposition,
            // so we have to get the baseproposition via the map and then get
            // its transitioninput
            writer.println("");
            Proposition baseprop = intToProp.get(baseprops.get(i));
            writer.println("propositions[" + baseprops.get(i)
                    + "] = " + writeViewProp(baseprop.getSingleInput()) +";");
        }
        writer.println("}");
        //now we have the update method, so we need some helper methods
        //to get the goals/terminal/baseprops etc
        writer.println("public static List<Integer> getTrueBaseProps(int[] state) {");
        writer.println("propositions = new boolean["+propnumber+"];");
        writer.println("update(state);");
        for (int i = 0; i <= (ordering.size() / splitnumber) + 1; i++) {
            writer.println("update"+i+"();");
        }
        writer.println(" List<Integer> result = new ArrayList<Integer>();");
        writer.println("for (int i = 0; i < baseprops.length; i++) {");
        writer.println("if (propositions[baseprops[i]])");
        writer.println(" result.add(baseprops[i]);");
        writer.println("}");
        writer.println(" return result;");
        writer.println("}");
        writer.println("public static List<Integer> getTrueLegalProps(int[] state) {");
        writer.println("propositions = new boolean["+propnumber+"];");
        writer.println("update(state);");
        for (int i = 0; i <= (ordering.size() / splitnumber) + 1; i++) {
            writer.println("update"+i+"();");
        }
        writer.println("List<Integer> result = new ArrayList<Integer>();");
        writer.println("for (int i = 0; i < legalprops.length; i++) {");
        writer.println("if (propositions[legalprops[i]])");
        writer.println("result.add(legalprops[i]);");
        writer.println("}");
        writer.println("return result;");
        writer.println("}");
        writer.println("public static List<Integer> getTrueGoalProps(int[] state) {");
        writer.println("propositions = new boolean["+propnumber+"];");
        writer.println("update(state);");
        for (int i = 0; i <= (ordering.size() / splitnumber) + 1; i++) {
            writer.println("update"+i+"();");
        }
        writer.println("List<Integer> result = new ArrayList<Integer>();");
        writer.println("for (int i = 0; i < goalprops.length; i++) {");
        writer.println("if (propositions[goalprops[i]])");
        writer.println("result.add(goalprops[i]);");
        writer.println("}");
        writer.println("return result;");
        writer.println("}");
        writer.println("public static int isTerminal(int[] state){");
        writer.println("propositions = new boolean["+propnumber+"];");
        writer.println("update(state);");
        for (int i = 0; i <= (ordering.size() / splitnumber) + 1; i++) {
            writer.println("update"+i+"();");
        }
        writer.println("if(propositions[terminalprop])");
        writer.println("return 1;");
        writer.println(" return 0;");
        writer.println("}");
        writer.println(" public static void main(String[] args) {");
        writer.println(" if (args[0].equals(\"b\")) {");
        writer.println("int[] input = new int[args.length -1];");
        writer.println("int j = 0;");
        writer.println("for (int i = 1; i < args.length; i++) {");
        writer.println(" input[j] = Integer.parseInt(args[i]);");
        writer.println("j++;");
        writer.println("}");            
        //TODO remove time computation
        writer.println("long start = System.currentTimeMillis();");
        writer.println("List<Integer> result =  getTrueBaseProps(input);");
        writer.println("long stop = System.currentTimeMillis();");
        writer.println("System.out.println(\"Time for computation: \" + (stop-start));");
        writer.println(" for(int i=0; i < result.size(); i++)");
        writer.println("System.out.println(result.get(i));");
        writer.println("}");
        writer.println(" if (args[0].equals(\"l\")) {");
        writer.println("int[] input = new int[args.length -1];");
        writer.println("int j = 0;");
        writer.println("for (int i = 1; i < args.length; i++) {");
        writer.println(" input[j] = Integer.parseInt(args[i]);");
        writer.println("j++;");
        writer.println("}");            
        writer.println("List<Integer> result =  getTrueLegalProps(input);");    
        writer.println(" for(int i=0; i < result.size(); i++)");
        writer.println("System.out.println(result.get(i));");
        writer.println("}");
        writer.println(" if (args[0].equals(\"g\")) {");
        writer.println("int[] input = new int[args.length -1];");
        writer.println("int j = 0;");
        writer.println("for (int i = 1; i < args.length; i++) {");
        writer.println(" input[j] = Integer.parseInt(args[i]);");
        writer.println("j++;");
        writer.println("}");            
        writer.println("List<Integer> result =  getTrueGoalProps(input);");    
        writer.println(" for(int i=0; i < result.size(); i++)");
        writer.println("System.out.println(result.get(i));");
        writer.println("}");
        writer.println(" if (args[0].equals(\"t\")) {");
        writer.println("int[] input = new int[args.length -1];");
        writer.println("int j = 0;");
        writer.println("for (int i = 1; i < args.length; i++) {");
        writer.println(" input[j] = Integer.parseInt(args[i]);");
        writer.println("j++;");
        writer.println("}");            
        writer.println("int result =  isTerminal(input);");    
        writer.println("System.out.println(result);");
        writer.println("}");
        writer.println("}");
        writer.println("}");
        writer.close();

    }

    /**
     * Writes the code line for a given proposition / how it has to be
     * computed. I.E. if its parent is an or, it should be or_parent1 |
     * or_parent2 Note that the inputs of view propositions can only be Not, Or,
     * And and other propositions, only for base propositions exist transitions
     * 
     * @param vp
     *            the viewproposition
     * @return a codelinestring
     * @throws AndGateException
     */
    private String writeViewProp(Component vpparent)
            throws AndGateException {
        //Component vpparent = vp.getSingleInput();
        // if the input is only a proposition, we have only this line
        if (vpparent instanceof Proposition)
            return "propositions[" + propToInt.get(vpparent) + "]";
        // if its input is Not, it just negates the input
        if (vpparent instanceof Not)
            return "(!" + writeViewProp(vpparent.getSingleInput()) + ")";
        // if its input is an Or, we have to write each or input, divided by
        // the | operator, i.e. (propositions[1] | propositions[2])
        // we need parantheses, because we can have multiple or layers, like
        // ((propositions[1] | propositions[2]) | (propositions[3]
        // |propositions[4]))
        if (vpparent instanceof Or) {
            String result = "(";
            for (Component orInput : vpparent.getInputs()) {
                result += writeViewProp(orInput) + "|";
            }
            // we delete the last | and replace it by )
            result = result.substring(0, (result.length() - 1));
            result += ")";
            return result;
        }
        if (vpparent instanceof And) {
            String result = "(";
            for (Component orInput : vpparent.getInputs()) {
                result += writeViewProp(orInput) + "&";
            }
            // we delete the last & and replace it by )
            result = result.substring(0, (result.length() - 1));
            result += ")";
            return result;
            
        } 
        if (vpparent instanceof Transition)
        {
            return writeViewProp(vpparent.getSingleInput());
        }
        else {
            return "true;";
            //throw new ViewUpdateException();
        }
    }


}
