package propnet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import propnet.architecture.And;
import propnet.architecture.Component;
import propnet.architecture.Constant;
import propnet.architecture.Not;
import propnet.architecture.Or;
import propnet.architecture.Proposition;
import propnet.architecture.Transition;
import core.exceptions.AndGateException;
import core.logging.TheLogger;

/**
 * this class creates multiple .java files for a given propnet, which can be
 * compiled to compute operations (i.e. getNextState, getLegalMoves, etc) on the
 * propnet.
 * 
 * Instead of updating every proposition, we check which propositions have to be
 * updated, thus just updating a small part of the whole graph.
 * 
 * Depending on the classsplitnumber, we create a new .java file for every n
 * view-, input-, and basepropositions.
 * 
 * @author flo
 * 
 */
public class OptimizedPropNetCodeGeneration {

	/**
	 * The objectname of this class, important when handling multiple code files
	 */
	private String oname;

	/**
	 * for every n view-,base-,input-propositions we create a new class, n =
	 * classsplitnumber. Important, because Java restricts the number of methods
	 * and codesize of a class
	 */
	int classsplitnumber = 300;
	/**
	 * determines the number of inputupdate methods we create, i.e. packs 100
	 * updates in one method.
	 */
	int splitnumber = 1000;
	/**
	 * The number of all propositions in the propnet
	 */
	private int propnumber;
	/**
	 * The basepropositionindices in the array of all propositions
	 */
	private List<Integer> baseprops = new ArrayList<Integer>();

	/**
	 * the legalpropositionindices in the array of all propositions
	 */
	private List<Integer> legalprops = new ArrayList<Integer>();

	/** the viewpropositionindices in the array of all prpopositions */
	private List<Integer> viewprops = new ArrayList<Integer>();

	/**
	 * the goalpropositionindices in the array of all propositions
	 */
	private List<Integer> goalprops = new ArrayList<Integer>();

	/**
	 * the inputpropositions
	 */
	private ArrayList<Integer> inputprops = new ArrayList<Integer>();

	/**
	 * the terminalpropositionnumber
	 */
	private int terminalprop;

	/**
	 * a map that maps each proposition to its position in the boolean array for
	 * statecomputation
	 */
	private HashMap<Proposition, Integer> propToInt;

	/**
	 * a map that maps each position in the boolean array to a proposition
	 */
	private HashMap<Integer, Proposition> intToProp;

	/**
	 * a map that maps each position in the gate array to an and gate
	 */
	private HashMap<Component, Integer> andGateToInt;

	/**
	 * a map that maps each position in the gate array to an or gate
	 */
	private HashMap<Component, Integer> orGateToInt;

	/**
	 * a map that maps each position in the gate array to an not gate
	 */
	private HashMap<Component, Integer> notGateToInt;

	/**
	 * the ordering in which order the Not Gates have to be computed
	 */
	private List<Component> ordering;

	/** constants are components that always have a fixed value */
	private List<Constant> constants;

	/** the number of the init proposition */
	private int initprop;

	/** a map that maps each viewproposition to its classnumber */
	private HashMap<Integer, Integer> viewclassnumbers = new HashMap<Integer, Integer>();

	/** a map that maps each inputproposition to its classnumber */
	private HashMap<Integer, Integer> inputclassnumbers = new HashMap<Integer, Integer>();

	/** a map that maps each baseproposition to its classnumber */
	private HashMap<Integer, Integer> baseclassnumbers = new HashMap<Integer, Integer>();

	/** the computation class for this code generation */
	private PropNetComputations pncomputations;

	/**
	 * Initializes everything we need to write code for a propnet. Also logs the
	 * number of base/input/goal update methods.
	 * 
	 * @param pnc
	 *            pnc contains information about the foldername where the java
	 *            files are created
	 * @param pn
	 *            the propnet, for which we create our methods
	 * @param baseprops
	 *            HashSet that stores the index of each base proposition
	 * @param legalprops
	 *            List that stores the index of each legal proposition
	 * @param goalprops
	 *            List that stores the index of each goal proposition
	 * @param viewprops
	 *            List that stores the index of each view proposition
	 * @param notOrdering
	 *            evaluationorder of the notgates
	 * @param inputprops
	 *            List that stores the index of each input proposition
	 */
	public OptimizedPropNetCodeGeneration(PropNetComputations pnc, PropNet pn,
			List<Integer> baseprops, List<Integer> legalprops,
			List<Integer> goalprops, List<Integer> viewprops,
			List<Component> notOrdering, ArrayList<Integer> inputprops) {

		pncomputations = pnc;
		this.propnumber = pn.getPropositions().size();
		this.baseprops = baseprops;
		this.legalprops = legalprops;
		this.viewprops = viewprops;
		this.goalprops = goalprops;
		this.terminalprop = pn.getPropToInt().get(pn.getTerminalProposition());
		this.propToInt = pn.getPropToInt();
		this.ordering = notOrdering;
		this.intToProp = pn.getIntToProp();
		this.andGateToInt = pn.getAndGateToInt();
		this.orGateToInt = pn.getOrGateToInt();
		this.notGateToInt = pn.getNotGateToInt();
		this.inputprops = inputprops;
		this.initprop = pn.getPropToInt().get(pn.getInitProposition());
		this.constants = pn.getConstants();
		TheLogger.LOGGER
				.fine(String
						.format("number of update methods: [ base=%d, view=%d, input=%d, NOT=%d ], CONSTANTS: %d",
								baseprops.size(), viewprops.size(),
								inputprops.size(), notGateToInt.size(),
								constants.size()));

	}

	/**
	 * Creates multiple java files, with the given nameprefix + I/IU/V/P.
	 * 
	 * @param filename
	 *            the nameprefix for the javafiles
	 * @throws IOException
	 * @throws AndGateException
	 *             byte was not enough for the andgate array
	 */
	public void createPropnetProgram(String filename) throws IOException,
			AndGateException {

		// sort the view propositions, so that we have the first n
		// (classsplitnumber) methods in the first
		// class, etc
		Collections.sort(viewprops);
		// store for each view proposition its class number
		for (int i = 0; i < viewprops.size(); i++) {
			viewclassnumbers.put(viewprops.get(i), i / classsplitnumber);
		}
		// same procedure for inputprops
		Collections.sort(inputprops);
		// store for each input proposition its class number
		for (int i = 0; i < inputprops.size(); i++) {
			inputclassnumbers.put(inputprops.get(i), i / classsplitnumber);
		}
		// same procedure for baseprops
		Collections.sort(baseprops);
		// store for each base proposition its class number
		for (int i = 0; i < baseprops.size(); i++) {
			baseclassnumbers.put(baseprops.get(i), i / classsplitnumber);
		}
		// create a new folder
		File file = new File(pncomputations.getFoldername());
		file.mkdir();
		oname = filename;

		try {
			createMainCode("P");
			createPropCode("I");
			createPropCode("V");
			createPropCode("B");
		} catch (Exception e) {
			TheLogger.LOGGER
					.fine("unknown Proposition type input while creating code");
			e.printStackTrace();
		}
	}

	/**
	 * Creates the java code for proposition updates, as well the code to update
	 * these propositions. Creates several classes, in case of many propositions
	 * (specified by classsplitnumber).
	 * 
	 * @param filename
	 *            name of the file
	 * @param proptype
	 *            Propositiontype. Possible values: V, P, I TODO: implement this
	 *            for Not gates.
	 * @throws Exception
	 */
	public void createPropCode(String proptype) throws Exception {
		String filename = proptype;
		String updatemethod = "";
		int propsize = 0;
		if (proptype == "B") {
			propsize = baseprops.size();
			updatemethod = "updatebaseprops";
		} else if (proptype == "V") {
			propsize = viewprops.size();
			updatemethod = "updateviewprops";

		} else if (proptype == "I") {
			propsize = inputprops.size();
			updatemethod = "updateinputprops";

		} else
			throw new Exception();

		for (int i = 0; i <= propsize / classsplitnumber; i++) {
			File file = new File(pncomputations.getClasspath() + oname
					+ filename + i + ".java");
			pncomputations.getClassnames().add(oname + filename + i);
			file.delete();
			file.createNewFile();
			PrintWriter writer = new PrintWriter(file);

			writer.println("public class " + oname + filename + i + " {");

			for (int j = i * classsplitnumber; j < (i + 1) * classsplitnumber
					&& j < propsize; j++) {

				if (proptype == "V")
					createViewPropMethod(viewprops.get(j), writer, i);
				if (proptype == "I")
					createInputPropMethod(inputprops.get(j), writer, i);
				if (proptype == "B")
					createBasePropMethod(baseprops.get(j), writer, i);
			}
			writer.println("}");
			writer.close();
		}
		// enumerator for propositions, because we can't start from 0
		// after we create another class
		int propEnum = 0;
		// a class that handles the proposition update
		for (int i = 0; i <= propsize / classsplitnumber; i++) {
			File file = new File(pncomputations.getClasspath() + oname
					+ proptype + "U" + i + ".java");
			pncomputations.getClassnames().add(oname + proptype + "U" + i);
			file.delete();
			file.createNewFile();
			PrintWriter writer = new PrintWriter(file);

			writer.println("public class " + oname + proptype + "U" + i + " {");

			writer.println("public static synchronized void " + updatemethod
					+ "() {");
			for (int j = i * classsplitnumber; (j < (i + 1) * classsplitnumber)
					&& (j < propsize); j++) {
				if (proptype == "I") {
					writer.println("if(" + oname + "P.p["
							+ inputprops.get(propEnum) + "])");
					writer.println(oname + "I"
							+ inputclassnumbers.get(inputprops.get(propEnum))
							+ ".ui" + inputprops.get(propEnum) + "();");
					propEnum++;
				}
				if (proptype == "V") {
					writer.println("if(" + oname + "P.p["
							+ viewprops.get(propEnum) + "])");
					writer.println(oname + "V"
							+ viewclassnumbers.get(viewprops.get(propEnum))
							+ ".uv" + viewprops.get(propEnum) + "();");
					propEnum++;
				}
				if (proptype == "B") {
					writer.println("if(" + oname + "P.p["
							+ baseprops.get(propEnum) + "])");
					writer.println(oname + "B"
							+ baseclassnumbers.get(baseprops.get(propEnum))
							+ ".ub" + baseprops.get(propEnum) + "();");
					propEnum++;
				}
			}
			writer.println("}");
			writer.println("}");
			writer.close();
		}
	}

	/**
	 * This file includes all the data structures we need for representing the
	 * propnet structure, like boolean arrays for propositions, byte arrays for
	 * the andgates, etc. Here are also the call-up methods for updating the
	 * whole propnet, given an input/state
	 * 
	 * @param filename
	 * @throws IOException
	 * @throws AndGateException
	 */
	public void createMainCode(String filename) throws IOException,
			AndGateException {
		// create a new file
		File file = new File(pncomputations.getClasspath() + oname + filename
				+ ".java");
		pncomputations.getClassnames().add(oname + filename);
		file.delete();
		file.createNewFile();
		PrintWriter writer = new PrintWriter(file);
		// handle the imports, needed by that file
		writer.println("import java.util.ArrayList;");
		writer.println("import java.util.List;");
		writer.println("import java.util.Arrays;");
		// start the class body
		writer.println("public class " + oname + filename + " {");
		// a boolean array which represents the proposition values
		writer.println("static boolean[] p = new boolean[" + propnumber + "];");
		// a boolean array to store the base proposition after an update
		writer.println("static boolean[] bp = new boolean[" + propnumber + "];");
		// an int array, which says which proposition number is a base prop
		String baseproplist = "{";
		for (int baseprop : baseprops) {
			baseproplist += baseprop + ",";
		}
		baseproplist = baseproplist.substring(0, baseproplist.length() - 1)
				+ "}";
		writer.println("static int[] baseprops = new int[]" + baseproplist
				+ ";");
		// an int array, which says which proposition number is a legal prop
		String legalproplist = "{";
		for (int i = 0; i < legalprops.size() - 1; i++) {
			legalproplist += legalprops.get(i) + ",";
		}
		legalproplist += legalprops.get(legalprops.size() - 1) + "}";
		writer.println("static int[] legalprops = new int[]" + legalproplist
				+ ";");
		// an int array, which says which proposition number is a goal prop
		String goalproplist = "{";
		for (int i = 0; i < goalprops.size() - 1; i++) {
			goalproplist += goalprops.get(i) + ",";
		}
		goalproplist += goalprops.get(goalprops.size() - 1) + "}";
		writer.println("static int[] goalprops = new int[]" + goalproplist
				+ ";");
		// the terminalpropositionnumber
		writer.println("static int terminalprop = " + terminalprop + ";");
		// the initpropnumber
		writer.println("static int initprop = " + initprop + ";");

		// the and-counter values, implemented as an byte array
		writer.println("public static byte[] agc = new byte["
				+ andGateToInt.keySet().size() + "];");

		// the or-gate values, true if an or has been visited
		writer.println("public static boolean[] orf = new boolean["
				+ orGateToInt.keySet().size() + "];");
		// flags for not gates
		writer.println("public static boolean[] notf = new boolean["
				+ notGateToInt.keySet().size() + "];");

		// /////////////////////////////////methods////////////////////////////////////////

		// a method to fill the and counter with its number of inputs - 1
		writer.println("public static void resetAgc() {");
		writer.println("Arrays.fill(agc, (byte) 1);");
		for (Component andgate : andGateToInt.keySet()) {
			if (andgate.getInputs().size() != 2) {
				if (andgate.getInputs().size() > Byte.MAX_VALUE)
					throw new AndGateException();
				writer.println("agc[" + andGateToInt.get(andgate) + "] = "
						+ (andgate.getInputs().size() - 1) + ";");
			}
		}
		writer.println("}");
		// every base/input proposition and every not gate has its own update
		// method
		// thus we need some form of name convention for these methods.
		// we use method+b|i|N+arraypositionnumber for the methodnames

		for (Component not : ordering) {
			createNotPropMethod(not, writer);
		}

		createInitPropMethod(writer);
		createConstantMethod(writer);

		// the update method for updating a propnet, given the state/input
		// (represented as an
		// int array
		writer.println("public static synchronized void u(int[] args) {");
		writer.println("for (int i=0;i < args.length; i++)");
		writer.println("p[args[i]] = true;");
		writer.println("uC();");
		writer.println("if(p[" + initprop + "])");
		writer.println("updateInitprop();");
		for (int i = 0; i <= baseprops.size() / classsplitnumber; i++) {
			writer.println(oname + "BU" + i + ".updatebaseprops();");
		}
		for (int i = 0; i <= inputprops.size() / classsplitnumber; i++) {
			writer.println(oname + "IU" + i + ".updateinputprops();");
		}
		writer.println("updatenots();");
		writer.println("}");

		writer.println("public static synchronized void updatenots() {");
		for (Component not : ordering) {
			writer.println("if(!notf[" + notGateToInt.get(not) + "])");
			writer.println("uN" + notGateToInt.get(not) + "();");
		}
		writer.println("}");

		// now we have the update methods, so we need some helper methods
		// to get the goals/terminal/baseprops etc
		writer.println("public static synchronized List<Integer> getTrueBaseProps(int[] state) {");
		resetArrays(writer);
		writer.println("u(state);");
		writer.println(" List<Integer> result = new ArrayList<Integer>();");
		writer.println("for (int i = 0; i < baseprops.length; i++) {");
		writer.println("if (bp[baseprops[i]])");
		writer.println(" result.add(baseprops[i]);");
		writer.println("}");
		writer.println(" return result;");
		writer.println("}");
		writer.println("public static synchronized List<Integer> getTrueLegalProps(int[] state) {");
		resetArrays(writer);
		writer.println("u(state);");
		writer.println("List<Integer> result = new ArrayList<Integer>();");
		writer.println("for (int i = 0; i < legalprops.length; i++) {");
		writer.println("if (p[legalprops[i]])");
		writer.println("result.add(legalprops[i]);");
		writer.println("}");
		writer.println("return result;");
		writer.println("}");
		writer.println("public static synchronized List<Integer> getTrueGoalProps(int[] state) {");
		resetArrays(writer);
		writer.println("u(state);");
		writer.println("List<Integer> result = new ArrayList<Integer>();");
		writer.println("for (int i = 0; i < goalprops.length; i++) {");
		writer.println("if (p[goalprops[i]])");
		writer.println("result.add(goalprops[i]);");
		writer.println("}");
		writer.println("return result;");
		writer.println("}");
		writer.println("public static synchronized int isTerminal(int[] state){");
		resetArrays(writer);
		writer.println("u(state);");
		writer.println("if(p[terminalprop])");
		writer.println("return 1;");
		writer.println(" return 0;");
		writer.println("}");

		// the main method, for test purposes
		writer.println(" public static void main(String[] args) {");
		writer.println(" if (args[0].equals(\"b\")) {");
		writer.println("int[] input = new int[args.length -1];");
		writer.println("int j = 0;");
		writer.println("for (int i = 1; i < args.length; i++) {");
		writer.println(" input[j] = Integer.parseInt(args[i]);");
		writer.println("j++;");
		writer.println("}");
		writer.println("long start = System.currentTimeMillis();");
		writer.println("List<Integer> result =  getTrueBaseProps(input);");
		writer.println("long stop = System.currentTimeMillis();");
		writer.println("System.out.println(\"Time for computation: \" + (stop-start));");
		writer.println("for(int i=0; i < result.size(); i++)");
		writer.println("System.out.println(result.get(i));");
		writer.println("}");
		writer.println(" if (args[0].equals(\"l\")) {");
		writer.println("int[] input = new int[args.length -1];");
		writer.println("int j = 0;");
		writer.println("for (int i = 1; i < args.length; i++) {");
		writer.println(" input[j] = Integer.parseInt(args[i]);");
		writer.println("j++;");
		writer.println("}");
		writer.println("List<Integer> result =  getTrueLegalProps(input);");
		writer.println(" for(int i=0; i < result.size(); i++)");
		writer.println("System.out.println(result.get(i));");
		writer.println("}");
		writer.println(" if (args[0].equals(\"g\")) {");
		writer.println("int[] input = new int[args.length -1];");
		writer.println("int j = 0;");
		writer.println("for (int i = 1; i < args.length; i++) {");
		writer.println(" input[j] = Integer.parseInt(args[i]);");
		writer.println("j++;");
		writer.println("}");
		writer.println("List<Integer> result =  getTrueGoalProps(input);");
		writer.println(" for(int i=0; i < result.size(); i++)");
		writer.println("System.out.println(result.get(i));");
		writer.println("}");
		writer.println(" if (args[0].equals(\"t\")) {");
		writer.println("int[] input = new int[args.length -1];");
		writer.println("int j = 0;");
		writer.println("for (int i = 1; i < args.length; i++) {");
		writer.println(" input[j] = Integer.parseInt(args[i]);");
		writer.println("j++;");
		writer.println("}");
		writer.println("int result =  isTerminal(input);");
		writer.println("System.out.println(result);");
		writer.println("}");
		writer.println("}");
		writer.println("}");
		writer.close();

	}

	/**
	 * a method to write the code for resetting the data structures
	 * 
	 * @param writer
	 */
	private void resetArrays(PrintWriter writer) {
		writer.println("p = new boolean[" + propnumber + "];");
		writer.println("bp = new boolean[" + propnumber + "];");
		writer.println("resetAgc();");
		writer.println("orf = new boolean[" + orGateToInt.keySet().size()
				+ "];");
		writer.println("notf = new boolean[" + notGateToInt.keySet().size()
				+ "];");

	}

	/**
	 * writes the code for a basepropupdate method, given its number in the
	 * boolean array
	 * 
	 * @param basepropnumber
	 */
	private void createBasePropMethod(int basepropnumber, PrintWriter writer,
			int classnumber) {
		String methodname = "ub" + basepropnumber;
		writer.println("public static void " + methodname + "() {");
		writeGateCode(intToProp.get(basepropnumber), writer, "base",
				classnumber, 0, methodname, true, 0);
		writer.println("}");
	}

	/**
	 * writes the code for a viewproposition, given its number in the boolean
	 * array
	 * 
	 * @param viewpropnumber
	 * @param writer
	 * @param classnumber
	 *            the number of the class owning this view proposition
	 */
	private void createViewPropMethod(int viewpropnumber, PrintWriter writer,
			int classnumber) {
		String methodname = "uv" + viewpropnumber;
		writer.println("public static void " + methodname + "() {");
		// TODO update only legal/goal prop values, not all view props
		writer.println(oname + "P.p[" + viewpropnumber + "] = true;");
		writeGateCode(intToProp.get(viewpropnumber), writer, "view",
				classnumber, 0, methodname, true, 0);
		writer.println("}");
	}

	/**
	 * writes the code for an inputproposition, given its number in the boolean
	 * array
	 * 
	 * @param inputpropumber
	 * @param writer
	 */
	private void createInputPropMethod(int inputpropumber, PrintWriter writer,
			int classnumber) {
		String methodname = "ui" + inputpropumber;
		writer.println("public static void " + methodname + "() {");
		writeGateCode(intToProp.get(inputpropumber), writer, "input",
				classnumber, 0, methodname, true, 0);
		writer.println("}");
	}

	/**
	 * writes the code for updating a not gate
	 * 
	 * @param not
	 * @param writer
	 */
	private void createNotPropMethod(Component not, PrintWriter writer) {
		int notnumber = notGateToInt.get(not);
		String methodname = "uN" + notnumber;
		writer.println("public static void " + methodname + "() {");
		writeGateCode(not, writer, "not", 0, 0, methodname, true, 0);
		writer.println("}");
	}

	/**
	 * writes the code for updating the initprop method
	 * 
	 * @param writer
	 */
	private void createInitPropMethod(PrintWriter writer) {
		String methodname = "updateInitprop";
		writer.println("public static void updateInitprop() {");
		writeGateCode(intToProp.get(initprop), writer, "init", 0, 0,
				methodname, true, 0);
		writer.println("}");
	}

	/**
	 * writes the code for updating constants and their children
	 * 
	 * @param writer
	 */
	private void createConstantMethod(PrintWriter writer) {
		String methodname = "uC()";
		writer.println("public static void uC() {");
		for (Constant c : constants) {
			if (c.getValue())
				writeGateCode(c, writer, "constant", 0, 0, methodname, true, 0);
		}

		writer.println("}");
	}

	/**
	 * writes the code for the outputs of a given component, like counting down
	 * the andgate number, switching the or/not flags, assigning values to the
	 * propositions, etc
	 */
	private void writeGateCode(Component c, PrintWriter writer, String s,
			int classnumber, Integer linesWritten, String methodname,
			boolean methodClosable, int methodnumber) {
		/**
		 * if we have written more than 500 lines of code we want to split up
		 * the method, to keep its size balanced. Keep in mind that we can only
		 * split if we are not inside of an if-block
		 */
		for (Component output : c.getOutputs()) {
			if (linesWritten > 500 && methodClosable) {
				writer.println(methodname + "part" + methodnumber + "();");
				writer.println("}");
				writer.println("public static void " + methodname + "part"
						+ methodnumber + "() {");
				methodnumber++;
				linesWritten = 0;
			}
			if (output instanceof And) {

				writer.println("if (" + oname + "P.agc["
						+ andGateToInt.get(output) + "] == 0) {");
				linesWritten++;
				writeGateCode(output, writer, s, classnumber, linesWritten,
						methodname, false, methodnumber);
				writer.println("} else { " + oname + "P.agc["
						+ andGateToInt.get(output) + "] -=1;}");
				linesWritten++;

			} else if (output instanceof Or) {
				writer.println("if (!" + oname + "P.orf["
						+ orGateToInt.get(output) + "]){");
				linesWritten++;
				writeGateCode(output, writer, s, classnumber, linesWritten,
						methodname, false, methodnumber);
				writer.println("" + oname + "P.orf[" + orGateToInt.get(output)
						+ "] = true;}");
				linesWritten++;

			} else if (output instanceof Proposition) {
				if (s == "view"
						&& viewclassnumbers.get(propToInt.get(output)) == classnumber) {

					writer.println("uv" + propToInt.get(output) + "();");
					linesWritten++;
				} else {
					writer.println(oname + "V"
							+ viewclassnumbers.get(propToInt.get(output))
							+ ".uv" + propToInt.get(output) + "();");
					linesWritten++;
				}
			} else if (output instanceof Not) {
				writer.println(oname + "P.notf[" + notGateToInt.get(output)
						+ "] = true;");
				linesWritten++;

			} else if (output instanceof Transition) {
				writer.println(oname + "P.bp["
						+ propToInt.get(output.getSingleOutput()) + "] = true;");
				linesWritten++;

			}
		}

	}

}