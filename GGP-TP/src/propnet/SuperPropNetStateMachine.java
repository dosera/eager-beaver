package propnet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import propnet.factory.OptimizingPropNetFactory;

import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;
import core.gdl.Gdl;
import core.gdl.GdlSentence;
import core.logging.TheLogger;
import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;
import statemachine.StateMachine;

/**
 * This class should be a superclass of the PropnetStateMachine, i.e. it tries to factor the propnet,
 * and then uses multiple PropNetStateMachines for multiple PropNet Factors.
 * It is still under construction, but fulfills its actual purposes. 
 * 
 * It is not yet clear if the computation values are computed correctly (I think currently it 
 * overestimates the calls in the number of the subpropnets).
 * @author flo
 *
 */
public class SuperPropNetStateMachine extends StateMachine
{

	/**Is this propnet factorisable? */
    private boolean factorisable = false;
    private PropNet propNet = null;
    /**For each factor we create a subpropnet */
    private List<PropNet> subPropNets = new ArrayList<PropNet>();
    private List<PropNetStateMachine> statemachines = new ArrayList<PropNetStateMachine>();

    /** is this propnet factorisable? */
    public boolean isFactorisable() {
        return factorisable;
    }

    /** returns the propnet */
    public PropNet getPropNet() {
        return propNet;
    }

    /** the subpropnets of this statemachine */
    public List<PropNetStateMachine> getSubStateMachines() {
        return statemachines;
    }

    @Override
    public void initialize(List<Gdl> description) {
        try
        {
            long start = System.currentTimeMillis();
            propNet = OptimizingPropNetFactory.create(description);
            long stop = System.currentTimeMillis();
            long timetobuildPN = (stop - start) / 1000;
            TheLogger.LOGGER
                    .fine("PN created in " + timetobuildPN + " seconds");

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        Factoring f = new Factoring(propNet);
        // check if propnet is factorisable
        if (f.getIsets().size() > 1)
        {
            // create the propnets for each factor and assign one statemachine
            // to each propnet
            factorisable = true;
            f.getIsAndcreatePns();
            this.subPropNets = f.GetSubPropNets();
            for (PropNet pn : subPropNets)
            {
                PropNetStateMachine pns = new CachedPropNetStateMachine();
                pns.initialize(pn);
                statemachines.add(pns);
            }
            propNet = null;
        }
    }

    @Override
    public int getNextStateCalls() {
        int result = 0;
        for (PropNetStateMachine pnsm : statemachines)
            result += pnsm.getNextStateCalls();
        return result;
    }

    @Override
    public int getGoalCalls() {
        int result = 0;
        for (PropNetStateMachine pnsm : statemachines)
            result += pnsm.getGoalCalls();
        return result;
    }

    @Override
    public int getLegalCalls() {
        int result = 0;
        for (PropNetStateMachine pnsm : statemachines)
            result += pnsm.getLegalCalls();
        return result;
    }

    @Override
    public int getTerminalCalls() {
        int result = 0;
        for (PropNetStateMachine pnsm : statemachines)
            result += pnsm.getTerminalCalls();
        return result;
    }

    @Override
    public long getStateComputationTime() {
        int result = 0;
        for (PropNetStateMachine pnsm : statemachines)
            result += pnsm.getStateComputationTime();
        return result;
    }

    @Override
    public long getLegalComputationTime() {
        int result = 0;
        for (PropNetStateMachine pnsm : statemachines)
            result += pnsm.getLegalComputationTime();
        return result;
    }

    @Override
    public long getGoalComputationTime() {
        int result = 0;
        for (PropNetStateMachine pnsm : statemachines)
            result += pnsm.getGoalComputationTime();
        return result;
    }

    @Override
    public long getTerminalComputationTime() {
        int result = 0;
        for (PropNetStateMachine pnsm : statemachines)
            result += pnsm.getTerminalComputationTime();
        return result;
    }

    @Override
    public int getGoal(MachineState state, Role role)
            throws GoalDefinitionException
    {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isTerminal(MachineState state) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<Role> getRoles() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public MachineState getInitialState() {
        HashSet<GdlSentence> contents = new HashSet<GdlSentence>();
        for (PropNetStateMachine pnsm : statemachines)
        {
            MachineState subinitialstate = pnsm.getInitialState();
            contents.addAll(subinitialstate.contents);
        }
        return new MachineState(contents);
    }

    @Override
    public List<Move> getLegalMoves(MachineState state, Role role)
            throws MoveDefinitionException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public MachineState getNextState(MachineState state, List<Move> moves)
            throws TransitionDefinitionException
    {
        HashSet<GdlSentence> nextsubstates = new HashSet<GdlSentence>();
        for (PropNetStateMachine pnsm : statemachines)
        {
            HashSet<GdlSentence> contents = getSubState(state, pnsm);
            List<Move> submoves = new ArrayList<Move>();
            for (Move move : moves)
            {
                if (pnsm.getPropNet().getInputPropositions().containsKey(moves))
                    submoves.add(move);
            }
            MachineState substate = new MachineState(contents);
            nextsubstates.addAll(pnsm.getNextState(substate, submoves)
                    .getContents());
        }
        return new MachineState(nextsubstates);
    }

	/**
	 * extracts a substate from a state for a given statemachine
	 * @param state the state which we want to reduce
	 * @param pnsm the statemachine for which we want to get a smaller state 
	 */
	public HashSet<GdlSentence> getSubState(MachineState state, PropNetStateMachine pnsm) {
		HashSet<GdlSentence> contents = new HashSet<GdlSentence>();
		for (GdlSentence partOfState : state.contents)
		{
		    if (pnsm.getPropNet().getBasePropositions()
		            .containsKey(partOfState.toTerm()))
		        contents.add(partOfState);
		}
		return contents;
	}

    /**
     * Given a state and a statemachine (which should be one of the
     * substatemachines), we compute a partial state for this given
     * statemachine. e.g. if our state is ( cell 3 3 4 ) ( cell 1 1 0 ) and our
     * statemachine only handles the lights 33, 34, 43,44 this should return the
     * state ( cell 3 3 4 )
     * 
     * @param currentState
     *            the source state
     * @param pnsm
     *            the substatemachine
     * @return a partial state for this statemachine
     */
    public MachineState computeStateForMachine(MachineState currentState,
            PropNetStateMachine pnsm)
    {
        HashSet<GdlSentence> contents = getSubState(currentState, pnsm);
        return new MachineState(contents);
    }

	@Override
	public long getMaxlegalcomputation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getMaxgoalcomputation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getMaxnextstatecomputation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getMaxterminalcomputation() {
		// TODO Auto-generated method stub
		return 0;
	}

}
