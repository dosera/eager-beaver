package propnet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;

import core.logging.TheLogger;

/**
 * a classloader to reload the same class multiple times, 
 * see http://tutorials.jenkov.com/java-reflection/dynamic-class-loading-reloading.html
 * @author jenkov
 *
 */
public class MyClassLoader extends ClassLoader{

   PropNetComputations pnc;

    public MyClassLoader(ClassLoader parent, PropNetComputations pnc) {
        super(parent);
        this.pnc = pnc;
    }

    public Class<?> loadClass(String name) throws ClassNotFoundException {
        if(!pnc.getClassnames().contains(name))
                return super.loadClass(name);

        try {
            String classpath = "bin/" + name +".class";
            File f = new File(classpath);
            String url = "file:" + f.getAbsolutePath();
            URL myUrl = new URL(url);
            java.net.URLConnection connection = myUrl.openConnection();
            InputStream input = connection.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int data = input.read();

            while(data != -1){
                buffer.write(data);
                data = input.read();
            }

            input.close();

            byte[] classData = buffer.toByteArray();

            return defineClass(name,
                    classData, 0, classData.length);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            TheLogger.LOGGER.log(Level.SEVERE, "IOException while creating propnet", e);
        }

        return null;
    }

}