package propnet;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import core.gdl.GdlSentence;
import core.gdl.GdlTerm;
import propnet.architecture.*;

/**
 * A class to build out of a propnet an updatecode that uses Bits instead of
 * Components, for a faster updating method. We represent every component as one
 * Bit, saved in a BitSet. Additionally, we have an Inputmapping, which maps the
 * componentindex to its inputcomponents_indices, so that we can compute easily
 * which value the actual component gets. Additionally we need different a
 * Hashmap, that maps Integers to a value, which states if a component(index) is
 * of a certain type (AND, OR, ...).
 * 
 * First we save which component has which Index (-> Constructor), as well as
 * saving which component has which type. Then we save the inputs of every
 * componentindex.
 * 
 * 
 * 
 * @author flo _und_ tim
 * @deprecated This class is totally wrong and does not work. DONT USE THIS. It is only here
 *             for historical purposes.
 */
public class PropnetGeneration {

	private BitSet componentvalues;
	private Map<Component, Integer> componentmapping;
	private Map<Integer, Component> reversedcomponentmapping;
	private Map<Integer, List<Integer>> componentinputs = new HashMap<Integer, List<Integer>>();
	private List<Integer> viewordering = new ArrayList<Integer>();
	private List<Integer> baseprops = new ArrayList<Integer>();
	/**
	 * The componenttype of each component(via its index) we use: 1 - AND 2 - OR
	 * 3 - NOT 4 - baseprop, 5 - legalprop, 0 everything else
	 */
	private Map<Integer, Integer> componenttype;
	private List<Integer> truelegalpropositions;
	private List<Integer> truegoalpropositions;

	public PropnetGeneration(Set<Component> components,
			HashSet<Component> legalprops, Map<GdlTerm, Proposition> baseprops,
			HashSet<Component> goalprops, List<Proposition> ordering) {
		int i = 0;
		this.componentmapping = new HashMap<Component, Integer>();
		this.reversedcomponentmapping = new HashMap<Integer, Component>();
		this.componenttype = new HashMap<Integer, Integer>();
		// store the index for each component, as well as its type
		for (Component comp : components) {
			this.componentmapping.put(comp, i);
			this.reversedcomponentmapping.put(i, comp);
			if (comp instanceof And)
				this.componenttype.put(i, 1);
			else if (comp instanceof Or)
				this.componenttype.put(i, 2);
			else if (comp instanceof Not)
				this.componenttype.put(i, 3);
			else if (comp instanceof Proposition
					&& baseprops.containsKey(((Proposition) comp).getName())) {
				this.componenttype.put(i, 4);
				this.baseprops.add(i);
			} else if (legalprops.contains(comp))
				this.componenttype.put(i, 5);
			else if (goalprops.contains(comp))
				this.componenttype.put(i, 6);
			else
				this.componenttype.put(i, 0);
			i++;
		}
		// store the inputindices of each component
		for (Component comp : components) {
			List<Integer> inputs = new ArrayList<Integer>();
			for (Component input : comp.getInputs())
				inputs.add(componentmapping.get(input));

			this.componentinputs.put(componentmapping.get(comp), inputs);
		}
		// initialize the BitSet
		componentvalues = new BitSet(components.size());
		// transform the ordering
		viewordering = new ArrayList<Integer>();
		for (Proposition proposition : ordering) {
			viewordering.add(componentmapping.get(proposition));
		}
	}

	/**
	 * The method for updating all propositions. We get the input and base
	 * propositions, set their Bits on true and then update all other
	 * propositions, according to the ordering.
	 * 
	 * @param baseprops
	 *            the base propositions, which are set to true, given as
	 *            components
	 * @param inputprops
	 *            the input propositions, which are set to true, given as
	 *            components
	 */
	public void update(Set<Component> baseprops, Set<Component> inputprops) {
		// clear the truepropositions, as well as resetting the bitset
		truegoalpropositions = new ArrayList<Integer>();
		truelegalpropositions = new ArrayList<Integer>();
		componentvalues.clear();
		for (Component baseprop : baseprops) {
			componentvalues.set(componentmapping.get(baseprop), true);
		}
		for (Component inputprop : inputprops) {
			componentvalues.set(componentmapping.get(inputprop), true);
		}

		for (int viewprop : viewordering) {
			setValue(viewprop);
		}
	}

	/**
	 * updates the base propositions, to compute a new state, when the whole
	 * network was updated
	 * 
	 * @return returns the list of basepropositions, which are true in the next
	 *         state
	 */
	public List<Proposition> updateBaseProps() {
		List<Proposition> result = new ArrayList<Proposition>();
		for (int baseprop : baseprops) {
			{
				setValue(componentinputs.get(baseprop).get(0));
				setValue(baseprop);
			}
			if (componentvalues.get(baseprop))
				result.add(((Proposition) (reversedcomponentmapping
						.get(baseprop))));
		}
		return result;
	}

	/**
	 * returns the list of true legalprops
	 * 
	 * @return
	 */
	public HashSet<Component> getLegalProps() {
		HashSet<Component> result = new HashSet<Component>();
		for (int legalpropindex : truelegalpropositions) {
			result.add(reversedcomponentmapping.get(legalpropindex));
		}
		return result;
	}

	/**
	 * returns the list of true goalpropositions
	 * 
	 * @return
	 */
	public HashSet<Component> getGoalProps() {
		HashSet<Component> result = new HashSet<Component>();
		for (int goalpropindex : truegoalpropositions) {
			result.add(reversedcomponentmapping.get(goalpropindex));
		}
		return result;
	}

	/**
	 * returns the value of the terminal proposition
	 * 
	 * @param terminalprop
	 *            the terminalproposition
	 * @return value of the terminalproposition
	 */
	public boolean getTerminalValue(Proposition terminalprop) {
		return componentvalues.get(componentmapping.get(terminalprop));
	}

	/**
	 * sets the bit of the component on its value, according to its inputs. If
	 * one of the input is and/not/or first compute that value
	 * 
	 * @param index
	 *            the index of the component
	 */
	public void setValue(int index) {

		for (int input : componentinputs.get(index)) {
			int inputtype = componenttype.get(input);
			if (inputtype == 1 || inputtype == 2 || inputtype == 3)
				setValue(input);
		}
		boolean value;
		// component is AND
		if (componenttype.get(index) == 1) {
			value = true;
			for (int input : componentinputs.get(index)) {
				value = value & componentvalues.get(input);
			}
			componentvalues.set(index, value);
		}
		// component is OR
		if (componenttype.get(index) == 2) {
			value = false;
			for (int input : componentinputs.get(index)) {
				value = value | componentvalues.get(input);
			}
			componentvalues.set(index, value);
		} else if (componenttype.get(index) == 1) {
			value = true;
			for (int input : componentinputs.get(index)) {
				value = value & componentvalues.get(input);
			}
			componentvalues.set(index, value);
		}
		// component is NOT
		else if (componenttype.get(index) == 3) {
			value = !componentvalues.get(componentinputs.get(index).get(0));
			componentvalues.set(index, value);
		}
		// component is legalproposition
		else if (componenttype.get(index) == 5) {
			value = componentvalues.get(componentinputs.get(index).get(0));
			componentvalues.set(index, value);
			if (value)
				truelegalpropositions.add(index);
		}
		// component is goalproposition
		else if (componenttype.get(index) == 6) {
			value = componentvalues.get(componentinputs.get(index).get(0));
			componentvalues.set(index, value);
			if (value)
				truegoalpropositions.add(index);
		}
		// component is Proposition or Transition
		else {
			value = componentvalues.get(componentinputs.get(index).get(0));
			componentvalues.set(index, value);
		}
	}
}
