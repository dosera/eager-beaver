package propnet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import core.gdl.GdlConstant;
import core.gdl.GdlFunction;
import core.gdl.GdlPool;
import core.gdl.GdlSentence;
import core.gdl.GdlTerm;

import propnet.architecture.Component;
import propnet.architecture.Constant;
import propnet.architecture.Proposition;
import statemachine.Role;

/**
 * This class tries to split up a PropNet in multiple Factors. Factoring is a
 * method to create smaller subgames, thus trying to reduce the size of the
 * search tree.
 * 
 * Currently this Factoring class should only work for games with conjunctive
 * factoring, i.e. you can decide which game you want to play and in the other
 * game you make a noop move. Even then we are not sure if it works on games
 * with legal moves depending on the two games. Unfortunately the only
 * interleaved games we have are lightson parallel (for which the factoring is
 * correctly computed) and tic tac toe parlallel (for which the original propnet
 * is not correct).
 * 
 * The next step here would be to compute disjunctive factors, i.e. we play 2
 * games parallel. The challenge lies in splitting up the legal proposition, so
 * that you have different legal moves, according to the game.
 * 
 * If you want to do further work on interleaving factoring most of this code
 * should be usable and correct. The challenge within interleaving factoring is
 * to decide which move is better for a game, how do we decide which move we
 * take etc.
 * 
 * For further reading, consider the stanford ggp course at:
 * http://logic.stanford.edu/classes/cs227/2011/lectures/lecture07.pdf
 * 
 * @author flo
 * 
 */
public class Factoring {
	/** The list of independent Sets for this Factoring */
	private ArrayList<HashSet<Component>> isets = new ArrayList<HashSet<Component>>();
	/**
	 * The list for action independent sets, i.e. independent sets which each
	 * factor has to implement
	 */
	private ArrayList<HashSet<Component>> aIsets = new ArrayList<HashSet<Component>>();
	/** For each independent set we create a subpropnet */
	ArrayList<PropNet> subpropnets = new ArrayList<PropNet>();

	/** index of an independent Set */
	private int[] iSetIndex;

	/** Propositional network we want to factor */
	private PropNet pn;

	/** creates a new factoring and already computes all action independent sets */
	public Factoring(PropNet pn) {
		this.pn = pn;
		iSetIndex = new int[pn.getComponents().size()];
		Arrays.fill(iSetIndex, -1);
		createAllIS();
	}

	public ArrayList<PropNet> GetSubPropNets() {
		return subpropnets;
	}

	/**
	 * Fills an ISet with its components. While doing this, it checks if it also
	 * is part of another ISet and then just sets the componentISetnumber
	 * according to the old ISet
	 * 
	 * @param is
	 *            The indepenent Set to fill
	 * @param c
	 *            The actual component during the fill
	 * @return the ISetnumber of the actual component
	 */
	private int fillIS(HashSet<Component> is, Component c) {

		if (c.equals(pn.getInitProposition()))
			return -1;
		// check if the actual component is already in an is (independent set)
		// if it is just return the is number
		// check also if we already visited this component

		if (iSetIndex[pn.getCompToInt().get(c)] == -1 && !is.contains(c)) {
			// it isnt, so we add it to the actual is and go through every input
			is.add(c);

			for (Component component : c.getInputs())
				computeSetNumber(is, c, component);
			for (Component component : c.getOutputs())
				computeSetNumber(is, c, component);
		}
		// return the is of the actual component
		return iSetIndex[pn.getCompToInt().get(c)];
	}

	/**
	 * computes the setnumber of the given component
	 * 
	 * @param is
	 *            independent set we work on
	 * @param c
	 *            parent
	 * @param component
	 *            component to check (inputs/outputs)
	 */
	private void computeSetNumber(HashSet<Component> is, Component c,
			Component component) {
		{
			// we compute the setnumber of the input (-1 if it isnt in an
			// is)
			int setnumberofinput = fillIS(is, component);
			if (setnumberofinput != -1) {
				// if the input was in another is, we set the is of the
				// actual component to the is
				// of its component
				iSetIndex[pn.getCompToInt().get(c)] = setnumberofinput;
			}
			// if the input wasnt in another is, we set the number to the new
			// isetnumber
			// which is just the number of all isets
			else {
				iSetIndex[pn.getCompToInt().get(c)] = isets.size();
			}
		}
	}

	/**
	 * Creates an Independent Set for this component, if it already is in an is
	 * we just fill its is.
	 * 
	 * @param v
	 *            The component for which the is is created
	 */
	int createIS(Component v, HashSet<Component> is) {
		is.add(v);
		if (!(v instanceof Proposition)) {
			for (Component input : v.getInputs()) {
				int isnumber = createIS(input, is);
				iSetIndex[pn.getCompToInt().get(v)] = isnumber;

			}
		} else {
			int newisnumber = fillIS(is, v.getSingleInput());
			iSetIndex[pn.getCompToInt().get(v)] = newisnumber;
			if (newisnumber == isets.size()) {
				isets.add(is);
				return newisnumber;
			} else {
				isets.get(newisnumber).addAll(is);
				return newisnumber;
			}
		}
		return 0;
	}

	/**
	 * filters the IS from all goal/terminalprops 
	 * TODO legalprops
	 * 
	 */
	private void createAllIS() {
		for (Set<Proposition> goalprops : pn.getGoalPropositions().values()) {
			for (Proposition goalprop : goalprops) {
				Component goalgate = goalprop.getSingleInput();
				for (Component input : goalgate.getInputs()) {
					HashSet<Component> is = new HashSet<Component>();
					createIS(input, is);
				}
			}

		}
		for (Component terminputs : pn.getTerminalProposition()
				.getSingleInput().getInputs()) {
			HashSet<Component> is = new HashSet<Component>();
			createIS(terminputs, is);
		}
	}

	/**
	 * creates an action independent set for a constant
	 * 
	 * @param constant
	 */
	private void createAIs(Component constant) {
		HashSet<Component> ais = new HashSet<Component>();
		ais.add(constant);
		iSetIndex[pn.getCompToInt().get(constant)] = aIsets.size();
		for (Component output : constant.getOutputs()) {
			fillAIs(output, ais, aIsets.size());
		}
		aIsets.add(ais);
	}

	/**
	 * creates for every constant an action independent set
	 */
	void createAllAIs() {
		for (Component constant : pn.getConstants()) {
			createAIs(constant);
		}
	}

	/**
	 * fills the Action independent Set recursively with the outputs of the
	 * component
	 * 
	 * @param c
	 *            actual component
	 * @param ais
	 *            ais to fill
	 * @param aisetnumber
	 *            number of the ais
	 */
	private void fillAIs(Component c, HashSet<Component> ais, int aisetnumber) {
		ais.add(c);
		iSetIndex[pn.getCompToInt().get(c)] = aisetnumber;
		for (Component output : c.getOutputs()) {
			fillAIs(output, ais, aisetnumber);
		}

	}

	public ArrayList<HashSet<Component>> getIsets() {
		return isets;
	}

	int[] getiSetIndex() {
		return iSetIndex;
	}

	ArrayList<HashSet<Component>> getaIsets() {
		return aIsets;
	}

	/**
	 * creates the propnets for each goal proposition
	 */
	private void filterISPropNets(Set<Proposition> propositions) {
		for (Proposition endprop : propositions) {
			Component lastgate = endprop.getSingleInput();
			for (Component input : lastgate.getInputs()) {
				HashSet<Component> iSet = isets.get(iSetIndex[pn.getCompToInt()
						.get(input)]);

				// TODO what if we can split the roles for each propnet, i.e.
				// parallel games vs 2 opponents
				// create a new propnet and replace the gate with the goal
				// proposition
				// TODO can we really change our actual propnet?
				// remove the gate from the outputs
				input.getOutputs().remove(lastgate);
				// create a new endprop (goal/terminal) and add it to the
				// outputs
				Proposition newgoalprop = new Proposition(endprop.getName());
				input.getOutputs().add(newgoalprop);
				newgoalprop.getInputs().add(input);
				iSet.add(newgoalprop);
			}
		}
	}

	/**
	 * creates for each IS an extra init proposition and replaces its links
	 */
	private void transformInitProp() {
		Proposition initprop = pn.getInitProposition();
		// record the iSets for the initprop
		HashMap<Integer, Set<Component>> outputSets = new HashMap<Integer, Set<Component>>();
		for (int i = 0; i < isets.size(); i++)
			outputSets.put(i, new HashSet<Component>());

		for (Component initOutput : initprop.getOutputs()) {
			int setnumber = iSetIndex[pn.getCompToInt().get(initOutput)];
			outputSets.get(setnumber).add(initOutput);
		}
		// transform the pn for every InitISetProposition
		for (int i = 0; i < isets.size(); i++) {
			Set<Component> iSet = outputSets.get(i);
			Proposition newinitprop = new Proposition(initprop.getName());
			for (Component initOutput : iSet) {
				// delete the old initprop, add the new initprop
				initOutput.getInputs().remove(initprop);
				initOutput.getInputs().add(newinitprop);
				newinitprop.getOutputs().add(initOutput);
			}
			isets.get(i).add(newinitprop);
		}
	}

	/**
	 * transforms the propnet into smaller subpns
	 */
	private void transformPropnet() {
		for (Set<Proposition> goalprops : pn.getGoalPropositions().values()) {
			filterISPropNets(goalprops);
		}
		HashSet<Proposition> termprop = new HashSet<Proposition>();
		termprop.add(pn.getTerminalProposition());
		filterISPropNets(termprop);
		transformInitProp();
	}

	/**
	 * creates the propnet for each independent set
	 */
	private void createPropNets() {
		// merge the is with all AIs
		HashMap<Constant, Set<Proposition>> constantLegalprops = new HashMap<Constant, Set<Proposition>>();
		for (HashSet<Component> iSet : isets) {
			for (HashSet<Component> ais : aIsets)
				iSet.addAll(ais);
			PropNet newpn = new PropNet(pn.getRoles(), iSet);
			// delete obsolete legal propositions (i.e. legalmoves from other
			// games)
			for (Role role : newpn.getLegalPropositions().keySet()) {
				Set<Proposition> legalprops = newpn.getLegalPropositions().get(
						role);
				Set<Proposition> legalpropstodelete = new HashSet<Proposition>();
				for (Proposition legalprop : legalprops)
					if (!newpn.getLegalInputMap().containsValue(legalprop)) {
						// TODO maybe contain query too slow because of
						// collection as list?
						legalpropstodelete.add(legalprop);
						for (Component input : legalprop.getInputs())
							input.getOutputs().remove(legalprop);

					}

				// delete the legalprops that are obsolete for this role
				newpn.getLegalPropositions().get(role)
						.removeAll(legalpropstodelete);
				newpn.getComponents().removeAll(legalpropstodelete);
				newpn.getPropositions().removeAll(legalpropstodelete);
				newpn.getViewPropositions().removeAll(legalpropstodelete);

				// add the noop legal proposition
				GdlConstant roleconstant = role.getName().getName();
				GdlConstant noop = new GdlConstant("noop");
				ArrayList<GdlTerm> body = new ArrayList<GdlTerm>();
				body.add(roleconstant);
				body.add(noop);
				GdlFunction noopfunction = GdlPool.getFunction(
						GdlPool.getConstant("legal"), body);
				Proposition noopprop = new Proposition(noopfunction);

				// add the proposition to the pn
				newpn.getLegalPropositions().get(role).add(noopprop);
				newpn.getComponents().add(noopprop);
				newpn.getPropositions().add(noopprop);
				newpn.getViewPropositions().add(noopprop);

				// add a noop inputproposition
				body = new ArrayList<GdlTerm>();
				body.add(roleconstant);
				body.add(noop);

				GdlSentence doesSentence = GdlPool.getRelation(
						GdlPool.getConstant("does"), body);
				Proposition noopinputprop = new Proposition(
						doesSentence.toTerm());

				// GdlFunction noopinputfunction = GdlPool.getFunction(name,
				// body);
				// Proposition noopinputprop = new
				// Proposition(noopinputfunction);

				// add the proposition to the pn
				newpn.getInputPropositions().put(doesSentence.toTerm(),
						noopinputprop);
				newpn.getComponents().add(noopinputprop);
				newpn.getPropositions().add(noopinputprop);
				newpn.getLegalInputMap().put(noopprop, noopinputprop);
				newpn.getLegalInputMap().put(noopinputprop, noopprop);

				// add a true constant, so that noop is always possible
				Constant trueconst = new Constant(true);
				newpn.getConstants().add(trueconst);
				newpn.getComponents().add(trueconst);
				trueconst.getOutputs().add(noopprop);
				noopprop.getInputs().add(trueconst);

				constantLegalprops.put(trueconst, legalprops);

			}

			for (Constant constant : constantLegalprops.keySet()) {
				Set<Proposition> legalprops = constantLegalprops.get(constant);
				for (Proposition legalprop : legalprops) {
					constant.getOutputs().add(legalprop);
					legalprop.getInputs().add(constant);
				}
			}
			newpn.fillMaps();
			subpropnets.add(newpn);
		}

		int j = 0;
		for (PropNet subPropNet : subpropnets) {
			subPropNet.renderToFile("subpn" + j);
			j++;
		}
	}

	/**
	 * creates all independent (action) sets, transforms the actual propnet, so
	 * we can get multiple propnets and creates a list of new propnets
	 */
	public void getIsAndcreatePns() {
		createAllAIs();
		transformPropnet();
		createPropNets();
	}
}
