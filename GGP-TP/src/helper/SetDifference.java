package helper;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import core.gdl.GdlTerm;

/**
 * @author dimi
 * 
 */
public class SetDifference {

	static String[] blacklist = new String[] { "( control", "( step", "( pp",
			"( piece_count", "( succ", "(control", "(step", "(pp",
			"(piece_count", "(succ", "( capture", "(capture" };

	/**
	 * Creates an array with two sets, the - first containing the difference to
	 * the second - second containing the difference to the first
	 */
	public static <T> Set<T>[] difference(Set<T> firstSet, Set<T> secondSet) {
		Set<T> tmp = new HashSet<T>(firstSet);
		tmp.removeAll(secondSet);
		Set<T> tmp2 = new HashSet<T>(secondSet);
		tmp2.removeAll(firstSet);
		return new Set[] { tmp, tmp2 };
	}

	/**
	 * This method filters set(s) and removes stuff like - control - step
	 * propositions
	 * 
	 * @WARNING This method overwrites the contents of the argument set! So if
	 *          it is called like filterTiles(ms.getContents()) the contents of
	 *          the ms are changed!
	 */
	@SafeVarargs
	public static <T> void filterTiles(Set<T>... args) {
		for (Set<T> s : args) {
			Iterator<T> it = s.iterator();
			while (it.hasNext()) {
				T el = it.next();
				for (int i = 0; i < blacklist.length; i++)
					if (el.toString().toLowerCase().startsWith(blacklist[i]))
						it.remove();
			}
		}
	}

	public static boolean containsUnnecessaryPropositions(GdlTerm el) {
		for (int i = 0; i < blacklist.length; i++)
			if (el.toString().toLowerCase().startsWith(blacklist[i]))
				return true;
		return false;
	}
}
