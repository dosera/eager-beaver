package helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;

import players.uct.UCTSubPlayer;
import propnet.PropNetStateMachine;
import propnet.SuperPropNetStateMachine;
import propnet.architecture.Proposition;
import statemachine.Role;
import core.gdl.GdlConstant;
import core.gdl.GdlPool;
import core.gdl.GdlRelation;
import core.gdl.GdlTerm;
import core.logging.TheLogger;

public class StorePossibleGoalValues {
	/**
     * computes all possible outcomes for each player
     */
    public static void store(UCTSubPlayer sp) {
    	try{
            String s = "";
	        if (sp.getSM() instanceof PropNetStateMachine
	                || sp.getSM() instanceof SuperPropNetStateMachine) {
	            for (Role r : sp.getSM().getRoles()) {
	                Set<Proposition> l = ((PropNetStateMachine) (sp.getSM()))
	                        .getPropNet().getGoalPropositions().get(r);
	                List<Integer> res = new ArrayList<Integer>();
	                for (Proposition p : l) {
	                    if (!res.contains(((PropNetStateMachine) (sp.getSM()))
	                            .getGoalValue(p)))
	                        res.add(((PropNetStateMachine) (sp.getSM()))
	                                .getGoalValue(p));
	                }
	                // sort the array such that minima and maxima are in correct order!
	                Object[] x = res.toArray();
	                Arrays.sort(x);
	                List<Integer> result = new ArrayList<Integer>();
	                for (Object o : x)
	                    result.add((Integer) o);
	
	                s += "Role " + r + ": " + Arrays.toString(x) + ", ";
	                sp.putPossibleEndScores(r, result);
	            }
	
	            TheLogger.LOGGER.log(Level.FINE, s.substring(0, s.length() - 2));
	        }
	        else {
	            ConcurrentMap<GdlConstant, ConcurrentMap<List<GdlTerm>, GdlRelation>> relationpool = GdlPool.getRelationPool();
	            GdlConstant goalhead = GdlPool.getConstant("goal");
	            ConcurrentMap<List<GdlTerm>, GdlRelation> goalrelations = relationpool.get(goalhead);
	            for (Role r : sp.getSM().getRoles()) {
	                List<Integer> res =  new ArrayList<Integer>();
	              for(List<GdlTerm> term: goalrelations.keySet())
	                  if(term.get(0).toString().equals(r.toString())) {
	                       res.add((Integer.parseInt(term.get(1).toString())));
	                  }
	                // sort the array such that minima and maxima are in correct order!
		            Object[] x = res.toArray();
	                Arrays.sort(x);
	                List<Integer> result = new ArrayList<Integer>();
	                for (Object o : x)
	                    result.add((Integer) o);
	                s += "Role " + r + ": " + Arrays.toString(x) + ", ";
		            sp.putPossibleEndScores(r, result);
	            }
	            TheLogger.LOGGER.log(Level.FINE, s.substring(0, s.length() - 2));
	        }
    	}
    	catch (Exception e)
    	{
    		for (Role r: sp.getSM().getRoles())
    		{
    			sp.putPossibleEndScores(r, new ArrayList<Integer>());
    		}
    		TheLogger.LOGGER.log(Level.FINE, "storepossiblegoalValues method could not generate goal values since they are computed on runtime");
    	}
    }

}
