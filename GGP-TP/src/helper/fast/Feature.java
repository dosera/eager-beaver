package helper.fast;

import core.gdl.GdlSentence;
import core.gdl.GdlTerm;

public class Feature {

	private GdlTerm content;
	public GdlTerm getContent() { return this.content; }
	
	public Feature(GdlTerm content) {
		this.content = content;
	}
	
	public boolean contentEquals(GdlSentence other) {
		return this.content.toSentence().equals(other);
	}
	
	@Override
	public String toString() {
		return content.toString();
	}
}
