package players;

import java.util.List;
import java.util.Random;

import propnet.PropNetStateMachine;

import statemachine.Move;
import statemachine.StateMachine;
import statemachine.StateMachineGamer;
import statemachine.prover.CachedProverStateMachine;

import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;

/**
 * RandomGamer is a very simple state-machine-based Gamer that will always
 * pick randomly from the legal moves it finds at any state in the game. This
 * is one of a family of simple "reflex" gamers which act entirely on reflex
 * (picking the first legal move, or a random move) regardless of the current
 * state of the game.
 * 
 * This is not really a serious approach to playing games, and is included in
 * this package merely as an example of a functioning Gamer.
 */
public final class PropNetRandomGamer extends StateMachineGamer
{
        /**
         * Does nothing
         */
        @Override
        public void stateMachineMetaGame(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException
        {
                // Do nothing.
        }
        /**
         * Selects a random legal move
         */
        @Override
        public Move stateMachineSelectMove(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException
        {
                long start = System.currentTimeMillis();

                List<Move> moves = getStateMachine().getLegalMoves(getCurrentState(), getRole());
                Move selection = (moves.get(new Random().nextInt(moves.size())));

                long stop = System.currentTimeMillis();

                return selection;
        }
        @Override
        public void stateMachineStop() {
                // Do nothing.
        }
        /**
         * Uses a PropNetStateMachine
         */
        @Override
        public StateMachine getInitialStateMachine() {
                return new PropNetStateMachine();
        }

        @Override
        public String getName() {
                return "Random";
        }
}
