package players.uct.strategyhandler;

import java.util.HashMap;

import players.uct.UCTSubPlayer;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.expansion.AExpansion;
import statemachine.Move;
import core.Settings;

public abstract class MastHandler extends AStrategyHandler {

	// this hashmap is used for the MAST Heuristic
	private HashMap<Move, float[]> mastMap = new HashMap<Move, float[]>();

	public HashMap<Move, float[]> getMastMap() {
		return this.mastMap;
	}

	public MastHandler(UCTSubPlayer sp, AExpansion expansion,
			AHeuristic heuristic) {
		super(sp, expansion, heuristic);
	}

	@Override
	public String toString() {
		return super.toString() + ", treeOnlyMast: "
				+ (Settings.treeOnlyMast ? "true" : "false");
	}
}
