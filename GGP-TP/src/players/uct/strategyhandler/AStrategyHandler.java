/**
 * 
 */
package players.uct.strategyhandler;

import java.util.ArrayList;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.ABackpropagation;
import players.uct.evaluation.AEvaluation;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.tree.Node;
import core.Settings;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;

/**
 * @author Phil
 * 
 *         Specifies a strategy for a UCTSubPlayer. A Strategy defines which
 *         extensions we use for our UCT computation.
 */
public abstract class AStrategyHandler {

	protected ASelection selection;
	protected AExpansion expansion;
	protected AEvaluation simulation;
	protected ABackpropagation backpropagation;
	protected UCTSubPlayer sp;
	protected AHeuristic heuristic;

	// list with last moves
	public ArrayList<Node> lastSelectedNodes = new ArrayList<Node>();

	public AStrategyHandler(UCTSubPlayer sp, AExpansion expansion,
			AHeuristic heuristic) {
		this.sp = sp;
		this.expansion = expansion;
		this.heuristic = heuristic;
	}

	public AHeuristic getHeuristic() {
		return this.heuristic;
	}

	public ASelection getOtherSelection() {
		return null;
	}

	/**
	 * This method is executed before the four phases begin.
	 */
	public void preComputeEveryRun() {
		this.lastSelectedNodes = new ArrayList<Node>();
	}

	/**
	 * Recursive selection call.
	 * @return selected node.
	 */
	public Node select(Node node, boolean maximize, int[] depthOfLeafNodeArray) {
		if (node.getChildren().size() == 0 || node.hasUnexpandedChildren()) {
			this.lastSelectedNodes.add(node);
			return node;
		} else {
			// if we only have one possible move to play
			// -> maximize reward of the opponent
			// if we have more than one opponent -> ? TODO
			Node nextNode = maximizeByRole(node, maximize);
			// all children are currently locked
			if (nextNode == null)
				return null;
			this.lastSelectedNodes.add(node);
			// increase the depth of the current LeafNode by 1
			depthOfLeafNodeArray[0] += 1;
			return select(nextNode, maximize, depthOfLeafNodeArray);
		}
	}

	/**
	 * Expansion call.
	 * @param leafNode
	 * @return the newly expanded children.
	 * @throws GoalDefinitionException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 */
	public ArrayList<Node> expand(Node leafNode)
			throws GoalDefinitionException, MoveDefinitionException,
			TransitionDefinitionException {
		ArrayList<Node> result = new ArrayList<Node>();
		if (sp.getSM().isTerminal(leafNode.getMS())) {
			float[] winsarray = new float[sp.getSM().getRoles().size()];
			for (int i = 0; i < winsarray.length; i++) {
				winsarray[i] = (float) (sp.getSM().getGoal(leafNode.getMS(),
						sp.getRoleIndexMap().getKey(i)) * .01);
			}
			leafNode.updateValues(winsarray);
			result.add(leafNode);
		} else {
			result = this.expansion.expandTree(leafNode, sp);
		}
		return result;
	}

	/**
	 * Simulation call.
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws GoalDefinitionException
	 */
	public void simulate(ArrayList<Node> expandedChildren)
			throws MoveDefinitionException, TransitionDefinitionException,
			GoalDefinitionException {
		this.simulation.eval(expandedChildren, Settings.evaluationDepth);
	}

	/**
	 * Backpropagation call.
	 */
	public void backpropagate(ArrayList<Node> expandedChildren) {
		for (Node child : expandedChildren)
			this.backpropagation.propagate(child, child.lastScore, this,
					this.sp);
	}

	/**
	 * This method is executed after the four phases.
	 */
	public void postComputeEveryRun(Node leafNode) {
		// adjust the bias for the uctcomputation
		if (Settings.updateBias)
			this.selection.updateBias(leafNode, this.sp);
	}
	
	public Node maximizeByRole(Node node, boolean maximize) {
		Node nextNode = null;
		// we have only one move available and we are not in a singleplayer game
		if (node.getPossibleMovesOfRoleIndex()[sp.getMyRoleIndex()] == 1
				&& !(sp.getRoleIndexMap().size() == 1)) {
			if (maximize)
				nextNode = selection
						.calculateBestNode(node, (sp.getMyRoleIndex() + 1)
								% (sp.getRoleIndexMap().size() - 1), this, true);
			else
				nextNode = selection.calculateBestNode(node,
						sp.getMyRoleIndex(), this, false);

		} else
			nextNode = selection.calculateBestNode(node, sp.getMyRoleIndex(),
					this, true);
		return nextNode;
	}

	@Override
	public String toString() {
		return String.format("%s: %s, %s, %s %s", this
				.getClass().getSimpleName(), this.selection.getClass()
				.getSimpleName(), this.expansion.getClass().getSimpleName(),
				"updateBias: " + (Settings.updateBias ? "true" : "false"),
				(Settings.evaluationDepth > 0) ? ", "
						+ this.heuristic.getClass().getSimpleName() : "");

	}
}
