package players.uct.strategyhandler.methods;

import java.util.HashMap;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationFastMast;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.FastMastMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.strategyhandler.FastHandler;
import statemachine.Move;
import core.Settings;

public class FastMastStrategy extends FastHandler {

	private HashMap<Move, float[]> mastMap = new HashMap<Move, float[]>();

	public HashMap<Move, float[]> getMastMap() {
		return this.mastMap;
	}

	public FastMastStrategy(UCTSubPlayer sp, AExpansion expansion,
			ASelection selection, AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		super.selection = selection;
		super.simulation = new FastMastMCSimulation(sp, this);
		super.backpropagation = new BackpropagationFastMast();
	}

	public String toString() {
		return super.toString() + ", treeOnlyMast: "
				+ (Settings.treeOnlyMast ? "true" : "false");
	}
}
