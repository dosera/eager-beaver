package players.uct.strategyhandler.methods;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationRavePastFast;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.RavePastFastMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.selection.RaveComputation;
import core.Settings;

public class RavePastFastStrategy extends FastPastStrategy {

	private ASelection otherSelection;

	public RavePastFastStrategy(UCTSubPlayer sp, AExpansion expansion,
			ASelection otherselection, AHeuristic heuristic) {
		super(sp, expansion, new RaveComputation(), heuristic);
		this.otherSelection = otherselection;
		super.simulation = new RavePastFastMCSimulation(sp, this);
		super.backpropagation = new BackpropagationRavePastFast();
	}

	@Override
	public ASelection getOtherSelection() {
		return this.otherSelection;
	}

	@Override
	public String toString() {
		return super.toString() + ", betterBeta: "
				+ (Settings.betterBeta ? "true" : "false");
	}

}
