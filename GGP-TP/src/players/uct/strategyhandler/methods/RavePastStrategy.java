package players.uct.strategyhandler.methods;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationPastRave;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.RavePastMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.selection.RaveComputation;
import players.uct.strategyhandler.PastHandler;
import core.Settings;

public class RavePastStrategy extends PastHandler {

	private ASelection otherSelection;

	public RavePastStrategy(UCTSubPlayer sp, AExpansion expansion,
			ASelection otherselection, AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		super.selection = new RaveComputation();
		this.otherSelection = otherselection;
		super.simulation = new RavePastMCSimulation(sp, this);
		super.backpropagation = new BackpropagationPastRave();
	}

	public ASelection getOtherSelection() {
		return otherSelection;
	}

	@Override
	public String toString() {
		return super.toString() + ", betterBeta: "
				+ (Settings.betterBeta ? "true" : "false");
	}

}
