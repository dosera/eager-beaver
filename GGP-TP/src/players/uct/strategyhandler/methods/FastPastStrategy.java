package players.uct.strategyhandler.methods;

import java.util.HashMap;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationFastPast;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.FastPastMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.strategyhandler.FastHandler;
import statemachine.Move;
import core.gdl.GdlSentence;

public class FastPastStrategy extends FastHandler {

	public HashMap<Move, HashMap<GdlSentence, float[]>> actionPredicateValues;

	public FastPastStrategy(UCTSubPlayer sp, AExpansion expansion,
			ASelection selection, AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		this.actionPredicateValues = new HashMap<Move, HashMap<GdlSentence, float[]>>();
		super.selection = selection;
		super.simulation = new FastPastMCSimulation(sp, this);
		super.backpropagation = new BackpropagationFastPast();
	}

}
