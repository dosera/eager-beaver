package players.uct.strategyhandler.methods;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationST;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.FastMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.strategyhandler.FastHandler;

/**
 * @author Phil
 * 
 *         UCT with FAST extension.
 */
public class FastStrategy extends FastHandler {

	public FastStrategy(UCTSubPlayer sp, ASelection selection,
			AExpansion expansion, AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		super.selection = selection;
		super.simulation = new FastMCSimulation(sp, this);
		super.backpropagation = new BackpropagationST();
	}
}
