package players.uct.strategyhandler.methods;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationRAVE;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.RaveFastMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.selection.RaveComputation;
import players.uct.strategyhandler.FastHandler;
import core.Settings;

public class RaveFastStrategy extends FastHandler {
	// Rave Stuff
	private ASelection otherSelection;

	public ASelection getOtherSelection() {
		return otherSelection;
	}

	public RaveFastStrategy(UCTSubPlayer sp, AExpansion expansion,
			ASelection otherselection, AHeuristic heuristic) {

		super(sp, expansion, heuristic);
		super.selection = new RaveComputation();
		this.otherSelection = otherselection;
		super.simulation = new RaveFastMCSimulation(sp, this);
		super.backpropagation = new BackpropagationRAVE();
	}

	@Override
	public String toString() {
		return super.toString() + ", betterBeta: "
				+ (Settings.betterBeta ? "true" : "false");
	}
}
