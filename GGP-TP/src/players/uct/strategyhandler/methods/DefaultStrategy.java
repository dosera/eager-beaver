package players.uct.strategyhandler.methods;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationST;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.SimpleMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.strategyhandler.AStrategyHandler;

/**
 * @author Phil
 * 
 *         Basic version of the UCT algorithm.
 */
public class DefaultStrategy extends AStrategyHandler {

	public DefaultStrategy(UCTSubPlayer sp, ASelection selection,
			AExpansion expansion, AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		super.selection = selection;
		super.simulation = new SimpleMCSimulation(sp, this);
		super.backpropagation = new BackpropagationST();
	}
}
