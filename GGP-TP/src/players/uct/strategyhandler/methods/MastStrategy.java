package players.uct.strategyhandler.methods;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationMAST;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.MastMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.strategyhandler.MastHandler;

/**
 * @author Phil
 * 
 *         UCT with MAST extension. The MAST extension is used to bias the
 *         random rollout. The basic idea of MAST: prefer actions that have been
 *         good in previous steps.
 */
public class MastStrategy extends MastHandler {

	public MastStrategy(UCTSubPlayer sp, ASelection selection,
			AExpansion expansion, AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		super.selection = selection;
		super.simulation = new MastMCSimulation(sp, this);
		super.backpropagation = new BackpropagationMAST();
	}
}
