package players.uct.strategyhandler.methods;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationMastRave;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.RaveMastMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.selection.RaveComputation;
import players.uct.strategyhandler.MastHandler;
import core.Settings;

public class RaveMastStrategy extends MastHandler {

	private ASelection otherSelection;

	public RaveMastStrategy(UCTSubPlayer sp, AExpansion expansion,
			ASelection otherselection, AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		super.selection = new RaveComputation();
		this.otherSelection = otherselection;
		super.simulation = new RaveMastMCSimulation(sp, this);
		super.backpropagation = new BackpropagationMastRave();
	}

	public ASelection getOtherSelection() {
		return otherSelection;
	}

	@Override
	public String toString() {
		return super.toString() + ", betterBeta: "
				+ (Settings.betterBeta ? "true" : "false");
	}

}
