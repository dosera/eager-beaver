package players.uct.strategyhandler.methods;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationPAST;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.PastMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.strategyhandler.PastHandler;

public class PastStrategy extends PastHandler {

	public PastStrategy(UCTSubPlayer sp, ASelection selection,
			AExpansion expansion, AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		super.selection = selection;
		super.simulation = new PastMCSimulation(sp, this);
		super.backpropagation = new BackpropagationPAST();
	}
}
