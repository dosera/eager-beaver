package players.uct.strategyhandler.methods;

import players.uct.UCTSubPlayer;
import players.uct.backpropagation.BackpropagationRAVE;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.evaluation.simulation.RaveMCSimulation;
import players.uct.expansion.AExpansion;
import players.uct.selection.ASelection;
import players.uct.selection.RaveComputation;
import players.uct.strategyhandler.AStrategyHandler;
import core.Settings;

/**
 * @author Phil
 * 
 *         UCT with RAVE extension. The basic idea of RAVE: use the information
 *         gathered by earlier rollouts to bias the selection.
 */
public class RaveStrategy extends AStrategyHandler {

	private ASelection otherSelection;

	public RaveStrategy(UCTSubPlayer sp, AExpansion expansion,
			ASelection otherselection, AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		super.selection = new RaveComputation();
		this.otherSelection = otherselection;
		super.simulation = new RaveMCSimulation(sp, this);
		super.backpropagation = new BackpropagationRAVE();
	}

	@Override
	public ASelection getOtherSelection() {
		return this.otherSelection;
	}

	@Override
	public String toString() {
		return super.toString() + ", betterBeta: "
				+ (Settings.betterBeta ? "true" : "false");
	}
}
