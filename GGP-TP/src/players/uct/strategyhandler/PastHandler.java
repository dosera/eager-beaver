package players.uct.strategyhandler;

import java.util.HashMap;

import core.gdl.GdlSentence;
import players.uct.UCTSubPlayer;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.expansion.AExpansion;
import statemachine.Move;

public class PastHandler extends AStrategyHandler {

	// TODO: find a more efficient way of storing these values
	public HashMap<Move, HashMap<GdlSentence, float[]>> actionPredicateValues;
	
	public PastHandler(UCTSubPlayer sp, AExpansion expansion,
			AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		this.actionPredicateValues = new HashMap<Move, HashMap<GdlSentence, float[]>>();
	}

}
