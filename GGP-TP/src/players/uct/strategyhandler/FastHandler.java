package players.uct.strategyhandler;

import helper.SetDifference;
import helper.fast.Feature;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import players.uct.UCTSubPlayer;
import players.uct.evaluation.methods.AHeuristic;
import players.uct.expansion.AExpansion;
import propnet.PropNetStateMachine;
import statemachine.MachineState;
import statemachine.StateMachineGamer;
import core.gdl.GdlConstant;
import core.gdl.GdlTerm;

/**
 * currently FAST is <very> slow due to computing feature weights for 
 * EVERY role in EVERY simulation step
 * DON'T USE THIS EXTENSION!
 */
public abstract class FastHandler extends AStrategyHandler { 
	// constants used by FAST
	public static final float INITIAL_FEATURE_WEIGHT = 0f;
	public static final float ALPHA = 0.01f;
	public static final float LAMBDA = 0.99f;
	public static final float GAMMA = 0.999f; // close to no decay
	public static final float ZERO_MARGIN = 1E-6f;
	public static final float C = .5f;
	// this constant is needed for the combining scheme
	public static final float OMEGA = 1f;
	public boolean piecebased = false;

	// keeps track of the MachineStates encountered along the last simulation
	// note: the state from which the simulation started is not contained in the
	// list
	// the terminal state is at the last position
	public ArrayList<MachineState> episode;
	private ArrayList<Float[]> featureWeights;

	public ArrayList<Float[]> getFeatureWeights() {
		return featureWeights;
	}

	public void setFeatureWeights(ArrayList<Float[]> featureWeights) {
		this.featureWeights = featureWeights;
	}
	// this HashMap is stores all detected features and their indices used
	// throught FAST
	private ArrayList<Feature> featureList = new ArrayList<Feature>();
	
	public ArrayList<Feature> getFeatureList() {
		return this.featureList;
	}

	public FastHandler(UCTSubPlayer sp, AExpansion expansion,
			AHeuristic heuristic) {
		super(sp, expansion, heuristic);
		// fill the FAST Map, this only works for boardgames like tic tac toe
		// ...
		// not for stuff like chess xor skirmish
		// take into account if the board is changing in a game!
		this.extractFeatures();
		// we don't need the propnet anymore
		StateMachineGamer.propNet = null;
	}

	/**
	 * Extract features and detect if we have a piece or cell based game.
	 */
	protected void extractFeatures() {
		// FAST currently only works if the propnet has been built
		// (that means it also works if the propnet creation fails but the propnet has been created)
		Set<GdlTerm> featureSet = (StateMachineGamer.propNet == null ? 
				((PropNetStateMachine)sp.getSM()).getPropNet().getBasePropositions().keySet() :
					StateMachineGamer.propNet.getBasePropositions().keySet());
			
		this.setFeatureWeights(new ArrayList<Float[]>());
		this.getFeatureList().clear();

		// we need this set to detect if we have a cell based or a piece based
		// game
		Set<GdlTerm> lastElements = new HashSet<GdlTerm>();
		
		Iterator<GdlTerm> it = featureSet.iterator();
		while (it.hasNext()) {
			GdlTerm gdlf = it.next();

			// TODO rethink 
//			if(gdlf instanceof GdlConstant)
//			{
//				it.remove();
//				continue;
//			}
			// TODO
			// we don't want to have the control base propositions in our
			// feature list,
			// therefore we do a simple string comparison
			// do this in another way
			if (!SetDifference.containsUnnecessaryPropositions(gdlf)) {
				Feature feature = new Feature(gdlf);
				if(gdlf instanceof GdlConstant)
				{
					featureSet.remove(gdlf);
					continue; // TODO think about this if..
				}
				if (gdlf.toSentence().getBody().size() == 3 && // (<predicate-name><column><row><piece>)
						!gdlf.toSentence().getBody()
								.get(gdlf.toSentence().getBody().size() - 1)
								.toString().equalsIgnoreCase("b"))
				// we don't want to have blank cells, TODO, string comparison
				// sucks
				{
					lastElements.add(gdlf.toSentence().getBody()
							.get(gdlf.toSentence().getBody().size() - 1));
				}
				this.getFeatureList().add(feature);
				Float[] initialFeatureWeights = new Float[sp.getRoleIndexMap().size()];
				Arrays.fill(initialFeatureWeights, FastHandler.INITIAL_FEATURE_WEIGHT);
				this.getFeatureWeights().add(initialFeatureWeights);
			}
		}
		// we have more "pieces" than players -> piece based game
		if (super.sp.getRoleIndexMap().size() < lastElements.size()) {
			this.getFeatureList().clear();
			this.getFeatureWeights().clear();
			for (GdlTerm gdlt : lastElements) {
				this.getFeatureList().add(new Feature(gdlt));
				Float[] initialFeatureWeights = new Float[sp.getRoleIndexMap().size()];
				Arrays.fill(initialFeatureWeights,
						FastHandler.INITIAL_FEATURE_WEIGHT);
				this.getFeatureWeights().add(initialFeatureWeights);
			}
			piecebased = true;
		}
	}

}
