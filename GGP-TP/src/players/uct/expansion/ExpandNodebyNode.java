/**
 * 
 */
package players.uct.expansion;

import java.util.ArrayList;

import players.uct.tree.Node;

/**
 * @author tasse
 * 
 *         Takes a node and inserts its successors to the tree once at a time.
 */
public class ExpandNodebyNode extends AExpansion {
	@Override
	public ArrayList<Node> expand(Node n) {
		ArrayList<Node> result = new ArrayList<Node>();
		Node expandedNode = n.getPossibleChildren().poll();
		if (expandedNode == null)
			return result;
		n.getChildren().add(expandedNode);
		result.add(expandedNode);
		return result;
	}
}
