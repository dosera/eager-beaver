/**
 * 
 */
package players.uct.expansion;

import java.util.ArrayList;

import players.uct.tree.Node;

/**
 * @author Phil
 * 
 *         Takes a node and inserts all its successors to the tree at once.
 */
public class ExpandAllNodes extends AExpansion {
	@Override
	public ArrayList<Node> expand(Node n) {
		ArrayList<Node> result = new ArrayList<Node>();
		for (int i = 0; i < n.getPossibleChildren().size(); i++) {
			Node expandedNode = n.getPossibleChildren().poll();
			if (expandedNode == null)
				break;
			n.getChildren().add(expandedNode);
			result.add(expandedNode);
		}
		return result;
	}
}
