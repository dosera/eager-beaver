/**
 * 
 */
package players.uct.expansion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import players.uct.UCTGamer;
import players.uct.UCTSubPlayer;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;
import core.logging.TheLogger;

/**
 * @author Phil
 * 
 *         Implements the second step of the UCT algorithm.
 */
public abstract class AExpansion {

	public abstract ArrayList<Node> expand(Node n);

	public ArrayList<Node> expandTree(Node n, UCTSubPlayer sp)
			throws MoveDefinitionException, TransitionDefinitionException {
		if (n.hasUnexpandedChildren() && n.getPossibleChildren().size() == 0) {
			@SuppressWarnings("unchecked")
			List<Move>[] movesPerRole = new ArrayList[sp.getSM().getRoles()
					.size()];

			for (int i = 0; i < sp.getSM().getRoles().size(); i++) {
				Role r = sp.getSM().getRoles().get(i);
				int index = sp.getRoleIndexMap().get(r);
				movesPerRole[index] = sp.getSM().getLegalMoves(n.getMS(), r);
				// add all possible moves for each index to the
				// possiblemovesofroleindex map
				n.getPossibleMovesOfRoleIndex()[index] = movesPerRole[index]
						.size();
			}
			ArrayList<Move>[] cartesianCombinations = AExpansion
					.calculateCartesianCombinations(movesPerRole);

			for (int i = 0; i < cartesianCombinations.length; i++) {
				Move[] movesInArray = new Move[cartesianCombinations[i].size()];
				for (int j = 0; j < cartesianCombinations[i].size(); j++)
					movesInArray[j] = cartesianCombinations[i].get(j);

				Node child = new Node(n, new ArrayList<Node>(),
						new LinkedList<Node>(), sp.getSM().getNextState(
								n.getMS(), cartesianCombinations[i]),
						movesInArray, cartesianCombinations[i].size());

				// is this child terminal?
				this.checkTerminalChild(child, sp);

				// add to the nodes queue
				n.getPossibleChildren().add(child);
			}
		}

		ArrayList<Node> expandedChildren = expand(n);

		if (n.getPossibleChildren().size() == 0)
			n.setHasUnexpandedChildren(false);

		return expandedChildren;
	}

	/**
	 * calculates the Cartesian product of the according list elements
	 * 
	 * @param <E>
	 * @param movesPerRole
	 * @return
	 */
	public static <E> ArrayList<E>[] calculateCartesianCombinations(
			List<E>[] movesPerRole) {
		List<List<E>> input = Arrays.asList(movesPerRole);
		List<List<E>> res = cartesianProduct(input);
		@SuppressWarnings("unchecked")
		ArrayList<E>[] result = new ArrayList[res.size()];
		for (int i = 0; i < res.size(); i++) {
			result[i] = (ArrayList<E>) res.get(i);
		}
		return result;
	}

	static <E> List<List<E>> cartesianProduct(List<List<E>> lists) {
		List<List<E>> resultLists = new ArrayList<List<E>>();
		if (lists.size() == 0) {
			resultLists.add(new ArrayList<E>());
			return resultLists;
		} else {
			List<E> firstList = lists.get(0);
			List<List<E>> remainingLists = cartesianProduct(lists.subList(1,
					lists.size()));
			for (E condition : firstList) {
				for (List<E> remainingList : remainingLists) {
					ArrayList<E> resultList = new ArrayList<E>();
					resultList.add(condition);
					resultList.addAll(remainingList);
					resultLists.add(resultList);
				}
			}
		}
		return resultLists;
	}

	/**
	 * fills the "listmoves" with all legal moves of each role
	 * 
	 * @param gamer
	 *            : currentgamer
	 * @param listmoves
	 *            : empty list that is filled
	 * @throws MoveDefinitionException
	 */
	protected void movesOfOthersRandom(UCTGamer gamer, Move[] movesperrole,
			MachineState currentState) throws MoveDefinitionException {
		Random rand = new Random();
		for (Role r : gamer.getStateMachine().getRoles()) {
			if (!r.equals(gamer.getRole())) {
				List<Move> hisLegalMoves = gamer.getStateMachine()
						.getLegalMoves(currentState, r);
				int random = rand.nextInt(hisLegalMoves.size());
				movesperrole[gamer.getStateMachine().getRoleIndices().get(r)] = (hisLegalMoves
						.get(random));
			}
		}
	}

	/**
	 * is the child terminal?
	 * 
	 * @param child
	 */
	protected void checkTerminalChild(Node child, UCTSubPlayer sp) {
		if (sp.getSM().isTerminal(child.getMS())) {
			List<Integer> results = new ArrayList<Integer>();
			try {
				results = sp.getSM().getGoals(child.getMS());
			} catch (GoalDefinitionException e) {
				TheLogger.LOGGER.log(Level.SEVERE, "error", e);
			}
			// only do this in multiplayer games
			if (results.size() > 1)
				multiplayerCheck(child, results, sp);
			else
				singleplayerCheck(child, results, sp);
			child.setNearestGoalDepth(0);
			child.setNearestGoalScore(results.get(sp.getMyRoleIndex()));
		}
	}

	/**
	 * @param child
	 * @param results
	 */
	private void singleplayerCheck(Node child, List<Integer> results,
			UCTSubPlayer sp) {
		List<Integer> possibleOutcomes = sp
				.getPossibleEndScores(sp.getMyRole());
		for (int i = 0; i < possibleOutcomes.size(); i++) {
			// win
			if (results.get(sp.getMyRoleIndex()) >= possibleOutcomes
					.get(possibleOutcomes.size() - 1))
				child.setLeadsToWin(true);
			// defeat
			if (results.get(sp.getMyRoleIndex()) <= possibleOutcomes.get(0))
				child.setLeadsToDefeat(true);
		}
	}

	/**
	 * @param child
	 * @param results
	 */
	private void multiplayerCheck(Node child, List<Integer> results,
			UCTSubPlayer sp) {
		boolean iAmBetterThanSbElse = false; 
		// TODO: philosophical question:
		// shall this boolean be set if
		// i am worse than ALL others or just worse than ONE?
		/*
		 * EXAMPLE (Three Player Connect 4):
		 * 
		 * boolean set if i am worse than ONE: move that leads to defeat doesn't
		 * get blocked because i am better than at least one other player
		 * boolean set if i am worse than ALL OTHERS: (currently set) move leads
		 * to a situation where i am worse than any other player, this means i
		 * accept as long i am not the worst
		 */
		boolean amIBetterThanAnybodyElse = true;
		for (int l = 0; l < results.size(); l++) {
			// defeat
			if (results.get(sp.getMyRoleIndex()) >= results.get(l)
					&& !(l == sp.getMyRoleIndex())) {
				iAmBetterThanSbElse = true;
			}
			// win
			if (results.get(sp.getMyRoleIndex()) < results.get(l)
					&& !(l == sp.getMyRoleIndex())) {
				amIBetterThanAnybodyElse = false;
			}
		}
		if (!iAmBetterThanSbElse)
			child.setLeadsToDefeat(true);
		if (amIBetterThanAnybodyElse)
			child.setLeadsToWin(true);
	}
}
