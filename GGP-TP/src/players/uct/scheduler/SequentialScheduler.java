/**
 * 
 */
package players.uct.scheduler;

import java.util.HashMap;
import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.tree.Node;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;

/**
 * @author tasse
 * 
 *         UCTSubPlayers are executed in a sequential way.
 */
public class SequentialScheduler extends AScheduler {

	public SequentialScheduler(List<UCTSubPlayer> spList) {
		// ...
		super(spList);
	}

	@Override
	public HashMap<UCTSubPlayer, List<Node>> run(long timeAvailable)
			throws MoveDefinitionException, TransitionDefinitionException,
			GoalDefinitionException {
		long finishUntilDate = System.currentTimeMillis() + timeAvailable;
		int j = 0;

		while (System.currentTimeMillis() < finishUntilDate) {
			super.spList.get(j).run();
			j = (j + 1) % super.spList.size();
		}
		return super.allResults(super.spList);

	}
}
