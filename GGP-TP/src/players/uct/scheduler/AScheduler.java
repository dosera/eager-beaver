/**
 * 
 */
package players.uct.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.tree.Node;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;

/**
 * @author tasse
 * 
 *         Abstract class for schedulers. Schedulers handle the execution of the
 *         UCTSubPlayers.
 */
public abstract class AScheduler {

	protected final List<UCTSubPlayer> spList;

	public AScheduler(List<UCTSubPlayer> spList) {
		this.spList = spList;
	}

	/**
	 * Starts the scheduler. It is expected to stop after timeAvailable ms.
	 */
	public abstract HashMap<UCTSubPlayer, List<Node>> run(final long timeAvailable)
			throws MoveDefinitionException, TransitionDefinitionException,
			GoalDefinitionException;

	/**
	 * Extracts for each subplayer the potential moves (aka children).
	 */
	protected HashMap<UCTSubPlayer, List<Node>> allResults(
			List<UCTSubPlayer> list) {
		HashMap<UCTSubPlayer, List<Node>> result = new HashMap<UCTSubPlayer, List<Node>>();
		for (int k = 0; k < spList.size(); k++) {
			List<Node> resNodes = new ArrayList<Node>();
			for (int l = 0; l < spList.get(k).getTree().getRootNode()
					.getChildren().size(); l++) {
				resNodes.add(spList.get(k).getTree().getRootNode()
						.getChildren().get(l));
			}
			result.put(spList.get(k), resNodes);
		}
		return result;
	}
}
