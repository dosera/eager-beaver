package players.uct.scheduler;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

import players.uct.UCTSubPlayer;
import players.uct.tree.Node;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;
import core.logging.TheLogger;

/**
 * @author tasse
 * 
 *         UCTSubPlayers are executed in parallel using threads and a fixed
 *         threadPool. Currently there is one thread per UCTSubPlayer - this
 *         should be reworked if possible. TODO
 */
public class UCTThreadScheduler extends AScheduler {
	ExecutorService threadPool;

	public UCTThreadScheduler(List<UCTSubPlayer> spList)
			throws MoveDefinitionException, TransitionDefinitionException {
		super(spList);
	}

	@Override
	public HashMap<UCTSubPlayer, List<Node>> run(final long timeout)
			throws MoveDefinitionException, TransitionDefinitionException,
			GoalDefinitionException {
		this.threadPool = Executors.newFixedThreadPool(spList.size());
		final long time = System.currentTimeMillis();

		for (int i = 0; i < super.spList.size(); i++) {
			final int x = i;
			Runnable runnableSubPlayer = new Runnable() {
				@Override
				public void run() {
					while (System.currentTimeMillis() < timeout + time) {
						try {
							spList.get(x).run();
						} catch (MoveDefinitionException e) {
							TheLogger.LOGGER.log(Level.SEVERE,
									"error in UCTSubPlayer", e);
						} catch (TransitionDefinitionException e) {
							TheLogger.LOGGER.log(Level.SEVERE,
									"error in UCTSubPlayer", e);
						} catch (GoalDefinitionException e) {
							TheLogger.LOGGER.log(Level.SEVERE,
									"error in UCTSubPlayer", e);
						}
					}
				}
			};

			threadPool.submit(runnableSubPlayer);
		}

		while (System.currentTimeMillis() < timeout + time) {
			//
		}

		threadPool.shutdownNow();
		return super.allResults(super.spList);
	}
}
