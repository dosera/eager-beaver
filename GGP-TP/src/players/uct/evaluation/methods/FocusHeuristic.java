package players.uct.evaluation.methods;

import players.uct.UCTSubPlayer;
import statemachine.MachineState;
import core.exceptions.MoveDefinitionException;

/** 
 * @author tasse
 *
 * Heuristic that favors machine states that leave the least open 
 * move possibilities for ourselves
 */
public class FocusHeuristic extends AHeuristic {

	public int maxNumMoves = 1;
	@Override
	public float[] rateMS(MachineState ms, UCTSubPlayer sp) throws MoveDefinitionException {
		float[] result = new float[sp.getRoleIndexMap().size()];
		for(int i = 0; i < result.length; i++)
		{		
			int numMoves = sp.getSM().getLegalMoves(ms, sp.getRoleIndexMap().getKey(i)).size();
			if(maxNumMoves < numMoves)
				maxNumMoves = numMoves;
			result[i] = 1 - (float)((float)numMoves / (float)maxNumMoves);
		}
		return result;
	}

}
