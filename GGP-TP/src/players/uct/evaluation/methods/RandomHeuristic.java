package players.uct.evaluation.methods;

import java.util.Random;

import players.uct.UCTSubPlayer;

import statemachine.MachineState;

/**
 * @author tasse
 *
 * Rates states randomly.
 */
public class RandomHeuristic extends AHeuristic {

	@Override
	public float[] rateMS(MachineState ms, UCTSubPlayer sp) {
		Random rand = new Random();
		float[] scores = new float[sp.getRoleIndexMap().size()];
		for(int i = 0; i < scores.length; i++)
		{
			scores[i] = rand.nextFloat();
		}
		return scores;
	}
}
