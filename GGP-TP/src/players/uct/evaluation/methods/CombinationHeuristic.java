package players.uct.evaluation.methods;

import java.util.HashMap;
import java.util.List;

import players.uct.UCTSubPlayer;
import statemachine.MachineState;
import core.exceptions.MoveDefinitionException;

/**
 * @author Phil
 *
 * Class used to merge the values of different weighted heuristics.
 */
public class CombinationHeuristic extends AHeuristic{
	
	private HashMap<AHeuristic, Float> combinations;
	public CombinationHeuristic(List<AHeuristic> combinations, List<Float> weights)
	{
		for (int i = 0; i < combinations.size(); i++)
		{
			this.combinations.put(combinations.get(i), weights.get(i));
		}
		
	}
	@Override
	public float[] rateMS(MachineState ms, UCTSubPlayer sp)
			throws MoveDefinitionException {
		
		float[] result = new float[sp.getRoleIndexMap().size()];
		float[] scores = new float[result.length];
		for (AHeuristic h : combinations.keySet())
		{
			scores = h.rateMS(ms, sp);
			for (int i = 0; i < scores.length; i++)
			{
				result[i] += scores[i] * this.combinations.get(h);
			}
		}
		
		return result;
	}
	
	
}
