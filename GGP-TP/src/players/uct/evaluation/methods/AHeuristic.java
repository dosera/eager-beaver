/**
 * 
 */
package players.uct.evaluation.methods;

import core.exceptions.MoveDefinitionException;
import players.uct.UCTSubPlayer;
import statemachine.MachineState;

/**
 * @author tasse
 *
 * Abstract class for heuristic functions.
 */
public abstract class AHeuristic {
	/**
	 * Rates a machine state according to some heuristic function.
	 * @param ms : current machine state to rate
	 * @param sp : current UCTSubPlayer
	 * @return score for each role participating
	 * @throws MoveDefinitionException
	 */
	public abstract float[] rateMS(MachineState ms, UCTSubPlayer sp) throws MoveDefinitionException;

}
