package players.uct.evaluation.methods;

import core.exceptions.MoveDefinitionException;
import players.uct.UCTSubPlayer;
import statemachine.MachineState;

/**
 * @author tasse
 * Heuristic that favors machine states that leave the most open 
 * move possibilities for ourselves
 */
public class MobilityHeuristic extends AHeuristic {

	public int maxNumMoves = 1;
	
	public float[] rateMS(MachineState ms, UCTSubPlayer sp) throws MoveDefinitionException {
		float[] result = new float[sp.getRoleIndexMap().size()];
		for(int i = 0; i < result.length; i++)
		{		
			int numMoves = sp.getSM().getLegalMoves(ms, sp.getRoleIndexMap().getKey(i)).size();
			if(maxNumMoves < numMoves)
				maxNumMoves = numMoves;
			result[i] = (float)((float)numMoves / (float)maxNumMoves);
		}
		return result;
	}
}
