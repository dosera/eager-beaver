/**
 * 
 */
package players.uct.evaluation;

import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;
import core.Settings;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;

/**
 * @author tasse
 * 
 *         Implements the simulation part of the UCT algorithm.
 *         
 *         do{After,Before}EveryRecursionStep:
 *              this method is executed after/before every single step in the simulation
 *         do{After,Before}Simulation is:
 *              this method is executed after/before one whole simulation way
 *                             
 */
public abstract class AEvaluation {

	protected UCTSubPlayer sp;
	protected AStrategyHandler strategy;

	public AEvaluation(UCTSubPlayer sp, AStrategyHandler strategy) {
		this.sp = sp;
		this.strategy = strategy;
	}

	/**
	 * This method starts a simulation for each node of the expanded children
	 * list.
	 * 
	 * @param expandedChildren
	 * @param maxDepth
	 * @throws GoalDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws MoveDefinitionException
	 */
	public void eval(List<Node> expandedChildren, int maxDepth)
			throws GoalDefinitionException, TransitionDefinitionException,
			MoveDefinitionException {
		for (Node n : expandedChildren) {

			doBeforeSimulation(n.getMS());

			float[] scores = new float[n.getMove().length];
			// is the current state a terminal state? if so
			// -> we cannot simulate, return score directly
			if (sp.getSM().isTerminal(n.getMS())) {
				scores = getScores(n.getMS());
			}
			// start the simulation if the child is not terminal
			else {
				int[] depth = new int[] { 0 };
				scores = this.performRandomDepthChargeFromMove(n.getMS(),
						depth, maxDepth, strategy, sp, n);
			}
			// this only happens if the simulation could not have been finished
			if (scores == null)
				return;
			// update the scores of child n
			n.updateValues(scores);

			doAfterSimulation(n.getMS(), scores);

		}
	}

	/**
	 * These methods are executed before/after a simulation is started. This is
	 * useful for FAST and RAVE in order to reset records that track the
	 * MachineStates/Moves encountered along the random walk.
	 */
	protected void doBeforeSimulation(MachineState firstState) {
		return;
	}

	protected void doAfterSimulation(MachineState firstState, float[] scores) {
		return;
	}

	/**
	 * Responsible for the simulation. Recursively plays out moves until a goal
	 * or the maxDepth is reached. 
	 * - if the child is terminal, this method returns the corresponding goal values 
	 * - if a maxDepth is set, this method returns the corresponding goal values 
	 *                         based on a heuristic function defined with AHeuristic
	 * 
	 * @param ms
	 *            : the current machine state
	 * @param depth
	 *            : current depth
	 * @param maxDepth
	 *            : max depth to simulate to (set to 0 to go until the end)
	 * @param strategy
	 *            : current strategy
	 * @param sp
	 *            : current UCTSubPlayer
	 * @param expandedChild
	 *            : current child
	 * @return a score for each role participating in the game
	 * @throws GoalDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws MoveDefinitionException
	 */
	protected float[] performRandomDepthChargeFromMove(MachineState ms,
			int[] depth, int maxDepth, AStrategyHandler strategy,
			UCTSubPlayer sp, Node expandedChild)
			throws GoalDefinitionException, TransitionDefinitionException,
			MoveDefinitionException {
		if (sp.getSM().isTerminal(ms)) {
			return getScores(ms);
		} else if (depth[0] > maxDepth && maxDepth != 0) {
			return strategy.getHeuristic().rateMS(ms, sp);
		} else {
			depth[0] += 1;
			List<Move> nextMoves = nextMoves(ms);
			// if we cannot complete the simulation in the given time, we return
			// null
			if (sp.dateTillFinish < System.currentTimeMillis()) {
				return null;
			}
			MachineState nextState = nextMS(ms, nextMoves);
			float[] scores = performRandomDepthChargeFromMove(nextState, depth,
					maxDepth, strategy, sp, expandedChild);
			// we perform this check to ensure that the scores array is not null
			if (scores == null)
				return scores;
			doAfterEveryRecursionStep(nextMoves, scores, expandedChild,
					nextState);
			return scores;
		}
	}

	protected void doAfterEveryRecursionStep(List<Move> nextMoves,
			float[] scores, Node expandedChild, MachineState nextState) {
	}

	/**
	 * @param ms
	 * @return an array with scores for each role
	 * @throws GoalDefinitionException
	 */
	protected float[] getScores(MachineState ms) throws GoalDefinitionException {
		float[] result = new float[sp.getRoleIndexMap().size()];
		// TODO: 0.01f (100)
		for (int i = 0; i < result.length; i++)
			result[i] = Settings.SCORE_NORMALIZER
					* (float) sp.getSM().getGoal(ms,
							sp.getRoleIndexMap().getKey(i));
		return result;
	}

	protected List<Move> nextMoves(MachineState ms)
			throws MoveDefinitionException {
		return this.sp.getSM().getRandomJointMove(ms);
	}

	protected MachineState nextMS(MachineState ms, List<Move> nextMoves)
			throws TransitionDefinitionException, MoveDefinitionException {
		return this.sp.getSM().getNextStateDestructively(ms, nextMoves);
	}
}
