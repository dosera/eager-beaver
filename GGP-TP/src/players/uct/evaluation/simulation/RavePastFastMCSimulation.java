package players.uct.evaluation.simulation;

import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;

public class RavePastFastMCSimulation extends FastPastMCSimulation {

	public RavePastFastMCSimulation(UCTSubPlayer sp, AStrategyHandler strategy) {
		super(sp, strategy);
	}

	@Override
	protected void doAfterEveryRecursionStep(List<Move> nextMoves,
			float[] scores, Node expandedChild, MachineState nextState) {
		// call the parent method
		super.doAfterEveryRecursionStep(nextMoves, scores, expandedChild,
				nextState);
		// add our move to the list of the child n keep track of these moves
		// if we have noop we don't add it
		if (!nextMoves.get(sp.getMyRoleIndex()).toString()
				.equalsIgnoreCase("NOOP")) {
			expandedChild.followingMovesRave.add(nextMoves.get(sp
					.getMyRoleIndex()));
		}
	}

}
