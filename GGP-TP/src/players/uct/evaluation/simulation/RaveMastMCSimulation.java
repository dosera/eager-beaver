package players.uct.evaluation.simulation;

import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;

public class RaveMastMCSimulation extends MastMCSimulation {

	RaveMCSimulation ravesimulation;

	public RaveMastMCSimulation(UCTSubPlayer sp, AStrategyHandler strategy) {
		super(sp, strategy);
		this.ravesimulation = new RaveMCSimulation(sp, strategy);
	}

	@Override
	protected void doAfterEveryRecursionStep(List<Move> moves, float[] scores,
			Node expandedChild, MachineState nextState) {
		super.doAfterEveryRecursionStep(moves, scores, expandedChild, null);
		this.ravesimulation.doAfterEveryRecursionStep(moves, scores,
				expandedChild, null);
	}
}
