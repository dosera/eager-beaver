package players.uct.evaluation.simulation;

import java.util.List;
import java.util.Random;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.FastHandler;
import players.uct.strategyhandler.methods.RaveFastStrategy;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;

public class RaveFastMCSimulation extends FastMCSimulation {

	FastHandler strategy;
	Random rand;

	public RaveFastMCSimulation(UCTSubPlayer sp, FastHandler strategy) {
		super(sp, strategy);
		this.strategy = (RaveFastStrategy) strategy;
		rand = new Random();
	}

	@Override
	protected void doAfterEveryRecursionStep(List<Move> nextMoves,
			float[] scores, Node expandedChild, MachineState nextState) {
		super.doAfterEveryRecursionStep(nextMoves, scores, expandedChild,
				nextState);
		// add our move to the list in the ravehashmap of n to keep track of
		// these moves
		// if we have noop we dont add it
		if (!nextMoves.get(sp.getMyRoleIndex()).toString()
				.equalsIgnoreCase("NOOP")) {
			expandedChild.followingMovesRave.add(nextMoves.get(sp
					.getMyRoleIndex()));
		}
	}
}
