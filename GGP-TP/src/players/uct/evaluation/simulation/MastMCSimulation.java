package players.uct.evaluation.simulation;

import java.util.List;
import java.util.Random;

import players.uct.UCTSubPlayer;
import players.uct.evaluation.AEvaluation;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.MastHandler;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;
import core.Settings;
import core.exceptions.MoveDefinitionException;

public class MastMCSimulation extends AEvaluation {

	Random rand;
	MastHandler strategy;

	public MastMCSimulation(UCTSubPlayer sp, AStrategyHandler strategy) {
		super(sp, strategy);
		this.strategy = (MastHandler) strategy;
		rand = new Random();
	}

	@Override
	protected List<Move> nextMoves(MachineState ms)
			throws MoveDefinitionException {
		// currently we only bias the move selection for OUR player
		// the opponent(s) play randomly
		// we could extend our mastMap to score values for each player (float ->
		// floatarray)
		// and bias via gibbsBoltzmann for every player
		// TODO
		return super.sp.getSM().getRandomJointMove(ms,
				super.sp.getRoleIndexMap().getKey(super.sp.getMyRoleIndex()),
				gibbsBoltzmannMove(ms, super.sp.getMyRole()));
	}

	@Override
	protected void doAfterEveryRecursionStep(List<Move> moves, float[] scores,
			Node expandedChild, MachineState nextState) {
		if (!Settings.treeOnlyMast)
			checkNoop(moves, scores, this.strategy, super.sp);
	}

	/**
	 * if we perform a MASTcharge we have to ignore the NOOP case
	 */
	protected void checkNoop(List<Move> m, float[] scores,
			MastHandler strategy, UCTSubPlayer sp) {
		if (!m.get(sp.getMyRoleIndex()).toString().equalsIgnoreCase("NOOP")) // TODO
																				// TO
																				// STRING
		{
			if (strategy.getMastMap().containsKey(m.get(sp.getMyRoleIndex()))) {
				strategy.getMastMap().get(m.get(sp.getMyRoleIndex()))[0] += scores[sp
						.getMyRoleIndex()];
				strategy.getMastMap().get(m.get(sp.getMyRoleIndex()))[1] += 1;
			} else {
				float[] ourScores = new float[] { scores[sp.getMyRoleIndex()],
						1 };
				strategy.getMastMap()
						.put(m.get(sp.getMyRoleIndex()), ourScores);
			}
		}
	}

	/**
	 * Select a move for the simulation according to the gibbs-boltzmann
	 * probability distribution.
	 * 
	 * @throws MoveDefinitionException
	 */
	private Move gibbsBoltzmannMove(MachineState ms, Role role)
			throws MoveDefinitionException {
		List<Move> legalMoves = sp.getSM().getLegalMoves(ms, role);
		// no need to continue if only one move available
		if (legalMoves.size() == 1)
			return legalMoves.get(0);

		float[] QArray = new float[legalMoves.size()];
		float total = 0f;
		for (int i = 0; i < QArray.length; i++) {
			QArray[i] = (float) Math.pow(Math.E,
					getMoveScore(legalMoves.get(i), role, ms) / Settings.tau);
			total += QArray[i];
		}
		return legalMoves.get(selectMoveAccordingToProbability(QArray, total));
	}

	private float getMoveScore(Move move, Role role, MachineState ms) {
		// TODO: default value for Qha 1? or 0? CHECK THIS
		float Qha = 1f;
		if (this.strategy.getMastMap().containsKey(move)) {
			// compute the average score (wins / times found) for a move
			Qha = strategy.getMastMap().get(move)[0]
					/ strategy.getMastMap().get(move)[1];
		}
		return Qha;
	}

	private int selectMoveAccordingToProbability(float[] qArray, float total) {
		float ranNum = rand.nextFloat();
		float sum = 0f;
		for (int i = 0; i < qArray.length; i++) {
			sum += qArray[i] / total;
			if (ranNum < sum)
				return i;
		}
		return qArray.length - 1;
	}

}
