package players.uct.evaluation.simulation;

import helper.SetDifference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;

import players.uct.UCTSubPlayer;
import players.uct.evaluation.AEvaluation;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.FastHandler;
import players.uct.strategyhandler.methods.FastStrategy;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;
import statemachine.prover.ProverStateMachine;
import core.Settings;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;
import core.gdl.GdlSentence;
import core.gdl.GdlTerm;
import core.logging.TheLogger;

public class FastMCSimulation extends AEvaluation {

	FastHandler strategy;
	Random rand;

	boolean prover = false;

	public FastMCSimulation(UCTSubPlayer sp, AStrategyHandler strategy) {
		super(sp, strategy);
		this.strategy = (FastHandler) strategy;
		rand = new Random();
		// do we use the prover or the propnet?
		if (sp.getSM() instanceof ProverStateMachine)
			prover = true;
	}

	@Override
	protected void doBeforeSimulation(MachineState firstState) {
		// reset the episodes before every simulation
		strategy.episode = new ArrayList<MachineState>();
	}

	/**
	 * See "Simulation-Based General Game Playing" by Hilmar Finnsson
	 * (http://cadia.ru.is/wiki/public:cadiaplayer:main)
	 */
	@Override
	protected void doAfterSimulation(MachineState firstState, float[] scores) {
		if (strategy.episode.size() <= 0)
			return;
		// needed to update all weights at once after we computed them
		ArrayList<Float[]> new_weights = new ArrayList<Float[]>();
		for (int i = 0; i < strategy.getFeatureWeights().size(); i++) {
			new_weights.add(Arrays.copyOf(strategy.getFeatureWeights().get(i),
					strategy.getFeatureWeights().get(i).length));
		}
		for (int roleIndex = 0; roleIndex < super.sp.getRoleIndexMap().size(); roleIndex++) {
			// process the episode
			float v_s = 0.0f;
			float v_s_prime = -1;

			MachineState s, s_prime;

			int terminalIndex = strategy.episode.size() - 1;
			int rPos = strategy.episode.size() - 2;

			float r = 0f;
			for (int i = 0; i < scores.length; i++) {
				if (i == roleIndex)
					r += scores[i];
				else
					r -= scores[i];
			}

			float[] e = new float[terminalIndex];
			Arrays.fill(e, 0f);

			// loop through all states visited during simulation
			for (int i = 0; i < terminalIndex; i++) {
				s = strategy.episode.get(i);
				s_prime = strategy.episode.get(i + 1);

				int[] numFeatures_vs = new int[] { 0 };

				v_s = getValues(getFeaturesPresent(s, numFeatures_vs),
						roleIndex);
				int maxFeatures = numFeatures_vs[0];

				if (i < rPos) {
					int[] numFeatures_vsprime = new int[] { 0 };
					v_s_prime = getValues(
							getFeaturesPresent(s_prime, numFeatures_vsprime),
							roleIndex);
					maxFeatures = Math.max(numFeatures_vs[0],
							numFeatures_vsprime[0]);
				} else
					v_s_prime = r;
				v_s_prime *= FastStrategy.GAMMA;

				float delta = v_s_prime - v_s;

				// TODO: is it possible that maxFeatures is 0?
				delta /= maxFeatures;

				e[i] = 1;
				for (int j = 0; j <= i; j++) {
					// TODO: save features of every state in every episode
					updateWeights(
							delta,
							new_weights,
							getFeaturesPresent(strategy.episode.get(j),
									new int[] { 0 }), e[j], roleIndex);

					e[j] = FastStrategy.GAMMA * FastStrategy.LAMBDA * e[j];

					if (e[j] < FastStrategy.ZERO_MARGIN
							&& e[j] > -FastStrategy.ZERO_MARGIN)
						e[j] = 0;
				}
			}
		}
		strategy.setFeatureWeights(new_weights);
	}

	private void updateWeights(float delta, ArrayList<Float[]> new_weights,
			ArrayList<Integer> presentFeatures, float trace, int roleIndex) {
		if (trace == 0)
			return;
		for (int i = 0; i < presentFeatures.size(); i++) {
			// the i-th feature is not present -> nothing happens
			if (presentFeatures.get(i) == 0)
				continue;
			new_weights.get(i)[roleIndex] = strategy.getFeatureWeights().get(i)[roleIndex]
					+ FastStrategy.ALPHA
					* delta
					* trace
					* presentFeatures.get(i);
		}
	}

	private float getValues(ArrayList<Integer> featuresPresent, int roleIndex) {
		float result = 0f;
		for (int i = 0; i < featuresPresent.size(); i++) {
			result += strategy.getFeatureWeights().get(i)[roleIndex]
					* featuresPresent.get(i);
		}
		return result;
	}

	/**
	 * Returns an ArrayList where the entry at index i specifies whether the
	 * i-th feature is part of the given MachineState or not
	 */
	private ArrayList<Integer> getFeaturesPresent(MachineState state,
			int[] counter) {

		// cell based games (tictactoe)
		if (!this.strategy.piecebased) {
			ArrayList<Integer> result = new ArrayList<Integer>();
			// check if the feature is contained in the MachineState
			for (int i = 0; i < strategy.getFeatureList().size(); i++) {
				boolean contains = false;
				for (GdlSentence g : state.contents) {
					if (this.strategy.getFeatureList().get(i).contentEquals(g)) {
						result.add(1);
						contains = true;
						counter[0]++;
						break;
					}
				}
				if (!contains)
					result.add(0);
			}
			return result;
		}
		// piece based games (chess)
		// in this case we count how often a feature is present in the current
		// state
		else {
			ArrayList<Integer> result = new ArrayList<Integer>();
			for (int i = 0; i < strategy.getFeatureList().size(); i++) {
				int numContains = 0;
				boolean contains = false;
				for (GdlSentence g : state.contents) {
					// try{
					if (prover) {
						if (this.strategy
								.getFeatureList()
								.get(i)
								.toString()
								.equalsIgnoreCase(
										g.getBody()
												.get(0)
												.toSentence()
												.getBody()
												.get(g.getBody().get(0)
														.toSentence().getBody()
														.size() - 1).toString())) {
							numContains++;
							contains = true;
						}
					} else {
						if (this.strategy
								.getFeatureList()
								.get(i)
								.toString()
								.equalsIgnoreCase(
										g.getBody().get(g.getBody().size() - 1)
												.toString())) {
							numContains++;
							contains = true;
						}
					}
					// } catch (RuntimeException e) { }
				}
				if (contains) {
					counter[0]++;
					result.add(numContains);
				}
				if (!contains)
					result.add(0);
			}
			return result;
		}
	}

	@Override
	protected void doAfterEveryRecursionStep(List<Move> nextMoves,
			float[] scores, Node expandedChild, MachineState nextState) {
		// TODO: do this more efficient...
		strategy.episode.add(nextState);
	}

	@Override
	protected List<Move> nextMoves(MachineState ms)
			throws MoveDefinitionException {
		ArrayList<Move> result = new ArrayList<Move>();
		for (int i = 0; i < super.sp.getSM().getRoles().size(); i++)
			try {
				result.add(gibbsBoltzmannMove(ms, super.sp.getSM().getRoles()
						.get(i)));
			} catch (TransitionDefinitionException e) {
				TheLogger.LOGGER.log(Level.SEVERE,
						"error while matching move to feature", e);
			}
		return result;
	}

	/**
	 * Select a move for the simulation according to the gibbs-boltzmann
	 * probability distribution.
	 * 
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 */
	private Move gibbsBoltzmannMove(MachineState ms, Role role)
			throws MoveDefinitionException, TransitionDefinitionException {
		List<Move> legalMoves = sp.getSM().getLegalMoves(ms, role);
		// no need to continue if only one move available
		if (legalMoves.size() == 1)
			return legalMoves.get(0);

		float[] QArray = new float[legalMoves.size()];
		float total = 0f;
		for (int i = 0; i < QArray.length; i++) {
			QArray[i] = (float) Math.pow(Math.E,
					getMoveScore(legalMoves.get(i), role, ms) / Settings.tau);
			total += QArray[i];
		}
		return legalMoves.get(selectMoveAccordingToProbability(QArray, total));
	}

	/**
	 * ! currently we can only detect features in non-simultaneous games !
	 */
	protected float getMoveScore(Move move, Role role, MachineState ms)
			throws TransitionDefinitionException, MoveDefinitionException {
		if (!this.strategy.piecebased) // e.g.tictactoe
			return getCellBasedMoveScore(move, role, ms) * FastHandler.C;
		else
			return getPieceBasedMoveScore(move, role, ms); // e.g. checkers
	}

	protected float getCellBasedMoveScore(Move move, Role role, MachineState ms)
			throws TransitionDefinitionException, MoveDefinitionException {
		MachineState nextState = super.sp.getSM().getNextStateDestructively(ms,
				super.sp.getSM().getRandomJointMove(ms, role, move));

		// set with the true base propositions (gdlsentence)
		// that became true after MS is nextState
		Set<GdlSentence> trueBasePropsOfNextState = new HashSet<GdlSentence>(
				nextState.getContents());
		trueBasePropsOfNextState.removeAll(ms.getContents());

		// loop through the feature list and get the scores
		// of the features that are active
		float score = 0f;
		for (int i = 0; i < this.strategy.getFeatureList().size(); i++) {
			if (trueBasePropsOfNextState.contains(this.strategy
					.getFeatureList().get(i).getContent().toSentence()))
				score += this.strategy.getFeatureWeights().get(i)[super.sp
						.getRoleIndexMap().get(role)];
		}
		return score;
	}

	protected float getPieceBasedMoveScore(Move move, Role role, MachineState ms)
			throws TransitionDefinitionException, MoveDefinitionException {
		// e.g. (move h1 h8) -> we have to find the pieces on that fields

		Set<GdlSentence> currentStateContents = new HashSet<GdlSentence>(
				ms.getContents());
		SetDifference.filterTiles(currentStateContents); // filter unnecessary
															// propositions

		// games like 8 puzzle: move 4 5 : meaning moving tile 4 to 5
		String fromPosition = "";
		String toPosition = "";
		if (move.getContents().getBody().size() == 2) {
			fromPosition = move.getContents().get(0).toString() + " "
					+ move.getContents().get(1).toString();
			toPosition = ""; // ??? TODO
		}
		// games like chess: move h 1 h 8 : meaning moving the figure from h1 to
		// h8
		else if (move.getContents().getBody().size() == 4) {
			fromPosition = move.getContents().get(0).toString() + " "
					+ move.getContents().get(1).toString();
			toPosition = move.getContents().get(2).toString() + " "
					+ move.getContents().get(3).toString();
		}
		// games like checkers: move bp h 1 h 8 : meaning moving the figure bp
		// from h1 to h8
		else if (move.getContents().getBody().size() == 5) {
			fromPosition = move.getContents().get(1).toString() + " "
					+ move.getContents().get(2).toString();
			toPosition = move.getContents().get(3).toString() + " "
					+ move.getContents().get(4).toString();
		}
		// and doublejumps: doublejump bp h1 h8 f6 : meaning moving the figure
		// bp from h1 to h8 and to f6
		else if (move.getContents().getBody().size() > 5) {
			// we only need the "from Position" in this case..
			fromPosition = move.getContents().get(1).toString() + " "
					+ move.getContents().get(2).toString();
		}

		// get the pieces according to the move
		GdlTerm fromPiece = null;
		GdlTerm toPiece = null;
		// set this field for cases like checkers, where toPiece does NOT equal
		// capturedPiece
		ArrayList<GdlTerm> capturedPieces = new ArrayList<GdlTerm>();

		for (GdlSentence g : currentStateContents) {
			if (prover) {
				if (g.getBody().toString().toLowerCase().contains(fromPosition)) {
					fromPiece = g
							.getBody()
							.get(0)
							.toSentence()
							.getBody()
							.get(g.getBody().get(0).toSentence().getBody()
									.size() - 1);
				}
				if (g.getBody().toString().toLowerCase().contains(toPosition)) {
					toPiece = g
							.getBody()
							.get(0)
							.toSentence()
							.getBody()
							.get(g.getBody().get(0).toSentence().getBody()
									.size() - 1);
				}
			} else {
				if (g.toString().toLowerCase().contains(fromPosition)) {
					fromPiece = g.getBody().get(g.getBody().size() - 1);
				}
				if (g.toString().toLowerCase().contains(toPosition)) {
					toPiece = g.getBody().get(g.getBody().size() - 1);
				}
			}
		}

		// special case checkers
		if (Settings.matchID.contains("checker"))
			specialCaseCheckers(move, role, ms, currentStateContents,
					fromPiece, capturedPieces);

		// "otherwise" case (no capture move)
		// toPiece = null check is only there because the PROVER does not
		// explicitly
		// represent fields where NO PIECE = b is
		if (toPiece == null
				|| (toPiece.toSentence().toString().equalsIgnoreCase("b") && capturedPieces
						.size() == 0))
			return -1;

		// "capture" case
		// we add the captured piece in the list if the list isn't filled yet
		if (capturedPieces.size() == 0)
			capturedPieces.add(toPiece);
		// get the value from the featurelist!! PAMPAM
		float scoreFromPiece = 0f;
		float scoreToPiece = 0f;
		for (int i = 0; i < this.strategy.getFeatureList().size(); i++) {
			if (this.strategy.getFeatureList().get(i)
					.contentEquals(fromPiece.toSentence()))
				scoreFromPiece = this.strategy.getFeatureWeights().get(i)[super.sp
						.getRoleIndexMap().get(role)];
			for (int j = 0; j < capturedPieces.size(); j++) {
				if (this.strategy.getFeatureList().get(i)
						.contentEquals(capturedPieces.get(j).toSentence()))
					scoreToPiece += this.strategy.getFeatureWeights().get(i)[super.sp
							.getRoleIndexMap().get(role)];
			}
		}
		return -(2 * scoreToPiece + scoreFromPiece); // Q_h(a) score
	}

	/**
	 * This method is for the special case "checkers" where the captured figure
	 * is not on the toField but somewhere in between.
	 * 
	 * @throws TransitionDefinitionException
	 * @throws MoveDefinitionException
	 */
	private void specialCaseCheckers(Move move, Role role, MachineState ms,
			Set<GdlSentence> currentStateContents, GdlTerm fromPiece,
			ArrayList<GdlTerm> capturedPieces)
			throws TransitionDefinitionException, MoveDefinitionException {
		Set<GdlSentence> nextStateContents = new HashSet<GdlSentence>(super.sp
				.getSM()
				.getNextStateDestructively(ms,
						super.sp.getSM().getRandomJointMove(ms, role, move))
				.getContents());
		SetDifference.filterTiles(nextStateContents);
		Set<GdlSentence>[] diffContents = SetDifference.difference(
				currentStateContents, nextStateContents);

		for (GdlSentence gdl : diffContents[0]) {
			// in order to find out what pieces have been captured
			// we ignore the ones that were blank before
			// and the piece from the start cell
			if (!gdl.getBody().get(gdl.getBody().size() - 1).toString()
					.equalsIgnoreCase("b")
					&& !gdl.toString().toLowerCase()
							.endsWith(fromPiece.toString() + " )")) {
				// only consider pieces that are blank afterwards,
				// DON'T if we reach the end of the board in checkers and
				// change a pawn to a queen
				for (GdlSentence gdlNext : diffContents[1]) {
					if (gdl.getBody().containsAll(
							gdlNext.getBody().subList(0,
									gdlNext.getBody().size() - 1))) {
						// is the matched cell blank afterwards? (we didnt
						// receive a queen)
						if (gdlNext.getBody().get(gdlNext.getBody().size() - 1)
								.toString().equalsIgnoreCase("b")) {
							capturedPieces.add(gdl.getBody().get(
									gdl.getBody().size() - 1));
							continue;
						}
					}
				}

			}
		}
	}

	private int selectMoveAccordingToProbability(float[] qArray, float total) {
		float ranNum = rand.nextFloat();
		float sum = 0f;
		for (int i = 0; i < qArray.length; i++) {
			sum += qArray[i] / total;
			if (ranNum < sum)
				return i;
		}
		return qArray.length - 1;
	}

}
