package players.uct.evaluation.simulation;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import players.uct.UCTSubPlayer;
import players.uct.evaluation.AEvaluation;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.PastHandler;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;
import core.Settings;
import core.exceptions.MoveDefinitionException;
import core.gdl.GdlSentence;

public class PastMCSimulation extends AEvaluation {

	Random rand;
	MachineState msBeforeMove;

	public PastMCSimulation(UCTSubPlayer sp, AStrategyHandler strategy) {
		super(sp, strategy);
		this.rand = new Random();
		this.msBeforeMove = null;
	}

	@Override
	protected void doAfterEveryRecursionStep(List<Move> moves, float[] scores,
			Node expandedChild, MachineState nextState) {
		checkNoop(moves, scores, super.strategy, super.sp);
	}

	/**
	 * if we perform a PASTcharge we have to ignore the NOOP case
	 * 
	 */
	protected void checkNoop(List<Move> m, float[] scores,
			AStrategyHandler strategy, UCTSubPlayer sp) {
		if (!m.get(sp.getMyRoleIndex()).toString().equalsIgnoreCase("NOOP")) {
			PastHandler strat = (PastHandler) strategy;
			Move myMove = m.get(sp.getMyRoleIndex());
			// is the move already in the HashMap?
			// if not, we create a new hashmap for this move
			// in the actionPredicateValues map
			if (!strat.actionPredicateValues.containsKey(myMove))
				strat.actionPredicateValues.put(myMove,
						new HashMap<GdlSentence, float[]>());

			// we get the predicates of our fathers state
			for (GdlSentence predicate : this.msBeforeMove.getContents()) {
				// the predicate does not exist for this move yet
				// -> we add it
				if (!strat.actionPredicateValues.get(myMove).containsKey(
						predicate)) {
					strat.actionPredicateValues.get(myMove).put(predicate,
							new float[] { scores[sp.getMyRoleIndex()], 1 });
				}
				// we found move and predicate and update the values
				else {
					strat.actionPredicateValues.get(myMove).get(predicate)[0] += scores[sp
							.getMyRoleIndex()];
					strat.actionPredicateValues.get(myMove).get(predicate)[1] += 1;
				}
			}
		}
	}

	@Override
	protected List<Move> nextMoves(MachineState ms)
			throws MoveDefinitionException {
		msBeforeMove = ms;
		return super.sp.getSM().getRandomJointMove(ms,
				super.sp.getRoleIndexMap().getKey(super.sp.getMyRoleIndex()),
				gibbsBoltzmannMove(ms, super.strategy, super.sp));
	}

	/**
	 * Select a move for the simulation according to the
	 * gibbs-boltzmann probability distribution.
	 * @throws MoveDefinitionException
	 */
	private Move gibbsBoltzmannMove(MachineState ms, AStrategyHandler strategy,
			UCTSubPlayer sp) throws MoveDefinitionException {
		PastHandler strat = (PastHandler) strategy;
		List<Move> legalMoves = sp.getSM().getLegalMoves(ms,
				sp.getRoleIndexMap().getKey(sp.getMyRoleIndex()));
		// we only have one possible move
		// -> dont do anything else but return this move
		if (legalMoves.size() == 1)
			return legalMoves.get(0);
		float[] QArray = new float[legalMoves.size()];
		float total = 0f;
		float tau = Settings.tau;

		for (int i = 0; i < legalMoves.size(); i++) {
			float Qha = 1f;
			if (strat.actionPredicateValues.containsKey(legalMoves.get(i))) {
				float currentBest = Float.NEGATIVE_INFINITY;
				for (GdlSentence predicate : strat.actionPredicateValues.get(
						legalMoves.get(i)).keySet()) {
					if (!ms.getContents().contains(predicate)) // TODO
																// necessary?
						continue;
					if (strat.actionPredicateValues.get(legalMoves.get(i)).get(
							predicate)[0]
							/ strat.actionPredicateValues
									.get(legalMoves.get(i)).get(predicate)[1] > currentBest) {
						currentBest = strat.actionPredicateValues.get(
								legalMoves.get(i)).get(predicate)[0]
								/ strat.actionPredicateValues.get(
										legalMoves.get(i)).get(predicate)[1];
					}
				}
				Qha = currentBest;
			}
			QArray[i] = Qha;
			total += Math.pow(Math.E, Qha / tau);
		}
		float[] divArray = new float[QArray.length];
		for (int i = 0; i < QArray.length; i++) {
			divArray[i] = (float) Math.pow(Math.E, QArray[i] / tau) / total;
		}
		return legalMoves.get(selectMoveAccordingToProbability(divArray));
	}

	private int selectMoveAccordingToProbability(float[] divArray) {
		float ranNum = rand.nextFloat();
		float sum = 0f;
		for (int i = 0; i < divArray.length; i++) {
			if (sum <= ranNum && ranNum <= sum + divArray[i])
				return i;
			sum += divArray[i];
		}
		// due to a rounding error the last element of the divArray is taken
		return divArray.length - 1;
	}
}
