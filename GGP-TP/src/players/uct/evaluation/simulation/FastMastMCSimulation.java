package players.uct.evaluation.simulation;

import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.FastHandler;
import players.uct.strategyhandler.methods.FastMastStrategy;
import players.uct.strategyhandler.methods.FastStrategy;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;
import core.Settings;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;

public class FastMastMCSimulation extends FastMCSimulation{

	FastMastStrategy strategy;
	public FastMastMCSimulation(UCTSubPlayer sp, AStrategyHandler strategy) {
		super(sp, strategy);
		this.strategy = (FastMastStrategy)strategy;
	}

	@Override
	protected float getMoveScore(Move move, Role role, MachineState ms) throws TransitionDefinitionException, MoveDefinitionException
	{ 
		// MAST result
		float mastResult = 1f;
		if (this.strategy.getMastMap().containsKey(move)) {
			// compute the average score (wins / times found) for a move
			mastResult = strategy.getMastMap().get(move)[0]
					/ strategy.getMastMap().get(move)[1];
		}
		float fastResult;
		if(!this.strategy.piecebased)
			fastResult = super.getCellBasedMoveScore(move,role,ms) * FastStrategy.C; // e.g. tic tac toe
		else
			fastResult = super.getPieceBasedMoveScore(move, role, ms); // e.g. checkers
		// combination formula by hilmar finnsson
		if(fastResult == -1f)
			return mastResult;
		return mastResult + FastHandler.OMEGA * fastResult;
	}
	@Override
	protected void doAfterEveryRecursionStep(List<Move> moves, float[] scores,
			Node expandedChild, MachineState nextState) {
		super.doAfterEveryRecursionStep(moves, scores, expandedChild, nextState);
		if (!Settings.treeOnlyMast)
			checkNoop(moves, scores, this.strategy, super.sp);
	}

	/**
	 * if we perform a MASTcharge we have to ignore the NOOP case
	 */
	protected void checkNoop(List<Move> m, float[] scores,
			FastMastStrategy strategy, UCTSubPlayer sp) {
		if (!m.get(sp.getMyRoleIndex()).toString().equalsIgnoreCase("NOOP"))
		{
			if (strategy.getMastMap().containsKey(m.get(sp.getMyRoleIndex()))) {
				strategy.getMastMap().get(m.get(sp.getMyRoleIndex()))[0] += scores[sp
						.getMyRoleIndex()];
				strategy.getMastMap().get(m.get(sp.getMyRoleIndex()))[1] += 1;
			} else {
				float[] ourScores = new float[] { scores[sp.getMyRoleIndex()],
						1 };
				strategy.getMastMap()
						.put(m.get(sp.getMyRoleIndex()), ourScores);
			}
		}
	}
}
