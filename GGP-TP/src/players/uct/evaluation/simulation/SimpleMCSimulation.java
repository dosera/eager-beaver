/**
 * 
 */
package players.uct.evaluation.simulation;

import players.uct.UCTSubPlayer;
import players.uct.evaluation.AEvaluation;
import players.uct.strategyhandler.AStrategyHandler;

/**
 * @author tasse <-- Simulation Phase --> Class for the UCT Simulation phase
 */
public class SimpleMCSimulation extends AEvaluation {

	public SimpleMCSimulation(UCTSubPlayer sp, AStrategyHandler strategy) {
		super(sp, strategy);
	}
}
