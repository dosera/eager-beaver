package players.uct.evaluation.simulation;

import java.util.HashMap;
import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.FastHandler;
import players.uct.strategyhandler.methods.FastPastStrategy;
import players.uct.strategyhandler.methods.FastStrategy;
import players.uct.tree.Node;
import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;
import core.gdl.GdlSentence;

public class FastPastMCSimulation extends FastMCSimulation {

	FastPastStrategy strategy;
	MachineState msBeforeMove;

	public FastPastMCSimulation(UCTSubPlayer sp, AStrategyHandler strategy) {
		super(sp, strategy);
		this.strategy = (FastPastStrategy) strategy;
		this.msBeforeMove = null;
	}

	@Override
	protected float getMoveScore(Move move, Role role, MachineState ms)
			throws TransitionDefinitionException, MoveDefinitionException {
		// PAST result
		float pastResult = 1f;
		if (this.strategy.actionPredicateValues.containsKey(move)) {
			float currentBest = Float.NEGATIVE_INFINITY;
			for (GdlSentence predicate : this.strategy.actionPredicateValues
					.get(move).keySet()) {
				if (!ms.getContents().contains(predicate))
					continue;
				if (this.strategy.actionPredicateValues.get(move)
						.get(predicate)[0]
						/ this.strategy.actionPredicateValues.get(move).get(
								predicate)[1] > currentBest) {
					currentBest = this.strategy.actionPredicateValues.get(move)
							.get(predicate)[0]
							/ this.strategy.actionPredicateValues.get(move)
									.get(predicate)[1];
				}
			}
			pastResult = currentBest;
		}

		float fastResult;
		if (!this.strategy.piecebased)
			fastResult = super.getCellBasedMoveScore(move, role, ms)
					* FastStrategy.C; // e.g. tic tac toe
		else
			fastResult = super.getPieceBasedMoveScore(move, role, ms); // e.g.
																		// checkers
		// combination formula by hilmar finnsson
		if (fastResult == -1f)
			return pastResult;
		return pastResult + FastHandler.OMEGA * fastResult;
	}

	@Override
	protected void doAfterEveryRecursionStep(List<Move> moves, float[] scores,
			Node expandedChild, MachineState nextState) {
		super.doAfterEveryRecursionStep(moves, scores, expandedChild, nextState);
		checkNoop(moves, scores, super.strategy, super.sp);
	}

	/**
	 * if we perform a PASTcharge we have to ignore the NOOP case
	 * 
	 * @param t
	 * @param m
	 * @param scores
	 * @param strategy
	 */
	protected void checkNoop(List<Move> m, float[] scores,
			AStrategyHandler strategy, UCTSubPlayer sp) {
		if (!m.get(sp.getMyRoleIndex()).toString().equalsIgnoreCase("NOOP")) {
			Move myMove = m.get(sp.getMyRoleIndex());
			// is the move already in the HashMap?
			// if not, we create a new hashmap for this move
			// in the actionPredicateValues map
			if (!this.strategy.actionPredicateValues.containsKey(myMove))
				this.strategy.actionPredicateValues.put(myMove,
						new HashMap<GdlSentence, float[]>());

			// we get the predicates of our fathers state
			for (GdlSentence predicate : this.msBeforeMove.getContents()) {
				// the predicate does not exist for this move yet
				// -> we add it
				if (!this.strategy.actionPredicateValues.get(myMove)
						.containsKey(predicate)) {
					this.strategy.actionPredicateValues.get(myMove).put(
							predicate,
							new float[] { scores[sp.getMyRoleIndex()], 1 });
				}
				// we found move and predicate and update the values
				else {
					this.strategy.actionPredicateValues.get(myMove).get(
							predicate)[0] += scores[sp.getMyRoleIndex()];
					this.strategy.actionPredicateValues.get(myMove).get(
							predicate)[1] += 1;
				}
			}
		}
	}

	@Override
	protected List<Move> nextMoves(MachineState ms)
			throws MoveDefinitionException {
		msBeforeMove = ms;
		return super.nextMoves(ms);
	}
}
