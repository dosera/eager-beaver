package players.uct;

import helper.StorePossibleGoalValues;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;

import players.uct.movecomputation.AMoveComputation;
import players.uct.movecomputation.AverageWinsVisitsMove;
import players.uct.movecomputation.MostVisitedMove;
import players.uct.scheduler.AScheduler;
import players.uct.scheduler.SequentialScheduler;
import players.uct.scheduler.UCTThreadScheduler;
import players.uct.tree.Node;
import propnet.PropNetStateMachine;
import propnet.SuperPropNetStateMachine;
import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;
import statemachine.StateMachine;
import statemachine.StateMachineGamer;
import core.Settings;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;
import core.gdl.GdlSentence;
import core.logging.TheLogger;

/**
 * @author tasse
 * 
 *         This class implements a gamer using the UCT algorithm. UCT is based
 *         on four different phases: - Selection - Expansion - Simulation -
 *         Backpropagation
 */

public class UCTGamer extends StateMachineGamer {
	// ThreadScheduler
	private AScheduler scheduler;
	List<UCTSubPlayer> spList;

	public AScheduler getScheduler() {
		return scheduler;
	}

	@Override
	public StateMachine getInitialStateMachine() {
		return Settings.getStateMachine();
	}

	@Override
	public void stateMachineMetaGame(long timeout)
			throws TransitionDefinitionException, MoveDefinitionException,
			GoalDefinitionException {

		spList = new ArrayList<UCTSubPlayer>();
		// instantiate subplayers
		addSubPlayers();
		for (UCTSubPlayer sp : spList) {
			StorePossibleGoalValues.store(sp);
			// computation has to finish by dateTillFinish
			sp.dateTillFinish = timeout;
		}
		// if threads are set, we use the threaded scheduler, otherwise:
		// sequential
		if (Settings.numThreads > 0)
			scheduler = new UCTThreadScheduler(spList);
		else
			scheduler = new SequentialScheduler(spList);
		TheLogger.LOGGER.finest("initialized: "
				+ scheduler.getClass().getName());

		long timeavailable = timeout - System.currentTimeMillis()
				+ this.getMatch().getStartClock() * 80;
		TheLogger.LOGGER.fine("startclock time left: "
				+ (timeavailable < 0 ? 0 : timeavailable)
				+ " ms, starting UCT-precomputation...");

		// start the UCT precomputation here
		scheduler.run(timeavailable);
	}

	@Override
	public Move stateMachineSelectMove(long timeoutDate)
			throws TransitionDefinitionException, MoveDefinitionException,
			GoalDefinitionException {

		// schedulertimeout is the absolute time
		// available for the scheduler (e.g. 15sec)
		long schedulerTimeAvailable = timeoutDate - System.currentTimeMillis();

		// update Tree (use the precomputed tree)
		if (this.getStateMachine() instanceof SuperPropNetStateMachine
				&& ((SuperPropNetStateMachine) this.getStateMachine())
						.isFactorisable()) {
			for (UCTSubPlayer subplayer : spList) {
				HashSet<GdlSentence> subcontents = ((SuperPropNetStateMachine) this
						.getStateMachine()).getSubState(this.getCurrentState(),
						(PropNetStateMachine) subplayer.getSM());
				MachineState substate = new MachineState(subcontents);
				subplayer.updateTree(substate);
			}
		} else {
			for (UCTSubPlayer subplayer : spList)
				subplayer.updateTree(this.getCurrentState());
		}
		// set the date to timeout for every subplayer,
		// computation has to finish by dateTillFinish
		for (int i = 0; i < spList.size(); i++)
			spList.get(i).dateTillFinish = timeoutDate;

		// runs the subplayer(s) as long as we have time
		long schedulerstart = System.currentTimeMillis();
		HashMap<UCTSubPlayer, List<Node>> resultMap = scheduler
				.run(schedulerTimeAvailable);
		long timeUsedByScheduler = System.currentTimeMillis() - schedulerstart;
		if (timeUsedByScheduler > schedulerTimeAvailable + 100) // 100 puffer
																// for the
																// scheduler
			TheLogger.LOGGER.log(Level.SEVERE, "Scheduler ran "
					+ (timeUsedByScheduler - schedulerTimeAvailable)
					+ " ms too long");
		else
			TheLogger.LOGGER.log(Level.FINE, "Scheduler ran "
					+ timeUsedByScheduler + " ms");

		// Visualization
		if (Settings.drawUCT)
			for (int i = 0; i < spList.size(); i++)
				UCTVisualization.drawTree(Settings.visUCTFilename + i, spList
						.get(i).getTree(), Settings.visUCTDepth);

		// extract the best node and return it as a move
		long moveselectionstart = System.currentTimeMillis();
		AMoveComputation movecomputation = (Settings.useAveragesForUCT ? new AverageWinsVisitsMove(
				this.getRole()) : new MostVisitedMove(this.getRole()));
		Move bestM;
		if (getStateMachine() instanceof SuperPropNetStateMachine)
			bestM = movecomputation.evaluateBestMoveFactoring(resultMap, true,
					spList);
		else
			bestM = movecomputation.evaluateBestMove(resultMap, true);
		if (System.currentTimeMillis() - moveselectionstart > 0 + 300)
			TheLogger.LOGGER.log(
					Level.SEVERE,
					String.format("extracting the best move took %dms",
							System.currentTimeMillis() - moveselectionstart));
		return bestM;
	}

	@Override
	public void stateMachineStop() {
		this.getStateMachine().computeTimePerState();
	}

	@Override
	public String getName() {
		return "UCT-Gamer";
	}

	/**
	 * add multiple subplayers if we have a SuperPropNetStatemachine, just add
	 * one subplayer for other statemachines
	 */
	private void addSubPlayers() {
		if (getStateMachine() instanceof SuperPropNetStateMachine) {
			SuperPropNetStateMachine sm = (SuperPropNetStateMachine) getStateMachine();
			for (PropNetStateMachine pnsm : sm.getSubStateMachines()) {

				MachineState currentState = sm.computeStateForMachine(
						this.getCurrentState(), pnsm);
				UCTSubPlayer sp = new UCTSubPlayer(pnsm, this.getRole(),
						currentState);
				sp.setStrategy(Settings.setUpStrategy(sp));
				spList.add(sp);
			}
		} else {
			// we create a single subplayer for precomputation
			UCTSubPlayer sp = new UCTSubPlayer(this.getStateMachine(),
					this.getRole(), this.getCurrentState());
			sp.setStrategy(Settings.setUpStrategy(sp));
			spList.add(sp);
		}
	}

	/**
	 * this methods are used for UNIT test cases only
	 */
	@SuppressWarnings("unused")
	private void changeRole(Role role) {
		super.setRole(role);
	}

}
