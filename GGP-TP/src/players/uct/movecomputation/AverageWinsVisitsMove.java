package players.uct.movecomputation;

import java.util.HashMap;
import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.tree.Node;
import statemachine.Move;
import statemachine.Role;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class AverageWinsVisitsMove extends AMoveComputation {

	public AverageWinsVisitsMove(Role ourRole)
	{
		super.ourRole = ourRole;
	}
	@Override
	public Move evaluateBestMove(HashMap<UCTSubPlayer, List<Node>> resultMap,
			boolean logging) {
		Move bestMove = null;
        // this move is used to ensure that we don't return null
        // even if we don't have any possiblity to win
        Move lastInspectedMove = null;
        float lastInspectedMoveAverage = -1;
        boolean isGoalMove = false;
        float goalMoveVisits = -1;
        for (UCTSubPlayer sp : resultMap.keySet()) {
            
        	// logging
            if(logging)
            	super.log(sp);
            
            float bestAverageScore = Float.NEGATIVE_INFINITY;
            Node rootNode = sp.getTree().getRootNode();
            
            for (int i = 0; i < rootNode.getChildren().size(); i++) {
            	// update the last inspected move
            	// such that we don't use the last move in general but the one 
            	// with the most visits (situation: every node is a IGNORENODE 
            	// -> use the move with the most visits
            	if(lastInspectedMoveAverage <= rootNode.getChildren().get(i).getAverageScore(sp.getMyRoleIndex()))
            	{
            		lastInspectedMove = rootNode.getChildren().get(i).getMove()[sp.getMyRoleIndex()];
            		lastInspectedMoveAverage = rootNode.getChildren().get(i).getAverageScore(sp.getMyRoleIndex());
            	}
                // pruned nodes shall be ignored
                if (rootNode.getChildren().get(i).isPrunable())
                    continue;
                // do we have more than one legal move?
                if (rootNode.getPossibleMovesOfRoleIndex()[sp.getMyRoleIndex()] > 1) {
                    // win situation
                    if (rootNode.getChildren().get(i).LeadsToWin()) {
                    	isGoalMove = true;
                    	if(rootNode.getChildren().get(i).getAverageScore(sp.getMyRoleIndex()) > goalMoveVisits)
                    	{
                    		bestMove = rootNode.getChildren().get(i).getMove()[sp.getMyRoleIndex()];
                    		goalMoveVisits = rootNode.getChildren().get(i).getAverageScore(sp.getMyRoleIndex());
                    	}
                    }
                    // defeat situation
                    if (rootNode.getChildren().get(i).LeadsToDefeat()) {
                        continue;
                    }
                }
                
                if (rootNode.getChildren().get(i).getAverageScore(sp.getMyRoleIndex()) > bestAverageScore 
                	&& !isGoalMove)
                {
                    bestMove = rootNode.getChildren().get(i).getMove()[sp
                            .getMyRoleIndex()];
                    bestAverageScore = rootNode.getChildren().get(i).getAverageScore(sp.getMyRoleIndex());
                }
            }
        }
        return (bestMove == null ? lastInspectedMove : bestMove);
	}

	@Override
	public Move evaluateBestMoveFactoring(
			HashMap<UCTSubPlayer, List<Node>> resultMap, boolean logging,
			List<UCTSubPlayer> spList) {
		// TODO complete these method
		throw new NotImplementedException();
	}

}
