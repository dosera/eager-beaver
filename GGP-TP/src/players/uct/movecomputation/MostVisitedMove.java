package players.uct.movecomputation;

import java.util.HashMap;
import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.tree.Node;
import statemachine.Move;
import statemachine.Role;

public class MostVisitedMove extends AMoveComputation {

	public MostVisitedMove(Role ourRole)
	{
		super.ourRole = ourRole;
	}
	@Override
	public Move evaluateBestMove(HashMap<UCTSubPlayer, List<Node>> resultMap, boolean logging) {
		Move bestMove = null;
        // this move is used to ensure that we don't return null
        // even if we don't have any possiblity to win
        Move lastInspectedMove = null;
        long lastInspectedMoveVisits = 0;
        boolean isGoalMove = false;
        long goalMoveVisits = 0;
        for (UCTSubPlayer sp : resultMap.keySet()) {
            
        	// logging
            if(logging)
            	super.log(sp);
            
            long mostVisits = Long.MIN_VALUE;
            Node rootNode = sp.getTree().getRootNode();
            
            for (int i = 0; i < rootNode.getChildren().size(); i++) {
            	// update the last inspected move
            	// such that we don't use the last move in general but the one 
            	// with the most visits (situation: every node is a IGNORENODE 
            	// -> use the move with the most visits
            	if(lastInspectedMoveVisits <= rootNode.getChildren().get(i).getVisits())
            	{
            		lastInspectedMove = rootNode.getChildren().get(i).getMove()[sp.getMyRoleIndex()];
            		lastInspectedMoveVisits = rootNode.getChildren().get(i).getVisits();
            	}
                // pruned nodes shall be ignored
                if (rootNode.getChildren().get(i).isPrunable())
                    continue;
                // do we have more than one legal move?
                if (rootNode.getPossibleMovesOfRoleIndex()[sp.getMyRoleIndex()] > 1) {
                    // win situation
                    if (rootNode.getChildren().get(i).LeadsToWin()) {
                    	isGoalMove = true;
                    	if(rootNode.getChildren().get(i).getVisits() > goalMoveVisits)
                    	{
                    		bestMove = rootNode.getChildren().get(i).getMove()[sp.getMyRoleIndex()];
                    		goalMoveVisits = rootNode.getChildren().get(i).getVisits();
                    	}
                    }
                    // defeat situation
                    if (rootNode.getChildren().get(i).LeadsToDefeat()) {
                        continue;
                    }
                }
                
                if (rootNode.getChildren().get(i).getVisits() > mostVisits && !isGoalMove) {
                    bestMove = rootNode.getChildren().get(i).getMove()[sp
                            .getMyRoleIndex()];
                    mostVisits = rootNode.getChildren().get(i).getVisits();
                }
            }
        }
        return (bestMove == null ? lastInspectedMove : bestMove);
	}
	
	
	// TODO
	// it should be possible to remove the spList as argument and just loop through the map somehow
	@Override
	public Move evaluateBestMoveFactoring(
			HashMap<UCTSubPlayer, List<Node>> resultMap, boolean logging, List<UCTSubPlayer> spList) {
		HashMap<UCTSubPlayer, Node> noopmoves = new HashMap<UCTSubPlayer, Node>();
        HashMap<UCTSubPlayer, Node> bestmoves = new HashMap<UCTSubPlayer, Node>();

        for (UCTSubPlayer sp : spList) {
        	if(logging)
				super.log(sp);
            Node rootNode = sp.getTree().getRootNode();
            for (Node node : rootNode.getChildren()) {
                // for every node, check if its a noop move
                Move move = node.getMove()[sp.getMyRoleIndex()];
                if (move.toString().equalsIgnoreCase("NOOP"))
                    // save the visits of the noop move
                    noopmoves.put(sp, node);
                else {
                    if (node.isPrunable())
                        continue;
                    // replace if this move is better than the currently best
                    // move
                    if (bestmoves.get(sp) == null) {
                        bestmoves.put(sp, node);
                    } else {
                        if (bestmoves.get(sp).getVisits() <= node.getVisits())
                            bestmoves.put(sp, node);
                    }
                }
            }
        }
        // compute the best move
        long bestmovescore = 0;
        Node bestmove = null;
        UCTSubPlayer bestmovesp = null;
        for (UCTSubPlayer sp : spList) {
            // if(bestmoves.get(sp) == null)
            // continue;
            long actualmove = bestmoves.get(sp).getVisits();
            boolean dontmove = false;
            if (wonAfterMove(bestmoves.get(sp), sp)) {
                return bestmoves.get(sp).getMove()[sp.getMyRoleIndex()];
            }
            for (UCTSubPlayer otherplayers : spList) {
                if (!otherplayers.equals(sp)) {
                    if (lostAfterMove(noopmoves.get(otherplayers), otherplayers)) {
                        dontmove = true;
                        break;
                    }
                    actualmove += noopmoves.get(otherplayers).getVisits();
                }
            }
            if (actualmove > bestmovescore && !dontmove) {
                bestmovescore = actualmove;
                bestmove = bestmoves.get(sp);
                bestmovesp = sp;
            }
        }
        if (bestmove == null)
            return bestmoves.get(spList.get(0)).getMove()[spList.get(0)
                    .getMyRoleIndex()];
        return bestmove.getMove()[bestmovesp.getMyRoleIndex()];
	}

}
