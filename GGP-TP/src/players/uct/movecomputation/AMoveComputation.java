package players.uct.movecomputation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.FastHandler;
import players.uct.strategyhandler.MastHandler;
import players.uct.strategyhandler.PastHandler;
import players.uct.strategyhandler.methods.FastMastStrategy;
import players.uct.strategyhandler.methods.FastPastStrategy;
import players.uct.strategyhandler.methods.RaveFastStrategy;
import players.uct.strategyhandler.methods.RaveMastStrategy;
import players.uct.strategyhandler.methods.RavePastFastStrategy;
import players.uct.strategyhandler.methods.RavePastStrategy;
import players.uct.strategyhandler.methods.RaveStrategy;
import players.uct.tree.Node;
import statemachine.Move;
import statemachine.Role;
import core.gdl.GdlSentence;
import core.logging.TheLogger;

public abstract class AMoveComputation {

	Role ourRole;

	public abstract Move evaluateBestMove(
			HashMap<UCTSubPlayer, List<Node>> resultMap, boolean logging);

	/**
	 * evaluates the best move, if we have multiple sub players
	 * 
	 * @param resultMap
	 *            map with values for each move and each subplayer
	 * @return the best move
	 */
	public abstract Move evaluateBestMoveFactoring(
			HashMap<UCTSubPlayer, List<Node>> resultMap, boolean logging,
			List<UCTSubPlayer> spList);

	/** returns if we have lost the game after this move */
	public boolean lostAfterMove(Node node, UCTSubPlayer sp) {
		return (node.LeadsToDefeat() && sp.getSM().isTerminal(node.getMS()));

	}

	/** returns if we have won the game after this move */
	public boolean wonAfterMove(Node node, UCTSubPlayer sp) {
		return (node.LeadsToWin() && sp.getSM().isTerminal(node.getMS()));
	}

	// remove this....
	protected void log(UCTSubPlayer sp) {
		String p = "-----------------------------------------------------------------";
		int totalvisits = 0;
		TheLogger.LOGGER.fine(p);

		// fat logging part
		for (int i = 0; i < sp.getTree().getRootNode().getChildren().size(); i++) {

			totalvisits += sp.getTree().getRootNode().getChildren().get(i)
					.getVisits();
			Node currChild = sp.getTree().getRootNode().getChildren().get(i);

			String scoreStr = "[";
			for (int k = 0; k < currChild.getWins().length; k++) {
				if (this.ourRole.equals(sp.getRoleIndexMap().getKey(k)))
					scoreStr += "*"; // indicator for "our" role
				scoreStr += currChild.getWins()[k]
						+ ((k == currChild.getWins().length - 1) ? "" : ", ");
			}
			scoreStr += "]";

			String output = String.format("Moves:%s wins:%-20s visits:%-6s %s",
					Arrays.toString(currChild.getMove()), scoreStr,
					currChild.getVisits(), additionalInfo(currChild, sp));
			TheLogger.LOGGER.fine(output);
		}
		fastString(sp); // output of the fast values
		TheLogger.LOGGER.log(Level.CONFIG, String.format(
				"UCT visits on the game path: %d, possible Moves: %d",
				totalvisits, sp.getTree().getRootNode().getChildren().size()));
	}

	private String additionalInfo(Node currChild, UCTSubPlayer sp) {
		String result = "";
		if (this instanceof AverageWinsVisitsMove)
			result += String.format(" wins/visits: %.4f ",
					currChild.getAverageScore(sp.getMyRoleIndex()));

		result += mastString(currChild, sp);
		result += raveString(currChild, sp);

		// PAST logging deactivated for quality reasons
//		result += pastString(currChild, sp);

		// win / defeat / prune logging
		if (currChild.LeadsToWin())
			result += " - GOAL FOUND";
		if (currChild.LeadsToDefeat())
			result += " - IGNORE NODE";
		if (currChild.isPrunable())
			result += " - PRUNED";
		return result;
	}

	private String mastString(Node currChild, UCTSubPlayer sp) {
		if ((sp.getStrategy() instanceof MastHandler)
				&& !currChild.getMove()[sp.getMyRoleIndex()].toString()
						.equalsIgnoreCase("NOOP"))
			return String.format(
					" MAST: %.4f ",
					(float) ((MastHandler) (sp.getStrategy())).getMastMap()
							.get(currChild.getMove()[sp.getMyRoleIndex()])[0]
							/ (float) ((MastHandler) (sp.getStrategy()))
									.getMastMap().get(
											currChild.getMove()[sp
													.getMyRoleIndex()])[1]);
		else if ((sp.getStrategy() instanceof FastMastStrategy)
				&& !currChild.getMove()[sp.getMyRoleIndex()].toString()
				.equalsIgnoreCase("NOOP"))
	return String.format(
			" MAST: %.4f ",
			(float) ((FastMastStrategy) (sp.getStrategy())).getMastMap()
					.get(currChild.getMove()[sp.getMyRoleIndex()])[0]
					/ (float) ((FastMastStrategy) (sp.getStrategy()))
							.getMastMap().get(
									currChild.getMove()[sp
											.getMyRoleIndex()])[1]);
		return "";
	}

	private String pastString(Node currChild, UCTSubPlayer sp) {
		if (sp.getStrategy() instanceof PastHandler
				&& !currChild.getMove()[sp.getMyRoleIndex()].toString()
						.equalsIgnoreCase("NOOP")) {
			String result = "";
			for (GdlSentence sentence : ((PastHandler) sp.getStrategy()).actionPredicateValues
					.get(currChild.getMove()[sp.getMyRoleIndex()]).keySet()) {
				result += ((PastHandler) sp.getStrategy()).actionPredicateValues
						.get(currChild.getMove()[sp.getMyRoleIndex()]).get(
								sentence)[0]
						/ ((PastHandler) sp.getStrategy()).actionPredicateValues
								.get(currChild.getMove()[sp.getMyRoleIndex()])
								.get(sentence)[1];
				result += ", ";
			}
			return String.format(" PAST: %s ",
					result.substring(0, result.length() - 2));
		}
		else if (sp.getStrategy() instanceof FastPastStrategy
					&& !currChild.getMove()[sp.getMyRoleIndex()].toString()
							.equalsIgnoreCase("NOOP")) {
				String result = "";
				for (GdlSentence sentence : ((FastPastStrategy) sp.getStrategy()).actionPredicateValues
						.get(currChild.getMove()[sp.getMyRoleIndex()]).keySet()) {
					result += ((FastPastStrategy) sp.getStrategy()).actionPredicateValues
							.get(currChild.getMove()[sp.getMyRoleIndex()]).get(
									sentence)[0]
							/ ((FastPastStrategy) sp.getStrategy()).actionPredicateValues
									.get(currChild.getMove()[sp.getMyRoleIndex()])
									.get(sentence)[1];
					result += ", ";
				}
				return String.format(" PAST: %s ",
						result.substring(0, result.length() - 2));
			}
		return "";
	}

	private String raveString(Node currChild, UCTSubPlayer sp) {
		if ((sp.getStrategy() instanceof RaveStrategy
				|| sp.getStrategy() instanceof RaveMastStrategy || sp
					.getStrategy() instanceof RaveFastStrategy || sp
					.getStrategy() instanceof RavePastStrategy || sp
					.getStrategy() instanceof RavePastFastStrategy)
				&& !currChild.getMove()[sp.getMyRoleIndex()].toString()
						.equalsIgnoreCase("NOOP"))
			return String.format(" RAVE: %.4f ", currChild.getQRaveScore()
					/ (float) currChild.getQRaveVisits());
		return "";
	}
	
	private void fastString(UCTSubPlayer sp) {
		if (sp.getStrategy() instanceof FastHandler)
		{
			FastHandler fh = (FastHandler)sp.getStrategy();
			String result = "";
			for(int i=0; i < fh.getFeatureList().size(); i++)
			{
				result += fh.getFeatureList().get(i) + ": " + Arrays.toString(fh.getFeatureWeights().get(i)) + ((i == fh.getFeatureList().size() - 1) ? "" : ", ");
			}
			TheLogger.LOGGER.log(Level.FINE, "FAST: " + result);
		}
				
	}
}
