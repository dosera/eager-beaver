package players.uct;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import players.uct.tree.Node;
import players.uct.tree.Tree;
import core.logging.TheLogger;

/**
 * @author Phil
 * 
 *         visualizes a UCT search tree by exporting it into a .dot compatible
 *         file format
 */
public final class UCTVisualization {

	public static void drawTree(String filename, Tree t, int depth) {
		try {
			File f = new File(filename);
			FileOutputStream fos = new FileOutputStream(f);
			OutputStreamWriter fout = new OutputStreamWriter(fos, "UTF-8");

			List<String> lines = new ArrayList<String>();
			//
			StringBuilder sb = new StringBuilder();

			sb.append("digraph gameTree\n{\n");

			dfs(t.getRootNode(), 0, depth, lines);

			for (String line : lines)
				sb.append(line + ";\n");

			sb.append("}");

			fout.write(sb.toString());
			//

			fout.close();
			fos.close();
		} catch (Exception e) {
			TheLogger.LOGGER.log(Level.SEVERE, "StateMachine", e);
		}
	}

	public static void dfs(Node node, int currDepth, int toDepth,
			List<String> lines) {
		if (node.getChildren().size() == 0 || currDepth >= toDepth) {
			lines.add(node.toDotString(false));
			return;
		}

		lines.add(node.toDotString(true));

		for (int i = 0; i < node.getChildren().size(); i++) {
			dfs(node.getChildren().get(i), currDepth + 1, toDepth, lines);
		}
	}

}
