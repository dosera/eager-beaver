/**
 * 
 */
package players.uct.selection;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;
import core.Settings;

/**
 * @author tasse
 * 
 *         First UCT Computation Step: Select a leaf node out of the search tree
 *         which fits best according to the uct formula specified in the current
 *         strategy.
 */
public abstract class ASelection {

	float constant = (float) Math.sqrt(2);

	public float getConstant() {
		return this.constant;
	}

	/**
	 * returns the best node according to the corresponding uct formula
	 */
	public Node calculateBestNode(Node node, int roleIndex,
			AStrategyHandler strategy, boolean maximize) {

		Node bestNode = null;
		float bestScore = (maximize ? Float.NEGATIVE_INFINITY
				: Float.POSITIVE_INFINITY);

		for (int i = 0; i < node.getChildren().size(); i++) {
			// overvisit check
			if (node.getChildren().get(i).overvisited >= 0) {
				node.getChildren().get(i).overvisited--;
				continue;
			}
			// lock node iff it has been visited k = Settings.overvisited times
			// more often than its siblings
			lockOverVisitedNodes(node, i, Settings.overvisited);

			// calculation of uct stuff
			// is the "goal score" always 100? or does it depend on the game?
			// if so ~> do sth dependent on the goalvalue
			float uctscore = computeScore(node.getChildren().get(i), roleIndex,
					strategy, maximize);
			if ((maximize && bestScore < uctscore)
					|| (!maximize && bestScore > uctscore)) {
				bestNode = node.getChildren().get(i);
				bestScore = uctscore;
			}
		}
		return bestNode;
	}

	/**
	 * if we visit a node more than #times time we lock this node for some time
	 * 
	 * @param node
	 * @param i
	 * @param times
	 */
	void lockOverVisitedNodes(Node node, int i, int times) {
		if ((node.getChildren().size() - 1) != 0) {
			int total = 0;
			int x = 0;
			for (int j = 0; j < node.getChildren().size(); j++) {
				if (i == j || node.getChildren().get(j).overvisited >= 0) {
					x++;
					continue;
				}
				total += node.getChildren().get(j).getVisits();
			}
			float mean = (float) ((float) total / ((float) node.getChildren()
					.size() - x));
			if (mean * times < (float) node.getChildren().get(i).getVisits()) {
				node.getChildren().get(i).overvisited = (int) node
						.getChildren().get(i).getVisits();
			}
		}
	}

	/**
	 * Computes the UCT-Score for a move.
	 * @param child: contains the move that is evaluated
	 * @param roleIndex: returns the score for that role
	 * @param strategy
	 * @param maximize
	 * @return uctscore
	 */
	abstract float computeScore(Node child, int roleIndex,
			AStrategyHandler strategy, boolean maximize);

	/**
	 * updates the bias - accumulated rewards divided by number of visits -
	 * 
	 * @param node
	 */
	public void updateBias(Node node, UCTSubPlayer sp) {
		this.constant = (float) Math.sqrt(node.getWins()[sp.getMyRoleIndex()]
				/ node.getVisits());
	}
}
