/**
 * 
 */
package players.uct.selection;

import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;

/**
 * @author Phil
 * 
 *         Class for the UCT Computation UCBTuned 2
 */
public class UCBTuned2Computation extends ASelection {

	@Override
	public float computeScore(Node child, int roleIndex,
			AStrategyHandler strategy, boolean maximize) {
		float mcscore = (float) child.getWins()[roleIndex]
				/ (long) child.getVisits();
		float visroot = (float) Math.log(child.getParent().getVisits());
		float uctvalue = (float) Math.sqrt(visroot / (long) child.getVisits());

		// ucbtuned2 computation
		float tuned2_constant = (float) Math.sqrt(Math.max(0.001, mcscore
				* (1 - mcscore)));
		float uctscore = mcscore
				+ (maximize ? tuned2_constant * uctvalue + visroot
						/ (long) child.getVisits() : -1 * tuned2_constant
						* uctvalue + visroot / (long) child.getVisits());
		return uctscore;
	}
}