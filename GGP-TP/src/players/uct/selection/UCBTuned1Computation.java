/**
 * 
 */
package players.uct.selection;

import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;

/**
 * @author Phil
 * 
 *         Class for the UCT Computation UCBTuned 1
 */
public class UCBTuned1Computation extends ASelection {

	@Override
	public float computeScore(Node child, int roleIndex,
			AStrategyHandler strategy, boolean maximize) {
		float mcscore = (float) child.getWins()[roleIndex]
				/ (long) child.getVisits();
		float visroot = (float) Math.log(child.getParent().getVisits());
		float uctvalue = (float) Math.sqrt(visroot / (long) child.getVisits());

		// ucbtuned1 computation
		float tuned1_constant = (float) Math.sqrt(Math.max(0.001, mcscore
				* (1 - mcscore)));
		float uctscore = mcscore
				+ (maximize ? tuned1_constant * uctvalue : -1 * tuned1_constant
						* uctvalue);

		return uctscore;
	}

}