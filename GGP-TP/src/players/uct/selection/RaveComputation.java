/**
 * 
 */
package players.uct.selection;

import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;
import core.Settings;

/**
 * @author Phil
 * 
 *         This class extends the UCT formula by so called rave values.
 */
public class RaveComputation extends ASelection {

	@Override
	public float computeScore(Node child, int roleIndex,
			AStrategyHandler strategy, boolean maximize) {
		float QRaveSA = (float) (child.getQRaveScore() + (float) (1 / child
				.getQRaveVisits() * (child.lastScore[roleIndex] - child
				.getQRaveScore())));
		float ms = 0f;
		for (Node c : child.getParent().getChildren())
			ms += c.getQRaveVisits();
		float QRavePlusSA = (float) (QRaveSA + super.constant
				* Math.sqrt(Math.log(ms) / child.getQRaveVisits()));

		float betasa = 0f;

		if (!Settings.betterBeta)
			betasa = (float) Math.sqrt(Settings.k
					/ (3 * child.getParent().getVisits() + Settings.k));
		else
			betasa = (child.getQRaveVisits() / ((float) child.getVisits()
					+ (float) child.getQRaveVisits() + (float) (4
					* child.getVisits() * child.getQRaveVisits() * Math.pow(
					(Settings.k), 2))));

		// QURplussa
		return betasa
				* QRavePlusSA
				+ (1 - betasa)
				* strategy.getOtherSelection().computeScore(child, roleIndex,
						strategy, maximize);
	}
}