/**
 * 
 */
package players.uct.selection;

import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;

/**
 * @author Phil
 * 
 *         Class for the UCT Computation UCB1 wins(child) / visits(child) +
 *         constant * Sqrt(Log(visits(parent)) / visits(child))
 */
public class UCB1Computation extends ASelection {

	@Override
	public float computeScore(Node child, int roleIndex,
			AStrategyHandler strategy, boolean maximize) {
		float mcscore = (float) child.getWins()[roleIndex]
				/ (long) child.getVisits();
		float visroot = (float) Math.log(child.getParent().getVisits());
		float uctvalue = (float) Math.sqrt(visroot / (long) child.getVisits());
		float uctscore = mcscore
				+ (maximize ? constant * uctvalue : -1 * constant * uctvalue);
		;
		return uctscore;
	}
}