/**
 * 
 */
package players.uct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.BidirectionalMap;
import players.uct.tree.Node;
import players.uct.tree.Tree;
import statemachine.MachineState;
import statemachine.Role;
import statemachine.StateMachine;
import core.Settings;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.TransitionDefinitionException;
import core.logging.TheLogger;

/**
 * @author Phil
 * 
 *         The UCTSubPlayer is meant to execute the UCT algorithm. Each
 *         subplayer has its own statemachine and searchtree.
 */
public class UCTSubPlayer {

	AStrategyHandler strategy;
	private Tree t;
	private StateMachine sm;
	// map containing all possible end scores for each player
	private HashMap<Role, List<Integer>> possibleEndScoresMap = new HashMap<Role, List<Integer>>();
	// map that matches a role to an index
	private BidirectionalMap<Role, Integer> roleIndexMap = new BidirectionalMap<Role, Integer>();
	// role index of our player
	private int myRoleIndex;
	// the date till the computation has to be finished
	public long dateTillFinish;
	// this int represents the depth of the child we are starting our simulation
	// from
	private int depthOfLeafNode = 0;

	public UCTSubPlayer(StateMachine sm, Role myRole, MachineState currState) {
		this.sm = sm;
		this.t = new Tree(currState, this.getSM().getRoles().size());

		// we initialize the Role <-> index map
		for (int i = 0; i < sm.getRoles().size(); i++) {
			if (sm.getRoles().get(i).equals(myRole))
				this.myRoleIndex = i;
			this.getRoleIndexMap().put(sm.getRoles().get(i), i);
		}
	}

	/**
	 * sets the strategy which is used for UCT computation
	 * 
	 * @param strategy
	 */
	public void setStrategy(AStrategyHandler strategy) {
		this.strategy = strategy;
	}

	// constructor used for tests
	public UCTSubPlayer(AStrategyHandler strategy, Tree t, StateMachine sm,
			HashMap<Role, List<Integer>> possibleEndScoresMap,
			BidirectionalMap<Role, Integer> roleIndexMap, int myIndex) {
		this.strategy = strategy;
		this.t = t;
		this.sm = sm;
		this.possibleEndScoresMap = possibleEndScoresMap;
		this.roleIndexMap = roleIndexMap;
		this.myRoleIndex = myIndex;
	}

	// setter and getter
	public BidirectionalMap<Role, Integer> getRoleIndexMap() {
		return this.roleIndexMap;
	}

	public int getMyRoleIndex() {
		return this.myRoleIndex;
	}

	public List<Integer> getPossibleEndScores(Role r) {
		return possibleEndScoresMap.get(r);
	}

	public void putPossibleEndScores(Role r, List<Integer> l) {
		this.possibleEndScoresMap.put(r, l);
	}

	public Role getMyRole() {
		return roleIndexMap.getKey(myRoleIndex);
	}

	public Tree getTree() {
		return t;
	}

	public StateMachine getSM() {
		return sm;
	}

	public AStrategyHandler getStrategy() {
		return strategy;
	}

	public int getDepthOfLeafNode() {
		return depthOfLeafNode;
	}

	/**
	 * The update tree method reuses already computed parts of the tree (if the
	 * treeswapping option is set, otherwise a new tree is generated after each
	 * played move)
	 * 
	 * @param ms
	 *            : the current machine state
	 */
	public void updateTree(MachineState ms) {
		boolean found = false;

		if (Settings.treeswapping) {
			// this is the root node case
			if (ms.equals(this.t.getRootNode().getMS()))
				return;

			for (int i = 0; i < this.t.getRootNode().getChildren().size(); i++) {
				// loop through all current children and
				// select the a new root node
				if (ms.equals(this.t.getRootNode().getChildren().get(i).getMS())) {
					this.t.swapToNewRootNode(this.t.getRootNode().getChildren()
							.get(i));
					found = true;
					break;
				}
			}
		}

		// if we can't find a new root node, reset the tree
		if (!found) {
			this.t.swapToNewRootNode(new Node(null, new ArrayList<Node>(),
					new LinkedList<Node>(), ms, null, this.getRoleIndexMap()
							.size()));

			if (Settings.treeswapping)
				TheLogger.LOGGER
						.severe("ERROR while swapping tree nodes - couldnt find a new rootnode");
		}

	}

	/**
	 * executes a single iteration of a UCT computation by stepping once through
	 * each of the four phases
	 * 
	 * @throws GoalDefinitionException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 */
	public void run() throws GoalDefinitionException, MoveDefinitionException,
			TransitionDefinitionException {

		strategy.preComputeEveryRun();

		Node leafNode = null;

		if (this.dateTillFinish < System.currentTimeMillis())
			return;
		// track the depth of the leafNode we select
		int[] depthOfLeafNodeArray = new int[] { 0 };

		/** selection call **/
		leafNode = strategy.select(t.getRootNode(), Settings.maximize,
				depthOfLeafNodeArray);

		// we update the depth of the leafeNode int
		depthOfLeafNode = depthOfLeafNodeArray[0];

		// all children are locked, no leafnode has been found to work on ->
		// this thread has nothing to do
		if (leafNode == null)
			return;

		/** expansion call **/
		ArrayList<Node> expandedChildren = strategy.expand(leafNode);

		if (this.dateTillFinish < System.currentTimeMillis())
			return;

		/** simulation call **/
		strategy.simulate(expandedChildren);

		if (this.dateTillFinish < System.currentTimeMillis())
			return;

		/** backpropagation call **/
		strategy.backpropagate(expandedChildren);

		if (this.dateTillFinish < System.currentTimeMillis())
			return;

		strategy.postComputeEveryRun(leafNode);
	}
}
