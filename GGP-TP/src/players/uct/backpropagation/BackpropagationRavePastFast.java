package players.uct.backpropagation;

import java.util.List;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;
import statemachine.Move;

public class BackpropagationRavePastFast extends BackpropagationFastPast{
	
	@Override
	protected void doInEveryRecursionStep(Node child, Node simulationChild,
			float[] lastScore, AStrategyHandler strategy, UCTSubPlayer sp) {
		// call the parent method
		super.doInEveryRecursionStep(child, simulationChild, lastScore, strategy, sp);
		
		List<Node> mySiblings = child.getParent().getChildren();

		// has a sibling a move that has been played on the last way down?
		// if so we update his Rave values
		List<Move> list = simulationChild.followingMovesRave;

		for (int i = 0; i < mySiblings.size(); i++) {
			// we ignore ourselves TODO
			if (child.getMS().equals(mySiblings.get(i).getMS()))
				continue;

			if (list.contains(mySiblings.get(i).getMove()[sp.getMyRoleIndex()])) {
				mySiblings.get(i).setQRaveScore(
						mySiblings.get(i).getQRaveScore()
								+ child.lastScore[sp.getMyRoleIndex()]);
				mySiblings.get(i).setQRaveVisits(
						mySiblings.get(i).getQRaveVisits() + 1);
			}
		}
	}

}
