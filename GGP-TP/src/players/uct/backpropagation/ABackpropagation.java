package players.uct.backpropagation;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;

/**
 * @author Phil
 * 
 *         Abstract class handling the backpropagation.
 */
public abstract class ABackpropagation {

	/**
	 * Propagates the score gained by the simulation upwards through the tree.
	 * 
	 * @param currentChild
	 *            the child we are currently looking at
	 * @param simulationStartChild
	 *            the child where the simulation started from
	 * @param lastScore
	 *            the score received by the simulation we are propagating
	 * @param strategy
	 * @param sp
	 */
	public void propagate(Node currentChild, Node simulationStartChild,
			float[] lastScore, AStrategyHandler strategy, UCTSubPlayer sp) {
		if (currentChild.getParent() == null)
			return;
		currentChild.getParent().updateValues(lastScore);

		doInEveryRecursionStep(currentChild, simulationStartChild, lastScore,
				strategy, sp);

		// we check if our parent has no unexpanded children AND
		// if we only have one possible move [-> no choice]
		// and if we are locked ourselves
		// if so, we lock the parent as well
		this.checkIgnoreNode(currentChild, sp);
		// are we allowed to prune here?
		this.prune(currentChild, strategy, sp);

		propagate(currentChild.getParent(), simulationStartChild, lastScore,
				strategy, sp);
	}

	public void propagate(Node child, float[] lastScore,
			AStrategyHandler strategy, UCTSubPlayer sp) {
		// first child = currentchild (changes during recursion steps)
		// second child = simulation start child
		this.propagate(child, child, lastScore, strategy, sp);
	}

	/**
	 * This method is executed during backpropagation in each recursion step.
	 */
	protected abstract void doInEveryRecursionStep(Node child,
			Node simulationChild, float[] lastScore, AStrategyHandler strategy,
			UCTSubPlayer sp);

	/**
	 * we check if our parent has no unexpanded children AND if we only have one
	 * possible move [-> no choice] and if we are locked ourselves if so, we
	 * lock the parent as well
	 * 
	 * @param child
	 */
	protected void checkIgnoreNode(Node child, UCTSubPlayer sp) {
		if (!child.getParent().hasUnexpandedChildren()) {
			// defeat situation
			if (child.getParent().getPossibleMovesOfRoleIndex()[sp
					.getMyRoleIndex()] == 1 && child.LeadsToDefeat()) {
				child.getParent().setLeadsToDefeat(true);
			}
			// defeat situation for simultaneous cases
			// that means if a move like [x,y] leads to a defeat situation and
			// is being locked
			// we also want to lock all moves [x,a] for all a

			simultaneousDefeatSituations(child, sp.getMyRoleIndex());

			// win situation
			// only relevant in multiplayer games - second part of the if check
			if (child.LeadsToWin() && sp.getRoleIndexMap().size() > 1) {
				boolean moreThanOnePossibleMoveOpponents = false;
				for (int i = 0; i < sp.getRoleIndexMap().size(); i++) {
					if (i == sp.getMyRoleIndex())
						continue;
					if (child.getParent().getPossibleMovesOfRoleIndex()[i] > 1)
						moreThanOnePossibleMoveOpponents = true;
				}
				if (!moreThanOnePossibleMoveOpponents)
					child.getParent().setLeadsToWin(true);
			}
			// singleplayer case for backpropagation of win flag
			else if (child.LeadsToWin() && sp.getRoleIndexMap().size() == 1) {
				if (child.getParent().getPossibleMovesOfRoleIndex()[sp
						.getMyRoleIndex()] == 1)
					child.getParent().setLeadsToWin(true);
			}
		}
	}

	/**
	 * PROBABLY NOT WORKING YET --- EXPERIMENTAL!!! --- defeat situation for
	 * simultaneous cases that means if a move like [x,y] leads to a defeat
	 * situation and is being locked we also want to lock all moves [x,a] for
	 * all a
	 * 
	 * @param child
	 * @param sp
	 */
	protected void simultaneousDefeatSituations(Node child, int myRoleIndex) {
		boolean sbHasMoreThanOneMove = false;
		// lastScore.length = sp.getRoleIndexMap.size()
		for (int i = 0; i < child.lastScore.length; i++) {
			// somebody has one move or more
			if (child.getPossibleMovesOfRoleIndex()[i] > 1) {
				sbHasMoreThanOneMove = true;
			}
		}
		if (sbHasMoreThanOneMove) {
			boolean everyChildLeadsToDefeat = true;
			for (int i = 0; i < child.getChildren().size(); i++) {
				if (!child.getChildren().get(i).LeadsToDefeat()) {
					everyChildLeadsToDefeat = false;
					continue;
				}
				for (int j = 0; j < child.getChildren().size(); j++) {
					if (i == j)
						continue;
					if (child.getChildren().get(i).getMove()[myRoleIndex]
							.equals(child.getChildren().get(j).getMove()[myRoleIndex])) {
						child.getChildren().get(j).setLeadsToDefeat(true);
					}
				}
			}
			// since every child of the child leads to a defeat
			// we also want to block the parent (= child) node
			if (everyChildLeadsToDefeat)
				child.setLeadsToDefeat(true);
		}
	}

	/**
	 * this method is used to check whether we are allowed to prune parts of our
	 * tree. this is used to win games like lightsOn. At the moment we only
	 * check for pruning if we are in a single player game. The multiplayer part
	 * is experimental.
	 * 
	 * @param child
	 *            : is this child prunable?
	 * @param strategy
	 *            : current strategy
	 * @param sp
	 *            : current UCTSubPlayer
	 */
	protected void prune(Node child, AStrategyHandler strategy, UCTSubPlayer sp) {
		// we only do this in singleplayergames TODO: can we do it in
		// multiplayer?
		if (sp.getRoleIndexMap().size() > 1)
			return;

		// set the scores if necessary
		if (child.getNearestGoalScore() > child.getParent()
				.getNearestGoalScore()) {
			child.getParent().setNearestGoalDepth(
					child.getNearestGoalDepth() + 1);
			child.getParent().setNearestGoalScore(child.getNearestGoalScore());
		} else if (child.getParent().getNearestGoalScore() == child
				.getNearestGoalScore()
				&& child.getNearestGoalDepth() + 1 < child.getParent()
						.getNearestGoalDepth()) {
			child.getParent().setNearestGoalDepth(
					child.getNearestGoalDepth() + 1);
			// we don't have to set the score since they are the same
		}

		// ignore the root node
		for (int i = 1; i < strategy.lastSelectedNodes.size(); i++) {
			// are the last two moves the same?
			if (child.getMove()[sp.getMyRoleIndex()]
					.equals(strategy.lastSelectedNodes.get(i).getMove()[sp
							.getMyRoleIndex()])) {
				// prune only if the goal depth of the child is greater
				// than the goal depth of the father
				// this condition assures that we don't prune if we only look at
				// the same edge
				if (child.getNearestGoalDepth() + 1 > child.getParent()
						.getNearestGoalDepth()) {
					// prune only if the child does not have better scores
					if (child.getNearestGoalScore() <= child.getParent()
							.getNearestGoalScore()) {
						// is the possibleendscores map filled? that means if we
						// had an variable as endscore (eg. chinese checkers 3)
						// the map is not filled and we cannot prune
						if (sp.getPossibleEndScores(sp.getMyRole()).size() != 0) {
							// if the parent's score is NOT the maximum
							// available in the current game
							// we may lose information if we prune
							if (child.getParent().getNearestGoalScore() == sp
									.getPossibleEndScores(sp.getMyRole()).get(
											sp.getPossibleEndScores(
													sp.getMyRole()).size() - 1)) {
								child.setIsPrunable(true);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Experimental multiplayer prune - should be reworked before using.
	 */
	protected void pruneMultiplayer(Node child, AStrategyHandler strategy,
			UCTSubPlayer sp) {
		// TODO: WHY ARE THERE <ANY> PINK NODES IN TICTACTOE??

		// find the last node where our choice was > 1
		Node father = getLastFatherWithChoice(child, sp);
		// set the scores if necessary
		if (child.getNearestGoalScore() > father.getNearestGoalScore()) {
			father.setNearestGoalDepth(child.getNearestGoalDepth() + 1);
			father.setNearestGoalScore(child.getNearestGoalScore());
		} else if (father.getNearestGoalScore() == child.getNearestGoalScore()
				&& child.getNearestGoalDepth() + 1 < father
						.getNearestGoalDepth()) {
			father.setNearestGoalDepth(child.getNearestGoalDepth() + 1);
			// we don't have to set the score since they are the same
		}
		// are the last two moves the same for every role?
		boolean allTheSame = true;
		for (int j = 0; j < child.getMove().length; j++) {
			// rootNode
			if (father.getMove() == null) {
				allTheSame = false;
				break;
			}
			if (!child.getMove()[j].equals(father.getMove()[j])) {
				allTheSame = false;
				break;
			}
		}
		if (allTheSame) {
			// prune only if the goal depth of the child is greater
			// than the goal depth of the father
			// this condition assures that we don't prune if we only look at the
			// same edge
			if (child.getNearestGoalDepth() + 1 > father.getNearestGoalDepth()) {
				// prune only if the child does not have better scores
				if (child.getNearestGoalScore() <= father.getNearestGoalScore()) {
					// is the possibleendscores map filled? that means if we had
					// an variable as endscore (eg. chinese checkers 3)
					// the map is not filled and we cannot prune
					if (sp.getPossibleEndScores(sp.getMyRole()).size() != 0) {
						// if the parent's score is NOT the maximum available in
						// the current game
						// we may lose information if we prune
						if (father.getNearestGoalScore() == sp
								.getPossibleEndScores(sp.getMyRole()).get(
										sp.getPossibleEndScores(sp.getMyRole())
												.size() - 1)) {
							child.setIsPrunable(true);
							System.out.println("Multiplayer PRUNE");
						}
					}
				}
			}
		}

	}

	private Node getLastFatherWithChoice(Node child, UCTSubPlayer sp) {
		if (child.getParent().getPossibleMovesOfRoleIndex()[sp.getMyRoleIndex()] > 1)
			return child.getParent();
		Node newChild = getLastFatherWithChoice(child.getParent(), sp);
		return newChild;
	}
}
