package players.uct.backpropagation;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;

/**
 * @author Phil
 * 
 *         Implements the default backpropagation.
 */
public class BackpropagationST extends ABackpropagation {

	@Override
	protected void doInEveryRecursionStep(Node child, Node simulationChild,
			float[] lastScore, AStrategyHandler strategy, UCTSubPlayer sp) {
		// nothing to do!
	}

}
