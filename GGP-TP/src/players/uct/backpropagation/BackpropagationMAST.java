package players.uct.backpropagation;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.MastHandler;
import players.uct.tree.Node;
import statemachine.Move;

/**
 * @author Phil
 * 
 *         Implements backpropagation for MAST.
 */
public class BackpropagationMAST extends ABackpropagation {

	@Override
	protected void doInEveryRecursionStep(Node child, Node simulationChild,
			float[] lastScore, AStrategyHandler strategy, UCTSubPlayer sp) {
		MastHandler mh = (MastHandler) strategy;
		// MAST extension: we save the move/score in the map
		Move m = child.getMove()[sp.getMyRoleIndex()];
		if (!m.toString().equalsIgnoreCase("NOOP")) {
			if (mh.getMastMap().containsKey(m)) {
				mh.getMastMap().get(m)[0] += lastScore[sp.getMyRoleIndex()];
				mh.getMastMap().get(m)[1] += 1;
			} else {
				float[] ourScores = new float[] {
						lastScore[sp.getMyRoleIndex()], 1 };
				mh.getMastMap().put(m, ourScores);
			}
		}
	}
}
