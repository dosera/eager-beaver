package players.uct.backpropagation;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;

public class BackpropagationPastRave extends ABackpropagation {

	private BackpropagationPAST backpropagationPast = new BackpropagationPAST();
	private BackpropagationRAVE backpropagationRave = new BackpropagationRAVE();

	@Override
	protected void doInEveryRecursionStep(Node child, Node simulationChild,
			float[] lastScore, AStrategyHandler strategy, UCTSubPlayer sp) {
		this.backpropagationPast.doInEveryRecursionStep(child, simulationChild,
				lastScore, strategy, sp);
		this.backpropagationRave.doInEveryRecursionStep(child, simulationChild,
				lastScore, strategy, sp);
	}

}
