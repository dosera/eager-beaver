package players.uct.backpropagation;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.tree.Node;

public class BackpropagationMastRave extends ABackpropagation {

	private BackpropagationMAST backpropagationMast = new BackpropagationMAST();
	private BackpropagationRAVE backpropagationRave = new BackpropagationRAVE();

	@Override
	protected void doInEveryRecursionStep(Node child, Node simulationChild,
			float[] lastScore, AStrategyHandler strategy, UCTSubPlayer sp) {
		this.backpropagationMast.doInEveryRecursionStep(child, simulationChild,
				lastScore, strategy, sp);
		this.backpropagationRave.doInEveryRecursionStep(child, simulationChild,
				lastScore, strategy, sp);
	}

}
