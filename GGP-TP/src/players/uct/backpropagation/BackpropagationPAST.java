package players.uct.backpropagation;

import java.util.HashMap;

import players.uct.UCTSubPlayer;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.PastHandler;
import players.uct.tree.Node;
import statemachine.Move;
import core.gdl.GdlSentence;

public class BackpropagationPAST extends ABackpropagation {

	@Override
	protected void doInEveryRecursionStep(Node child, Node simulationChild,
			float[] lastScore, AStrategyHandler strategy, UCTSubPlayer sp) {

		PastHandler strat = (PastHandler) strategy;
		Move myMove = child.getMove()[sp.getMyRoleIndex()];
		// is the child's move already in the HashMap?
		// if not, we create a new hashmap for this move
		// in the actionPredicateValues map
		if (!strat.actionPredicateValues.containsKey(myMove))
			strat.actionPredicateValues.put(myMove,
					new HashMap<GdlSentence, float[]>());

		// we get the predicates of our fathers state
		for (GdlSentence predicate : child.getParent().getMS().getContents()) {
			// the predicate does not exist for this move yet
			// -> we add it
			if (!strat.actionPredicateValues.get(myMove).containsKey(predicate)) {
				strat.actionPredicateValues.get(myMove).put(predicate,
						new float[] { lastScore[sp.getMyRoleIndex()], 1 });
			}
			// we found move and predicate and update the values
			else {
				strat.actionPredicateValues.get(myMove).get(predicate)[0] += lastScore[sp
						.getMyRoleIndex()];
				strat.actionPredicateValues.get(myMove).get(predicate)[1] += 1;
			}
		}
	}

}
