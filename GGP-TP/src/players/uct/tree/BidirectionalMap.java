/**
 * 
 */
package players.uct.tree;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Phil
 * 
 *         This class implements a bidirectional Hashmap.
 */
public class BidirectionalMap<KeyType, ValueType> {
	private Map<KeyType, ValueType> keyToValueMap = new HashMap<KeyType, ValueType>();
	private Map<ValueType, KeyType> valueToKeyMap = new HashMap<ValueType, KeyType>();

	public void put(KeyType key, ValueType value) {
		keyToValueMap.put(key, value);
		valueToKeyMap.put(value, key);
	}

	public ValueType removeByKey(KeyType key) {
		ValueType removedValue = keyToValueMap.remove(key);
		valueToKeyMap.remove(removedValue);
		return removedValue;
	}

	public KeyType removeByValue(ValueType value) {
		KeyType removedKey = valueToKeyMap.remove(value);
		keyToValueMap.remove(removedKey);
		return removedKey;
	}

	public boolean containsKey(KeyType key) {
		return keyToValueMap.containsKey(key);
	}

	public boolean containsValue(ValueType value) {
		return keyToValueMap.containsValue(value);
	}

	/**
	 * @param key
	 * @return the key to the specified value
	 */
	public KeyType getKey(ValueType value) {
		return valueToKeyMap.get(value);
	}

	/**
	 * @param key
	 * @return the value to the specified key
	 */
	public ValueType get(KeyType key) {
		return keyToValueMap.get(key);
	}

	public int size() {
		return this.keyToValueMap.size();
	}
}
