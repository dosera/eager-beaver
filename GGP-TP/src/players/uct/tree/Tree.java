/**
 * 
 */
package players.uct.tree;

import java.util.ArrayList;
import java.util.LinkedList;

import statemachine.MachineState;

/**
 * @author tasse
 * 
 *         Class implementing the search tree.
 */
public final class Tree {

	private Node rootNode;

	// private constructor
	public Tree(MachineState rootState, int numRoles) {
		this.rootNode = new Node(null, new ArrayList<Node>(),
				new LinkedList<Node>(), rootState, null, numRoles);
	}

	public Node getRootNode() {
		return rootNode;
	}

	public void swapToNewRootNode(Node node) {
		this.rootNode = new Node(node, true);
	}
}
