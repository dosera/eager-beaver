/**
 * 
 */
package players.uct.tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import statemachine.MachineState;
import statemachine.Move;

/**
 * @author tasse
 * 
 *         Class implementing a node in the search tree.
 */
public class Node {

	// this is used for rave
	public ArrayList<Move> followingMovesRave = new ArrayList<Move>();

	private float[] wins;
	private long visits;
	private Node parent;
	private List<Node> children;
	private Queue<Node> possibleChildren;

	public Queue<Node> getPossibleChildren() {
		return possibleChildren;
	}

	MachineState ms;
	private Move[] move;

	public float[] lastScore;
	// this is used for the "overvisited" hack
	public int overvisited = -1;

	// QRave values
	private float QRaveScore = 0f;
	private int QRaveVisits = 1;

	// possible number of moves per role index in the corresponding machinestate
	private int[] possibleNumberOfMovesPerRole;

	// array for various flags such as
	// LeadsToDefeat/LeadsToWin/Prunable/hasUnexpandedChildren
	private boolean[] nodeFlags = new boolean[4];

	// goal- depth/score for the current node
	private int nearestGoalDepth = Integer.MAX_VALUE;
	private float nearestGoalScore = Float.NEGATIVE_INFINITY;

	public Node(Node parent, List<Node> children, Queue<Node> possibleChildren,
			MachineState ms, Move[] move, int numRoles) {
		this.children = new ArrayList<Node>(children);
		this.possibleChildren = new LinkedList<Node>(possibleChildren);
		this.parent = parent;
		this.ms = ms;
		this.move = move;

		this.wins = new float[numRoles];
		this.lastScore = new float[numRoles];
		for (int i = 0; i < numRoles; i++) {
			this.wins[i] = 0f;
			this.lastScore[i] = 0f;
		}
		this.visits = 0l;

		this.nodeFlags[3] = true; // this.hasUnexpandedChildren = true;

		this.possibleNumberOfMovesPerRole = new int[numRoles];
	}

	/**
	 * Constructor that takes old values and creates a new root node.
	 * 
	 * @param cnode
	 * @param setAsRootNode
	 */
	public Node(Node cnode, boolean setAsRootNode) {
		this((setAsRootNode ? null : cnode.parent), cnode.children,
				cnode.possibleChildren, cnode.ms, (setAsRootNode ? null
						: cnode.move), cnode.wins.length);
		this.wins = cnode.wins;
		this.visits = cnode.visits;
		this.nodeFlags = cnode.nodeFlags; // reuse all flags
		// this.nodeFlags[3] = false; // except the threadLock
		this.possibleNumberOfMovesPerRole = cnode.getPossibleMovesOfRoleIndex();
	}

	/**
	 * Updates values after one successfully executed run().
	 * @param wins
	 */
	public void updateValues(float[] wins) {
		for (int i = 0; i < wins.length; i++) {
			this.wins[i] += wins[i];
			this.lastScore[i] = wins[i];
		}
		this.visits++;
	}

	public List<Node> getChildren() {
		return children;
	}

	public Node getParent() {
		return parent;
	}

	public MachineState getMS() {
		return ms;
	}

	public Move[] getMove() {
		return move;
	}

	public long getVisits() {
		return this.visits;
	}

	public float[] getWins() {
		return this.wins;
	}

	public float getQRaveScore() {
		return QRaveScore;
	}

	public void setQRaveScore(float qRaveScore) {
		QRaveScore = qRaveScore;
	}

	public int getQRaveVisits() {
		return QRaveVisits;
	}

	public void setQRaveVisits(int qRaveVisits) {
		QRaveVisits = qRaveVisits;
	}

	public String toString() {
		return String.format("Node<m=%s:w=%s:v=%d:c=%d>",
				Arrays.toString(this.move), Arrays.toString(this.wins),
				this.visits, this.getChildren().size());
	}

	public String toDotString(boolean drawChildren) {
		String x = String.valueOf(this.getNearestGoalScore()) + " | "
				+ String.valueOf(this.getNearestGoalDepth());
		String color = "grey";
		if (this.nodeFlags[1]) // win
			color = "green";
		if (this.nodeFlags[0]) // defeat
			color = "red";
		if (this.nodeFlags[2]) // prunable
			color = "pink";
		return toDot("circle", color, String.valueOf(x), drawChildren);
	}

	private String toDot(String shape, String fillcolor, String nodeLabel,
			boolean drawChildren) {
		StringBuilder sb = new StringBuilder();
		int edgeLabelSize = 10;
		sb.append("\"@" + Integer.toHexString(this.hashCode()) + "\"[shape="
				+ shape + ", " + "style= filled, fillcolor=" + fillcolor
				+ ", label=\"" + nodeLabel + "\"] ");
		for (Node child : this.children) {
			if (!drawChildren)
				break;
			String edgeLabel = Arrays.toString(child.move).replace("[", "")
					.replace("]", "");

			sb.append("\"@" + Integer.toHexString(this.hashCode()) + "\"->"
					+ "\"@" + Integer.toHexString(child.hashCode())
					+ "\"[label=\"");
			for (int i = 0; i < edgeLabel.split(" ").length; i++)
				sb.append(edgeLabel.split(" ")[i]);
			sb.append("\", fontsize=\""
					+ edgeLabelSize
					+ "\", color=\""
					+ (child.getMS().equals(child.getParent().getMS()) ? "pink"
							: "grey") + "\"]");
		}
		return sb.toString();
	}

	/**
	 * Returns the average score, meaning wins divided by visits, for a given
	 * roleIndex
	 * 
	 * @param roleIndex
	 * @return
	 */
	public float getAverageScore(int roleIndex) {
		return (this.getVisits() == 0 ? 0 : (this.getWins()[roleIndex] / this
				.getVisits()));
	}

	public boolean LeadsToDefeat() {
		return this.nodeFlags[0];
	}

	public void setLeadsToDefeat(boolean leadsToDefeat) {
		this.nodeFlags[0] = leadsToDefeat;
		if (leadsToDefeat)
			this.nodeFlags[1] = false;
	}

	public int[] getPossibleMovesOfRoleIndex() {
		return possibleNumberOfMovesPerRole;
	}

	public boolean LeadsToWin() {
		return nodeFlags[1];
	}

	public void setLeadsToWin(boolean leadsToWin) {
		this.nodeFlags[1] = leadsToWin;
	}

	public int getNearestGoalDepth() {
		return this.nearestGoalDepth;
	}

	public void setNearestGoalDepth(int depth) {
		this.nearestGoalDepth = depth;
	}

	public float getNearestGoalScore() {
		return this.nearestGoalScore;
	}

	public void setNearestGoalScore(float score) {
		this.nearestGoalScore = score;
	}

	public boolean hasUnexpandedChildren() {
		return this.nodeFlags[3];
	}

	public void setHasUnexpandedChildren(boolean hasUnexpandedChildren) {
		this.nodeFlags[3] = hasUnexpandedChildren;
	}

	public boolean isPrunable() {
		return this.nodeFlags[2];
	}

	public void setIsPrunable(boolean isPrunable) {
		this.nodeFlags[2] = isPrunable;
	}

}
