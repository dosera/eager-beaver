/**
 * 
 */
package players.uct;
import java.util.List;

import junit.framework.Assert;

import helper.KifReader;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import statemachine.StateMachine;
import statemachine.prover.ProverStateMachine;
import core.gdl.*;

/**
 * @author tasse
 *
 */
public class UCTGamerTest {

    static StateMachine sm;
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        List<Gdl> description = KifReader.read("games/TwoButtonGame.kif");

        sm = new ProverStateMachine();
        sm.initialize(description);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void test()
    {
    	Assert.assertEquals(true, true);
    }
}
