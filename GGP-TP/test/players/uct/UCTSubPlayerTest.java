package players.uct;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import players.uct.evaluation.methods.RandomHeuristic;
import players.uct.expansion.ExpandNodebyNode;
import players.uct.selection.UCB1Computation;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.methods.DefaultStrategy;
import players.uct.tree.BidirectionalMap;
import players.uct.tree.Node;
import players.uct.tree.Tree;
import statemachine.Role;
import core.exceptions.GdlFormatException;
import core.exceptions.SymbolFormatException;
import core.gdl.GdlFactory;
import core.gdl.GdlProposition;

;

/**
 * @author tasse
 * 
 */
public class UCTSubPlayerTest {

	Node root;
	UCTSubPlayer sp;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		root = new Node(null, new ArrayList<Node>(), new LinkedList<Node>(),
				null, null, 1);
		root.updateValues(new float[] { .05f });
		root.updateValues(new float[] { .0f });
		root.updateValues(new float[] { .05f });
		root.updateValues(new float[] { .20f });
		root.updateValues(new float[] { .20f });
		root.updateValues(new float[] { .20f });
		root.updateValues(new float[] { 1.00f });

		// manually add a role to the gamer
		UCTGamer gamer = new UCTGamer();
		Method method = gamer.getClass().getDeclaredMethod("changeRole",
				new Class[] { Role.class });
		method.setAccessible(true);
		GdlProposition roleName = (GdlProposition) GdlFactory.create("x");
		method.invoke(gamer, new Role(roleName));

		BidirectionalMap<Role, Integer> roleMap = new BidirectionalMap<Role, Integer>();

		roleMap.put(gamer.getRole(), 0);

		sp = new UCTSubPlayer((AStrategyHandler) null, // strategyhandler
				(Tree) null, // t
				gamer.getStateMachine(), // sm
				null, // possibleEndSC<oresMap
				roleMap, // roleIndexMap
				0); // myIndex
		sp.setStrategy(new DefaultStrategy(sp, new UCB1Computation(),
				new ExpandNodebyNode(), new RandomHeuristic()));

		// public UCTSubPlayer(AStrategyHandler strategy, Tree t, StateMachine
		// sm, HashMap<Role, List> possibleEndScoresMap, BidirectionalMap<Role,
		// Integer> roleIndexMap, int myIndex)

		// t.getRoleIndexMap().put(gamer.getRole(), 0);

		// #1
		Node first_Child = new Node(root, new ArrayList<Node>(),
				new LinkedList<Node>(), null, null, 1);
		first_Child.updateValues(new float[] { .05f });
		// #2
		Node second_Child = new Node(root, new ArrayList<Node>(),
				new LinkedList<Node>(), null, null, 1);
		second_Child.updateValues(new float[] { .0f });
		second_Child.updateValues(new float[] { .05f });
		// #3
		Node third_Child = new Node(root, new ArrayList<Node>(),
				new LinkedList<Node>(), null, null, 1);
		third_Child.updateValues(new float[] { .20f });
		third_Child.updateValues(new float[] { .20f });
		third_Child.updateValues(new float[] { .20f });
		// #4
		Node fourth_Child = new Node(root, new ArrayList<Node>(),
				new LinkedList<Node>(), null, null, 1);
		fourth_Child.updateValues(new float[] { 1.00f });

		root.getChildren().add(first_Child);
		root.getChildren().add(second_Child);
		root.getChildren().add(third_Child);
		root.getChildren().add(fourth_Child);

		// #4.1
		Node fourth_first_Child = new Node(fourth_Child, new ArrayList<Node>(),
				new LinkedList<Node>(), null, null, 1);
		fourth_first_Child.updateValues(new float[] { 1f });
		fourth_Child.getChildren().add(fourth_first_Child);
		// #4.2
		Node fourth_second_Child = new Node(fourth_Child,
				new ArrayList<Node>(), new LinkedList<Node>(), null, null, 1);
		fourth_second_Child.updateValues(new float[] { .9f });
		fourth_Child.getChildren().add(fourth_second_Child);

		// propagate these values to their parent ...
		fourth_Child.updateValues(new float[] { 1f });
		fourth_Child.updateValues(new float[] { .9f });
		// ... and to their parent's parent
		this.root.updateValues(new float[] { 1f });
		this.root.updateValues(new float[] { .9f });
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void getNextLeafNodeRecursivelyTest() throws NoSuchMethodException,
			SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			GdlFormatException, SymbolFormatException, ClassNotFoundException {
		Assert.assertEquals(this.root,
				sp.getStrategy().select(root, true, new int[] { 0 })); // case:
																		// rootnode
																		// has
																		// unvisited
																		// children
		this.root.setHasUnexpandedChildren(false);
		Assert.assertEquals(this.root.getChildren().get(3), sp.getStrategy()
				.select(this.root, true, new int[] { 0 })); // case: rootnode
															// has no univisited
															// children
		this.root.getChildren().get(3).setHasUnexpandedChildren(false);
		Assert.assertEquals(
				this.root.getChildren().get(3).getChildren().get(0), sp
						.getStrategy().select(this.root, true, new int[] { 0 })); // case:
																					// the
																					// child
																					// that
																					// is
																					// selected
																					// does
																					// not
																					// have
																					// unvisited
																					// children
		Assert.assertEquals(
				this.root.getChildren().get(3).getChildren().get(0),
				sp.getStrategy().select(
						this.root.getChildren().get(3).getChildren().get(0),
						true, new int[] { 0 })); // case: this childs child has
													// univisited children

		// small test where only the rootnode exists
		// Node rnode = new Node(null, new ArrayList<Node>(), null, null,1);
		// Tree smalltree = new Tree(rnode, null);
		// Assert.assertEquals(rnode,
		// method.invoke(sp,smalltree.getRootNode()));
	}

	@Test
	public void propagateTest() throws NoSuchMethodException,
			SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			ClassNotFoundException {
		// t = sp.getTree();
		Assert.assertEquals(3.6f, this.root.getWins()[0]);
		this.root.getChildren().get(3).getChildren().get(1)
				.updateValues(new float[] { 0.2f });
		ArrayList<Node> list = new ArrayList<Node>();
		list.add(this.root.getChildren().get(3).getChildren().get(1));
		sp.getStrategy().backpropagate(list);
		Assert.assertEquals(1.1f, this.root.getChildren().get(3).getChildren()
				.get(1).getWins()[0], 0.00001);
		Assert.assertEquals(2, this.root.getChildren().get(3).getChildren()
				.get(1).getVisits());

		Assert.assertEquals(3.1f, this.root.getChildren().get(3).getWins()[0],
				0.00001);
		Assert.assertEquals(4, this.root.getChildren().get(3).getVisits());

		Assert.assertEquals(10, this.root.getVisits());
	}
}
