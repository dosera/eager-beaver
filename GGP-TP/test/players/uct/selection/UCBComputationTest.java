package players.uct.selection;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import core.gdl.GdlFactory;
import core.gdl.GdlProposition;

import players.uct.UCTGamer;
import players.uct.UCTSubPlayer;
import players.uct.evaluation.methods.RandomHeuristic;
import players.uct.expansion.ExpandNodebyNode;
import players.uct.selection.UCB1Computation;
import players.uct.selection.UCBTuned1Computation;
import players.uct.selection.UCBTuned2Computation;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.methods.DefaultStrategy;
import players.uct.tree.BidirectionalMap;
import players.uct.tree.Tree;
import players.uct.tree.Node;
import propnet.PropNetStateMachine;
import statemachine.MachineState;
import statemachine.Role;
import statemachine.StateMachine;
;

/**
 * @author tasse
 *
 */
public class UCBComputationTest {

    Node root;
    UCTSubPlayer sp;
    UCTGamer gamer;
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        root = new Node(null, new ArrayList<Node>(), new LinkedList<Node>(), null, null, 1);
        root.updateValues(new float[]{.10f});
        root.updateValues(new float[]{.0f});
        root.updateValues(new float[]{.05f});
        root.updateValues(new float[]{.20f});
        root.updateValues(new float[]{.20f});
        root.updateValues(new float[]{.20f});
        root.updateValues(new float[]{1.00f});
        
        
        
        // #1
        Node first_Child = new Node(root, new ArrayList<Node>(), new LinkedList<Node>(), null, null,1);
        first_Child.updateValues(new float[]{.10f});
        // #2
        Node second_Child = new Node(root, new ArrayList<Node>(), new LinkedList<Node>(), null, null,1);
        second_Child.updateValues(new float[]{.0f});
        second_Child.updateValues(new float[]{.05f});
        // #3
        Node third_Child = new Node(root, new ArrayList<Node>(), new LinkedList<Node>(), null, null,1);
        third_Child.updateValues(new float[]{.20f});
        third_Child.updateValues(new float[]{.20f});
        third_Child.updateValues(new float[]{.20f});
        // #4
        Node fourth_Child = new Node(root, new ArrayList<Node>(), new LinkedList<Node>(),null,null,1);
        fourth_Child.updateValues(new float[]{1.00f});
        
        root.getChildren().add(first_Child);
        root.getChildren().add(second_Child);
        root.getChildren().add(third_Child);
        root.getChildren().add(fourth_Child);
        
        // manually add a role to the gamer
        gamer = new UCTGamer();   
        Method method = gamer.getClass().getDeclaredMethod("changeRole",new Class[]{Role.class});
        method.setAccessible(true);  
        GdlProposition roleName = (GdlProposition) GdlFactory.create("schdof");
        method.invoke(gamer, new Role(roleName));

        BidirectionalMap<Role, Integer> roleMap = new BidirectionalMap<Role, Integer>();
        
        roleMap.put(gamer.getRole(), 0);
        
        sp = new UCTSubPlayer(
        		(AStrategyHandler) null, // strategyhandler
        		(Tree) null, // t
        		gamer.getStateMachine(),  // sm
        		null, // possibleEndSCoresMap
        		roleMap,  // roleIndexMap
        		0); // myIndex
        sp.setStrategy(new DefaultStrategy(sp,new UCB1Computation(),  new ExpandNodebyNode(), new RandomHeuristic()));
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void UCB1ComputationTest() {
        UCB1Computation selection = new UCB1Computation();
        float[] expectedMCScores = {0.1f,0.025f,0.2f,1f};
        float[] expectedUCTScores = {1.395f,0.986f,0.805f,1.395f};
        for(int i = 0; i < this.root.getChildren().size(); i++)
        {
            float result = selection.computeScore(this.root.getChildren().get(i), 0, null,true);
            Assert.assertEquals(expectedMCScores[i] + expectedUCTScores[i] * selection.getConstant(),result,0.01);
        }
        // fourth node is the best in this case
        Assert.assertEquals(this.root.getChildren().get(3), selection.calculateBestNode(root,0, null,true));
    }
    @Test
    public void UCBTuned1ComputationTest() {
        UCBTuned1Computation selection = new UCBTuned1Computation();
        float[] expectedMCScores = {0.1f,0.025f,0.2f,1f};
        float[] expectedUCTScores = {1.395f,0.986f,0.805f,1.395f};
        float[] computedConstants = {.09f,0.024375f,0.16f,0.001f};
        for(int i = 0; i < this.root.getChildren().size(); i++)
        {
            float result = selection.computeScore(this.root.getChildren().get(i),0, null, true);
            Assert.assertEquals(expectedMCScores[i] + expectedUCTScores[i] * Math.sqrt(computedConstants[i]),result,0.01);
        }
        // fourth node is the best in this case
        Assert.assertEquals(this.root.getChildren().get(3), selection.calculateBestNode(root,0, null,true));
    }
    @Test
    public void UCBTuned2ComputationTest() {
        UCBTuned2Computation selection = new UCBTuned2Computation();
        float[] expectedMCScores = {0.1f,0.025f,0.2f,1f};
        float[] expectedUCTScores = {1.395f,0.986f,0.805f,1.395f};
        float[] computedConstants = {.09f,0.024375f,0.16f,0.001f};
        for(int i = 0; i < this.root.getChildren().size(); i++)
        {
            float result = selection.computeScore(this.root.getChildren().get(i),0, null,true);
            Assert.assertEquals(expectedMCScores[i] + 
                    expectedUCTScores[i] * Math.sqrt(computedConstants[i]) + 
                    Math.log(this.root.getVisits()) / this.root.getChildren().get(i).getVisits() ,result,0.01);
        }
        // fourth node is the best in this case
        Assert.assertEquals(this.root.getChildren().get(3), selection.calculateBestNode(root,0, null,true));
    }
}
