package players.uct.selection;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import core.gdl.GdlFactory;
import core.gdl.GdlProposition;

import players.uct.UCTGamer;
import players.uct.UCTSubPlayer;
import players.uct.evaluation.methods.RandomHeuristic;
import players.uct.expansion.ExpandNodebyNode;
import players.uct.selection.UCB1Computation;
import players.uct.selection.UCBTuned1Computation;
import players.uct.selection.UCBTuned2Computation;
import players.uct.strategyhandler.AStrategyHandler;
import players.uct.strategyhandler.methods.DefaultStrategy;
import players.uct.tree.BidirectionalMap;
import players.uct.tree.Tree;
import players.uct.tree.Node;
import statemachine.Role;
;

/**
 * @author tasse
 *
 */
public class ASelectionTest {

    Node root;
    UCTSubPlayer sp;
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        root = new Node(null, new ArrayList<Node>(), new LinkedList<Node>(), null, null, 1);
        
//        t = Tree.getInstance();

        // manually add a role to the gamer
        UCTGamer gamer = new UCTGamer();   
        Method method = gamer.getClass().getDeclaredMethod("changeRole",new Class[]{Role.class});
        method.setAccessible(true);  
        GdlProposition roleName = (GdlProposition) GdlFactory.create("sdjin");
        method.invoke(gamer, new Role(roleName));
        

        BidirectionalMap<Role, Integer> roleMap = new BidirectionalMap<Role, Integer>();
        
        roleMap.put(gamer.getRole(), 0);
        
        sp = new UCTSubPlayer(
        		(AStrategyHandler) null, // strategyhandler
        		(Tree) null, // t
        		gamer.getStateMachine(),  // sm
        		null, // possibleEndSCoresMap
        		roleMap,  // roleIndexMap
        		0); // myIndex
        sp.setStrategy(new DefaultStrategy(sp,new UCB1Computation(),  new ExpandNodebyNode(), new RandomHeuristic()));
        
        // #1
        Node first_Child = new Node(root, new ArrayList<Node>(), new LinkedList<Node>(), null, null, 1);
        // #2
        Node second_Child = new Node(root, new ArrayList<Node>(), new LinkedList<Node>(), null, null, 1);
        // #3
        Node third_Child = new Node(root, new ArrayList<Node>(), new LinkedList<Node>(), null, null,1);
        // #4
        Node fourth_Child = new Node(root, new ArrayList<Node>(), new LinkedList<Node>(),null,null,1);
        
        root.getChildren().add(first_Child);
        root.getChildren().add(second_Child);
        root.getChildren().add(third_Child);
        root.getChildren().add(fourth_Child);
        
        // #4.1
        Node fourth_first_Child = new Node(fourth_Child, new ArrayList<Node>(), new LinkedList<Node>(), null ,null,1);
        fourth_Child.getChildren().add(fourth_first_Child);
        // #4.2
        Node fourth_second_Child = new Node(fourth_Child, new ArrayList<Node>(), new LinkedList<Node>(), null ,null,1);
        fourth_Child.getChildren().add(fourth_second_Child);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void lockOverVisitedNodesTest() {
        ASelection selection = new UCB1Computation();
        
        for(int i = 0; i < root.getChildren().size(); i++)
        {
            selection.lockOverVisitedNodes(root,i,50);
            Assert.assertEquals(-1, root.getChildren().get(i).overvisited);
        }
        // lock first
        for(int i = 0; i < 50; i++)
        {
            root.getChildren().get(0).updateValues(new float[]{0});
        }
        for(int i = 0; i < root.getChildren().size(); i++)
        {
            selection.lockOverVisitedNodes(root,i,50);
            
            if(i == 0)
                Assert.assertEquals(50, root.getChildren().get(i).overvisited);
            else
                Assert.assertEquals(-1, root.getChildren().get(i).overvisited);
        }
        // lock second
        for(int i = 0; i < 100; i++)
        {
            root.getChildren().get(1).updateValues(new float[]{0});
        }
        for(int i = 0; i < root.getChildren().size(); i++)
        {
            selection.lockOverVisitedNodes(root, i, 50);
            if(i == 0)
                Assert.assertEquals(50, root.getChildren().get(i).overvisited);
            else if(i == 1)
                Assert.assertEquals(100, root.getChildren().get(i).overvisited);
            else 
                Assert.assertEquals(-1, root.getChildren().get(i).overvisited);                
        }
    }
}
