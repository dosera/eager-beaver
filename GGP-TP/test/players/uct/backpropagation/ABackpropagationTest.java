/**
 * 
 */
package players.uct.backpropagation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import players.uct.tree.Node;
import statemachine.Move;
import core.gdl.GdlConstant;
import core.gdl.GdlSentence;

/**
 * @author tasse
 *
 */
public class ABackpropagationTest {
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void simultaneousDefeatSituationsTest()
    {
    	ArrayList<Node> rootList = new ArrayList<Node>();
    	Node rootNode = new Node(null, rootList, new LinkedList<Node>(), null, null, 3);
    	GdlSentence mySentence = new GdlConstant("A").toSentence();
    	GdlSentence p1Sentence = new GdlConstant("X").toSentence();
    	GdlSentence p2SentenceY = new GdlConstant("Y").toSentence();
    	GdlSentence p2SentenceZ = new GdlConstant("Z").toSentence();
    	Move[] moveC1 = 
    		{ new Move(mySentence),
    		  new Move(p1Sentence),
    		  new Move(p2SentenceY)
    	    };
    	Move[] moveC2 = 
    		{ new Move(mySentence),
      		  new Move(p1Sentence),
    		  new Move(p2SentenceZ)
    	    };
    	rootNode.getPossibleMovesOfRoleIndex()[0] = 1;
    	rootNode.getPossibleMovesOfRoleIndex()[1] = 1;
    	rootNode.getPossibleMovesOfRoleIndex()[2] = 2;
    	
    	ArrayList<Node> rootKids = new ArrayList<Node>();
    	List<Node> c1kids = new ArrayList<Node>();
    	Node child1 = new Node(rootNode, c1kids, new LinkedList<Node>(), null, moveC1, 3);
    	rootKids.add(child1);
    	child1.setLeadsToDefeat(true);
    	List<Node> c2kids = new ArrayList<Node>();
    	Node child2 = new Node(rootNode, c2kids, new LinkedList<Node>(), null, moveC2, 3);
    	rootKids.add(child2);
    	
    	rootNode.getChildren().addAll(rootKids);
    	
    	ABackpropagation ab = new BackpropagationST();
    	ab.simultaneousDefeatSituations(rootNode, 0);
    	Assert.assertEquals(true, child2.LeadsToDefeat());
    	Assert.assertTrue(rootNode.LeadsToDefeat());
    }
}
