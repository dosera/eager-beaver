/**
 * 
 */
package players.uct.expansion;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author tasse
 *
 */
public class AExpansionTest {
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void calculateCartesianCombinationsTest() throws NoSuchMethodException, SecurityException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException
    {        
        // first test: "NOOP" twice
        List<String> noopList = new ArrayList<String>();
        noopList.add("NOOP");
        List[] noopListArray = new List[2];
        noopListArray[0] = noopList;
        noopListArray[1] = noopList;
        
        // result
        List[] result = AExpansion.calculateCartesianCombinations(noopListArray);
        // length
        Assert.assertEquals(1,result.length );
        // content
        for(int i = 0; i < result.length; i++)
            for(int j = 0; j < result[i].size(); j++)
                Assert.assertEquals("NOOP", result[i].get(j));
        
        // second test: "NOOP" / "3" moves
        List<String> secondList = new ArrayList<String>();
        secondList.add("NOOP");
        List<String> secondList2 = new ArrayList<String>();
        secondList2.add("A M 1");
        secondList2.add("A M 2");
        secondList2.add("A M 3");
        List[] secondArray = new List[2];
        secondArray[0] = secondList;
        secondArray[1] = secondList2;
        
        // result
        result = AExpansion.calculateCartesianCombinations(secondArray);
        // length
        Assert.assertEquals(3,result.length );
        // content
        for(int i = 0; i < result.length; i++)
            Assert.assertEquals("[NOOP, A M " + (i+1) + "]", result[i].toString());
        
        // third test: "3" / "NOOP" moves
        List<String> thirdList = new ArrayList<String>();
        List<String> thirdList2 = new ArrayList<String>();
        thirdList2.add("NOOP");
        thirdList.add("A M 1");
        thirdList.add("A M 2");
        thirdList.add("A M 3");
        List[] thirdArray = new List[2];
        thirdArray[0] = thirdList;
        thirdArray[1] = thirdList2;
        
        // result
        result = AExpansion.calculateCartesianCombinations(thirdArray);
        // length
        Assert.assertEquals(3,result.length );
        // content
        for(int i = 0; i < result.length; i++)
            Assert.assertEquals("[A M " + (i+1) + ", NOOP]", result[i].toString());
        
     // third test: "4" / "3" moves
        List<String> fourthList = new ArrayList<String>();
        List<String> fourthList2 = new ArrayList<String>();
        fourthList.add("B M 1");
        fourthList.add("B M 2");
        fourthList.add("B M 3");
        fourthList.add("B M 4");
        fourthList2.add("A M 1");
        fourthList2.add("A M 2");
        fourthList2.add("A M 3");
        List[] fourthArray = new List[2];
        fourthArray[0] = fourthList;
        fourthArray[1] = fourthList2;
        
        // result
        result = AExpansion.calculateCartesianCombinations(fourthArray);
        // length
        Assert.assertEquals(12,result.length);
        // content
        Assert.assertEquals("[B M 2, A M 3]", result[5].toString());
        Assert.assertEquals("[B M 1, A M 1]", result[0].toString());
        Assert.assertEquals("[B M 4, A M 3]", result[11].toString());
        
        // fifth test: "4" / "3" / "2" moves
        List<String> fifthList = new ArrayList<String>();
        List<String> fifthList2 = new ArrayList<String>();
        List<String> fifthList3 = new ArrayList<String>();
        fifthList.add("A M 1");
        fifthList.add("A M 2");
        fifthList.add("A M 3");
        fifthList.add("A M 4");
        fifthList2.add("B M 1");
        fifthList2.add("B M 2");
        fifthList2.add("B M 3");
        fifthList3.add("C M 1");
        fifthList3.add("C M 2");
        List[] fifthArray = new List[3];
        fifthArray[0] = fifthList3;
        fifthArray[1] = fifthList;
        fifthArray[2] = fifthList2;
        
        // result
        result = AExpansion.calculateCartesianCombinations(fifthArray);
        // length
        Assert.assertEquals(24,result.length);
        // content
        Assert.assertEquals("[C M 1, A M 1, B M 1]", result[0].toString());
        Assert.assertEquals("[C M 2, A M 4, B M 3]", result[23].toString());
        
        // test order
        for(int i = 0; i < result.length; i++)
    	{
        	Assert.assertTrue(result[i].get(0).toString().contains("C"));
        	Assert.assertTrue(result[i].get(1).toString().contains("A"));
        	Assert.assertTrue(result[i].get(2).toString().contains("B"));
    	}
    }
}
