package propnet;

import static org.junit.Assert.*;

import helper.KifReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import propnet.PropNet;
import propnet.PropNetStateMachine;
import propnet.architecture.Component;
import propnet.architecture.Proposition;
import propnet.factory.OptimizingPropNetFactory;

import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;
import core.exceptions.GdlFormatException;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.SymbolFormatException;
import core.exceptions.TransitionDefinitionException;
import core.gdl.Gdl;
import core.gdl.GdlConstant;
import core.gdl.GdlSentence;
import core.gdl.GdlTerm;

public class PropNetStateMachineTest {

    static List<Gdl> gamerules;
    static PropNet propnet;
    static PropNetStateMachine pnsm = new PropNetStateMachine();
    static Set<GdlSentence> basepropSentences;

    public GdlSentence getSentenceFromString(Set<GdlSentence> set, String name) {
        for (GdlSentence sentence : set) {
            if (sentence.toString().equals(name)) {
                return sentence;
            }
        }
        System.out.println("sentence " + name + " not found");
        return null;
    }

    public Proposition getPropositionFromString(Set<Proposition> props,
            String name) {
        for (Proposition prop : props) {
            if (prop.getName().toSentence().toString().equals(name))
                return prop;
        }
        System.out.println("proposition " + name + " not found");
        return null;
    }

    public Move getMoveFromString(List<Move> moves, String name) {
        for (Move move : moves) {
            if (move.toString().equals(name))
                return move;

        }
        System.out.println("move" + name + " not found");
        return null;
    }

    public boolean containsProposition(List<Component> components,
            Proposition prop) {
        for (Component c : components) {
            if (c instanceof Proposition && ((Proposition) c).equals(prop))
                return true;
        }
        return false;
    }


    @Before
    public void setUp() throws Exception {
        gamerules = KifReader.read("games/TwoButtonGame.kif");
        propnet = OptimizingPropNetFactory.create(gamerules);
        pnsm.initialize(gamerules);
        Set<GdlTerm> basepropterms = pnsm.getPropNet().getBasePropositions()
                .keySet();
        basepropSentences = new HashSet<GdlSentence>();
        for (GdlTerm term : basepropterms) {
            basepropSentences.add(term.toSentence());
        }
    }

    @Test
    public void testGetGoal() throws GoalDefinitionException {
        MachineState state = pnsm.getInitialState();
        Role role = pnsm.getRoles().get(0);
        Assert.assertEquals(0, pnsm.getGoal(state, role));
        // TODO 100 points goal test
    }

    @Test
    public void testIsTerminal() {
        MachineState initialState = pnsm.getInitialState();
        boolean isTerminal = pnsm.isTerminal(initialState);
        Assert.assertFalse(isTerminal);
        // Todo test a terminal state
    }

    @Test
    public void testGetRoles() {
        List<Role> roles = pnsm.getRoles();
        Assert.assertTrue(roles.get(0).toString().equals("you"));
    }

    @Test
    public void testGetInitialState() {
        MachineState state = pnsm.getInitialState();
        GdlSentence offa = getSentenceFromString(basepropSentences, "( off a )");
        GdlSentence offb = getSentenceFromString(basepropSentences, "( off b )");
        GdlSentence ona = getSentenceFromString(basepropSentences, "( on a )");
        GdlSentence onb = getSentenceFromString(basepropSentences, "( on b )");

        Assert.assertTrue(state.getContents().contains(offa));
        Assert.assertTrue(state.getContents().contains(offb));
        Assert.assertFalse(state.getContents().contains(ona));
        Assert.assertFalse(state.getContents().contains(onb));
    }

    @Test
    public void testGetLegalMoves() throws IOException, SymbolFormatException,
            GdlFormatException, InterruptedException, MoveDefinitionException, TransitionDefinitionException {
        gamerules = KifReader
                .read("games/tictactoe_2008_AAAI_GGP_Competition.kif");
        pnsm.initialize(gamerules);
        propnet = pnsm.getPropNet();
        
        MachineState initialState = pnsm.getInitialState();
        Role role = pnsm.getRoles().get(0);

        List<Move> legalmoves = pnsm.getLegalMoves(initialState, role);

        Move play11x = getMoveFromString(legalmoves, "( play 1 1 x )");
        Move play12x = getMoveFromString(legalmoves, "( play 1 2 x )");
        Move play13x = getMoveFromString(legalmoves, "( play 1 3 x )");
        Move play21x = getMoveFromString(legalmoves, "( play 2 1 x )");
        Move play22x = getMoveFromString(legalmoves, "( play 2 2 x )");
        Move play23x = getMoveFromString(legalmoves, "( play 2 3 x )");
        Move play31x = getMoveFromString(legalmoves, "( play 3 1 x )");
        Move play32x = getMoveFromString(legalmoves, "( play 3 2 x )");
        Move play33x = getMoveFromString(legalmoves, "( play 3 3 x )");
        

        Assert.assertNotNull(play11x);
        Assert.assertNotNull(play12x);
        Assert.assertNotNull(play13x);
        Assert.assertNotNull(play21x);
        Assert.assertNotNull(play22x);
        Assert.assertNotNull(play23x);
        Assert.assertNotNull(play31x);
        Assert.assertNotNull(play32x);
        Assert.assertNotNull(play33x);
        Assert.assertEquals(9, legalmoves.size());
        
        
        // tests after more than one move
        
        Set<GdlSentence> contents = new HashSet<GdlSentence>();
        Proposition controloplayer = getPropositionFromString(propnet.getPropositions(), "( control oPlayer )");
        Proposition m11x = getPropositionFromString(propnet.getPropositions(), "( mark 1 1 x )");
        Proposition m22x = getPropositionFromString(propnet.getPropositions(), "( mark 2 2 x )");
        Proposition m21o = getPropositionFromString(propnet.getPropositions(), "( mark 2 1 o )");
        Proposition controlxplayer = getPropositionFromString(propnet.getPropositions(), "( control xPlayer )");
        Proposition m23x = getPropositionFromString(propnet.getPropositions(), "( mark 2 3 x )");
        Proposition m12x = getPropositionFromString(propnet.getPropositions(), "( mark 1 2 x )");
        Proposition m33o = getPropositionFromString(propnet.getPropositions(), "( mark 3 3 o )");
        Proposition m13x = getPropositionFromString(propnet.getPropositions(), "( mark 1 3 x )");

        
        
        
        contents.add(controloplayer.getName().toSentence());
        contents.add(m11x.getName().toSentence());
        contents.add(m22x.getName().toSentence());
        contents.add(m21o.getName().toSentence());

       
        MachineState quadstate = new MachineState(contents);
        
        legalmoves = pnsm.getLegalMoves(quadstate, role);
        Assert.assertTrue(legalmoves.size() > 0);
        Assert.assertTrue(legalmoves.size() == 1);
        legalmoves = pnsm.getLegalMoves(quadstate, pnsm.getRoles().get(1));
        Assert.assertTrue(legalmoves.size() > 0);
        Assert.assertTrue(legalmoves.size() == 6);

        //destructive statemachine test
        
        contents = new HashSet<GdlSentence>();
       
              
        
        contents.add(controlxplayer.getName().toSentence());
        contents.add(m23x.getName().toSentence());
        contents.add(m21o.getName().toSentence());

        quadstate = new MachineState(contents);
        MachineState followstate = pnsm.getNextStateDestructively(quadstate, pnsm.getRandomJointMove(quadstate));
        Assert.assertTrue(followstate.getContents().contains(controloplayer.getName().toSentence()));
        // TODO more test cases
        
        //getrandommovetest
        contents = new HashSet<GdlSentence>();
        contents.add(controloplayer.getName().toSentence());
        contents.add(m13x.getName().toSentence());
        contents.add(m33o.getName().toSentence());
        contents.add(m12x.getName().toSentence());
        quadstate = new MachineState(contents);
        legalmoves = pnsm.getLegalMoves(quadstate, pnsm.getRoles().get(1));
        Move play21o = getMoveFromString(legalmoves, "( play 2 1 o )");
        legalmoves = pnsm.getLegalMoves(quadstate, role);
        Move noop = getMoveFromString(legalmoves, "noop");
        ArrayList<Move> moves = new ArrayList<Move>();
        moves.add(noop);
        moves.add(play21o);
        followstate = pnsm.getNextState(quadstate, moves);
        Assert.assertTrue(followstate.getContents().contains(m33o.getName().toSentence()));
        
    }

    @Test
    public void testGetNextState() throws MoveDefinitionException,
            TransitionDefinitionException, IOException, SymbolFormatException,
            GdlFormatException, InterruptedException {

        gamerules = KifReader
                .read("games/tictactoe_2008_AAAI_GGP_Competition.kif");
        propnet = OptimizingPropNetFactory.create(gamerules);
        pnsm.initialize(gamerules);
        MachineState initialState = pnsm.getInitialState();
        Role role = pnsm.getRoles().get(0);

        List<Move> legalmovesx = pnsm.getLegalMoves(initialState, role);
        List<Move> legalmoveso = pnsm.getLegalMoves(initialState, pnsm
                .getRoles().get(1));

        Move play11x = getMoveFromString(legalmovesx, "( play 1 1 x )");
        Move onoop = getMoveFromString(legalmoveso, "noop");
        List<Move> ml = new ArrayList<Move>();
        ml.add(play11x);
        ml.add(onoop);

        // TODO: write a better test case
        MachineState nextState = pnsm.getNextState(initialState, ml);
        legalmovesx = pnsm.getLegalMoves(initialState, role);
        legalmoveso = pnsm.getLegalMoves(initialState, pnsm.getRoles().get(1));
        Assert.assertTrue(nextState.getContents().toString()
                .contains("( control oPlayer )"));
        Assert.assertTrue(nextState.getContents().toString()
                .contains("( mark 1 1 x )"));

        /**
         * MachineState initialstate = pnsm.getInitialState(); List<Move>
         * legalmoves = pnsm.getLegalMoves(initialstate,
         * pnsm.getRoles().get(0)); Move pressA = getMoveFromString(legalmoves,
         * "( pressButton a )"); List<Move> ml = new ArrayList<Move>();
         * ml.add(pressA); MachineState nextState =
         * pnsm.getNextState(pnsm.getStateFromBase(), ml);
         * 
         * GdlSentence offa = getSentenceFromString(basepropSentences,
         * "( off a )"); GdlSentence offb =
         * getSentenceFromString(basepropSentences, "( off b )"); GdlSentence
         * ona = getSentenceFromString(basepropSentences, "( on a )");
         * GdlSentence onb = getSentenceFromString(basepropSentences,
         * "( on b )");
         * 
         * Assert.assertFalse(nextState.getContents().contains(offa));
         * Assert.assertTrue(nextState.getContents().contains(offb));
         * Assert.assertTrue(nextState.getContents().contains(ona));
         * Assert.assertFalse(nextState.getContents().contains(onb));
         */

    }

    @Test
    public void testGetOrdering() throws InterruptedException, IOException,
            SymbolFormatException, GdlFormatException {
        gamerules = KifReader
                .read("games/tictactoe_2008_AAAI_GGP_Competition.kif");
        propnet = OptimizingPropNetFactory.create(gamerules);
        pnsm.initialize(gamerules);
        MachineState initstate = pnsm.getInitialState();
        Proposition legalo110 = getPropositionFromString(pnsm.getPropNet()
                .getPropositions(), "( legal oPlayer ( play 1 1 o ) )");
        Proposition emptycell11 = getPropositionFromString(pnsm.getPropNet()
                .getPropositions(), "( emptyCell 1 1 )");
        Proposition col3x = getPropositionFromString(pnsm.getPropNet()
                .getPropositions(), "( col 3 x )");
        Proposition linex = getPropositionFromString(pnsm.getPropNet()
                .getPropositions(), "( line x )");
        Proposition goalx100 = getPropositionFromString(pnsm.getPropNet()
                .getPropositions(), "( goal xPlayer 100 )");
        Proposition row2x = getPropositionFromString(pnsm.getPropNet()
                .getPropositions(), "( row 2 x )");
        Proposition open = getPropositionFromString(pnsm.getPropNet()
                .getPropositions(), "open");
        Proposition goalo50 = getPropositionFromString(pnsm.getPropNet()
                .getPropositions(), "( goal oPlayer 50 )");

        Assert.assertTrue(pnsm.getOrdering().indexOf(emptycell11) < pnsm
                .getOrdering().indexOf(legalo110));
        Assert.assertTrue(pnsm.getOrdering().indexOf(col3x) < pnsm
                .getOrdering().indexOf(linex));
        Assert.assertTrue(pnsm.getOrdering().indexOf(col3x) < pnsm
                .getOrdering().indexOf(goalx100));
        Assert.assertTrue(pnsm.getOrdering().indexOf(linex) < pnsm
                .getOrdering().indexOf(goalx100));
        Assert.assertTrue(pnsm.getOrdering().indexOf(row2x) < pnsm
                .getOrdering().indexOf(linex));
        Assert.assertTrue(pnsm.getOrdering().indexOf(row2x) < pnsm
                .getOrdering().indexOf(goalx100));
        Assert.assertTrue(pnsm.getOrdering().indexOf(linex) < pnsm
                .getOrdering().indexOf(goalo50));
        Assert.assertTrue(pnsm.getOrdering().indexOf(open) < pnsm.getOrdering()
                .indexOf(goalo50));

    }

    @Test
    public void testGetMoveFromProposition() {
        Set<Proposition> legalprops = pnsm.getPropNet().getLegalPropositions()
                .get(pnsm.getRoles().get(0));
        Proposition prop = getPropositionFromString(legalprops,
                "( legal you ( pressButton b ) )");
        Move move = PropNetStateMachine.getMoveFromProposition(prop);
        Assert.assertEquals("( pressButton b )", move.toString());
    }

}

