package propnet;

import static org.junit.Assert.*;

import helper.KifReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import core.exceptions.GdlFormatException;
import core.exceptions.GoalDefinitionException;
import core.exceptions.MoveDefinitionException;
import core.exceptions.SymbolFormatException;
import core.exceptions.TransitionDefinitionException;
import core.gdl.Gdl;

import propnet.architecture.Component;
import propnet.architecture.Proposition;
import propnet.factory.OptimizingPropNetFactory;
import statemachine.MachineState;
import statemachine.Move;
import statemachine.Role;

public class FactoringTest {

    PropNet propnet;

    public Move getMoveFromString(List<Move> moves, String name) {
        for (Move move : moves) {
            if (move.toString().equals(name))
                return move;

        }
        System.out.println("move" + name + " not found");
        return null;
    }
    
    public Proposition getPropositionFromString(Set<Proposition> props,
            String name) {
        for (Proposition prop : props) {
            if (prop.getName().toSentence().toString().equals(name))
                return prop;
        }
        System.out.println("proposition " + name + " not found");
        return null;
    }

    @Test
    public void testCreateIS() throws Exception {
        List<Gdl> gamerules = KifReader.read("games/LightsOnParallel.kif");
        propnet = OptimizingPropNetFactory.create(gamerules);
        propnet.renderToFile("propnet");
        Factoring f = new Factoring(propnet);
        Arrays.fill(f.getiSetIndex(), -1);
        f.getIsets().clear();
        Proposition lightson3 = getPropositionFromString(
                propnet.getPropositions(), "lightsOn-3");
        Proposition on24 = getPropositionFromString(propnet.getPropositions(),
                "( on 2 4 )");
        Proposition on22 = getPropositionFromString(propnet.getPropositions(),
                "( on 2 2 )");
        Proposition cell242 = getPropositionFromString(
                propnet.getPropositions(), "( cell 2 4 2 )");
        Proposition cell240 = getPropositionFromString(
                propnet.getPropositions(), "( cell 2 4 0 )");

        // create the first is, from lightson3
        HashSet<Component> is = new HashSet<Component>();
        f.createIS(lightson3,is);

        Assert.assertTrue(f.getIsets().size() > 0);
        Assert.assertEquals(0, f.getiSetIndex()[propnet.getCompToInt()
                .get(on24)]);
        Assert.assertTrue(f.getIsets()
                .get(f.getiSetIndex()[propnet.getCompToInt().get(on24)])
                .contains(on24));
        Assert.assertEquals(0,
                f.getiSetIndex()[propnet.getCompToInt().get(cell242)]);
        Assert.assertTrue(f.getIsets()
                .get(f.getiSetIndex()[propnet.getCompToInt().get(cell242)])
                .contains(cell242));
        Assert.assertEquals(-1,
                f.getiSetIndex()[propnet.getCompToInt().get(on22)]);

        // this should add to the is from lightson3 more props, since it should
        // be the same is
        is = new HashSet<Component>();
        f.createIS(cell240, is);
        Assert.assertEquals(0,
                f.getiSetIndex()[propnet.getCompToInt().get(cell240)]);
        Assert.assertTrue(f.getIsets()
                .get(f.getiSetIndex()[propnet.getCompToInt().get(cell240)])
                .contains(cell240));

    }

    @Test
    public void testCreateAllAIs() throws IOException, SymbolFormatException,
            GdlFormatException, InterruptedException {
        List<Gdl> gamerules = KifReader.read("games/LightsOnParallel.kif");
        propnet = OptimizingPropNetFactory.create(gamerules);
        propnet.renderToFile("propnet");
        Factoring f = new Factoring(propnet);
        Proposition legal13 = getPropositionFromString(
                propnet.getPropositions(), "( legal robot ( press 1 3 ) )");
        f.createAllAIs();
        Assert.assertTrue(f.getaIsets().size() > 0);
        Assert.assertTrue(f.getaIsets()
                .get(f.getiSetIndex()[propnet.getCompToInt().get(legal13)])
                .contains(legal13));

    }

    @Test
    public void testgetIsAndcreatePns() throws IOException,
            SymbolFormatException, GdlFormatException, InterruptedException, MoveDefinitionException, TransitionDefinitionException, GoalDefinitionException {
        List<Gdl> gamerules = KifReader.read("games/LightsOnParallel.kif");
        propnet = OptimizingPropNetFactory.create(gamerules);
        propnet.renderToFile("propnet");
        Factoring f = new Factoring(propnet);
        f.getIsAndcreatePns();
        PropNet pn = null;
        for (PropNet p : f.subpropnets) {
            for (Proposition proposition : p.getBasePropositions().values()) {
                if(proposition.getName().toString().equals("( cell 1 1 4 )"))
                    pn = p;
            }
        }
        PropNetStateMachine pnsm = new PropNetStateMachine();
        pnsm.initialize(pn);
        
        
        MachineState initialState = pnsm.getInitialState();
        pnsm.performDepthCharge(initialState, new int[10]);

        Role role = pnsm.getRoles().get(0);

        List<Move> legalmovesrobot = pnsm.getLegalMoves(initialState, role);

        Move press11 = getMoveFromString(legalmovesrobot, "( press 1 1 )");
        Move press12 = getMoveFromString(legalmovesrobot, "( press 1 2 )");
        Move press21 = getMoveFromString(legalmovesrobot, "( press 2 1 )");
        Move press22 = getMoveFromString(legalmovesrobot, "( press 2 2 )");

        Move noop = getMoveFromString(legalmovesrobot, "noop");
        List<Move> ml = new ArrayList<Move>();
        ml.add(press11);
       

        MachineState nextState = pnsm.getNextState(initialState, ml);
        
        Assert.assertTrue(nextState.getContents().toString()
                .contains("( cell 1 1 4 )"));
        Assert.assertEquals(0, pnsm.getGoal(nextState, role));
        
        ml = new ArrayList<Move>();
        ml.add(press22);
        nextState = pnsm.getNextState(nextState, ml);
        Assert.assertTrue(nextState.getContents().toString()
                .contains("( cell 1 1 3 )"));
        Assert.assertEquals(0, pnsm.getGoal(nextState, role));
        
        ml = new ArrayList<Move>();
        ml.add(press12);
        nextState = pnsm.getNextState(nextState, ml);
        Assert.assertEquals(0, pnsm.getGoal(nextState, role));

        ml = new ArrayList<Move>();
        ml.add(press21);
        nextState = pnsm.getNextState(nextState, ml);
        Assert.assertEquals(100, pnsm.getGoal(nextState, role));

        ml = new ArrayList<Move>();
        ml.add(noop);
        nextState = pnsm.getNextState(nextState, ml);
        Assert.assertTrue(nextState.getContents().toString()
                .contains("( cell 1 1 0 )"));
        Assert.assertEquals(0, pnsm.getGoal(nextState, role));
        
    }
}
